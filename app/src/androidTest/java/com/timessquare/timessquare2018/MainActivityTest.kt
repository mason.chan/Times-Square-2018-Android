package com.timessquare

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.internal.util.Checks
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.timessquare.menu.MenuItem
import com.timessquare.vic.vicprograms.itemmodel.VICGenericModelItem
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 *   Created by Mason on 14/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @After
    fun pullDown() {
        Thread.sleep(5000)
    }

    /**
     * Test case for click on VIC item.
     */
    @Test
    fun vicItemOnClick() {
        Espresso.onView(ViewMatchers.withId(R.id.menuButton))
                .perform(click())

        Espresso.onView(ViewMatchers.withId(R.id.MenuRecyclerView))
                .perform(
                        RecyclerViewActions.actionOnHolderItem(
                                withItemTextTitle("VIC"),
                                click()
                        )
                )

        Espresso.onView(ViewMatchers.withId(R.id.vicVic))
                .perform(click())

        Espresso.onView(ViewMatchers.withId(R.id.vicProgramsRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition<VICGenericModelItem.ViewHolder>(0, click()))

    }

    private fun withItemTextTitle(title: String): Matcher<RecyclerView.ViewHolder> {
        Checks.checkNotNull(title)
        return object : BoundedMatcher<RecyclerView.ViewHolder, MenuItem.ViewHolder>(MenuItem.ViewHolder::class.java) {
            override fun matchesSafely(item: MenuItem.ViewHolder): Boolean {
                return title == item.textView.text
            }

            override fun describeTo(description: Description) {
                description.appendText("")
            }
        }
    }
}