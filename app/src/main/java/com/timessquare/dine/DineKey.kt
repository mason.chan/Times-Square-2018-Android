package com.timessquare.dine

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 6/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class DineKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(DineKey))

    override fun createFragment(): BaseFragment = DineFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}