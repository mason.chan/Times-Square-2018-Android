package com.timessquare.dine

import com.timessquare.R
import com.timessquare.shopanddine.ShopDineFragment

/**
 *   Created by Mason on 6/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class DineFragment : ShopDineFragment() {
    override fun setTitle(): Int = R.string.food_beverages_title

    override fun isShopNotFood(): Boolean =false
}