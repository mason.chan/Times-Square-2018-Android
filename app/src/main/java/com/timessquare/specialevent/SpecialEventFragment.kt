package com.timessquare.specialevent

import com.timessquare.R
import com.timessquare.dummy.IndexDummyData
import com.timessquare.generallistview.GeneralListViewFragment
import com.timessquare.generallistview.GeneralListViewItem

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class SpecialEventFragment : GeneralListViewFragment() {

    override fun addData(): List<GeneralListViewItem> {
        var rawData = IndexDummyData.goodiesItems
        var result = ArrayList<GeneralListViewItem>()
        for (item in rawData) {
            result.add(GeneralListViewItem(item.mName, item.mImageUrl, item.mPosterUrl))
        }
        return result
    }

    override fun setTitle(): Int {
        return R.string.special_event_title
    }
}