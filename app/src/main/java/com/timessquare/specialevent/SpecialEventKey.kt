package com.timessquare.specialevent

import android.annotation.SuppressLint
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class SpecialEventKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(SpecialEventKey))

    override fun createFragment() = SpecialEventFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}