package com.timessquare.offershopdetail

import android.view.View
import com.timessquare.MainActivity
import com.timessquare.shopanddine.shopanddinedetail.ShopDineDetailFragment
import com.timessquare.shopanddine.shopanddinedetail.ShopDineDetailOfferItem
import com.timessquare.utils.Constant
import com.timessquare.vic.vicprograms.OfferType

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class OfferShopDetailFagment : ShopDineDetailFragment() {
    override fun addOffice(): ArrayList<ShopDineDetailOfferItem> {
        return ArrayList()
    }

    val offerType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT2) ?: 0 }

    override fun fillData(view: View) {
//        mView.vicLinearLayout.visibility = View.GONE
//        mView.bvicLinearLayout.visibility = View.GONE

        isEnglish = MainActivity[view.context].isEnglish()
        mView.contentTextView.text = when (offerType) {
            OfferType.VIC_Year_Round_Offers.value -> if (isEnglish) {
                shopEntity.vicYearRoundOfferDescription ?: ""
            } else {
                shopEntity.vicYearRoundOfferDescriptionZh ?: ""
            }
//            OfferType.BVIC_Year_Round_Offers.value -> content = shopEntity.bvicYearRoundOfferDescription?: ""
//            OfferType.TVIC_Year_Round_Offers.value -> content = shopEntity.gvicYearRoundOfferDescription?: ""
            OfferType.VIC_Birthday_Offers.value -> if (isEnglish) {
                shopEntity.vicBirthdayOfferDesctiption ?: ""
            } else {
                shopEntity.vicBirthdayOfferDesctiptionZh ?: ""
            }
//            OfferType.BVIC_Birthday_Offers.value -> content = shopEntity.bvicBirthdayOfferDesctiption?: ""
//            OfferType.TVIC_Birthday_Offers.value -> content = shopEntity.gvicBirthdayOfferDesctiption?: ""
            else -> ""
        }
        mView.meunVerticalLayout.visibility = View.GONE
    }

}