package com.timessquare.offershopdetail

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class OfferShopDetailKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(OfferShopDetailKey))

    override fun createFragment(): BaseFragment = OfferShopDetailFagment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}