package com.timessquare.carpark

import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.Gravity
import android.view.View
import android.widget.*
import com.timessquare.R
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout

/**
 *   Created by Mason on 4/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class CarParkUI() : AnkoComponent<CarParkFragment> {

    lateinit var mBannerImageView: ImageView
    lateinit var mOctopusCardButton: Button
    lateinit var mCreditCardButton: Button
    lateinit var mOctopusCardLayout: LinearLayout
    lateinit var mCreditCardLayout: LinearLayout
    lateinit var mOctopusCardEditText: EditText
    lateinit var mCreditCardFirstEditText: EditText
    lateinit var mCreditCardLastEditText: EditText
    lateinit var mLicensePlateNumberEditText: EditText
    lateinit var mCheckButton: Button
    lateinit var randomCodeTextView: TextView

    override fun createView(ui: AnkoContext<CarParkFragment>): View {
        return with(ui) {
            constraintLayout() {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    titleBar {
                        id = R.id.CarParkTitleBar
                    }.addBackgroundColor(R.color.color_cover).addMenuButton().addTitle(R.string.car_park_title)
                    //scroll view
                    scrollView {
                        verticalLayout {

                            // Banner
                            mBannerImageView = imageView {
                                id = R.id.CarParkBanner
                                imageResource = R.drawable.default_pic_black
                                adjustViewBounds = true
                            }.lparams(matchParent, wrapContent) {
                            }
                            // octopus card credit card switch
                            cardView {
                                id = R.id.CarParkCardView
                                backgroundColorResource = R.color.color_white
                                linearLayout() {
                                    gravity = Gravity.CENTER_VERTICAL
                                    orientation = LinearLayout.HORIZONTAL
                                    // Button Octopus Card
                                    mOctopusCardButton = include<Button>(R.layout.car_park_button) {
                                        id = R.id.CarParkOctopusButton
                                        textResource = R.string.car_park_octopus_card
                                        isPressed = false

                                    }
                                    // Button Credit Card
                                    mCreditCardButton = include<Button>(R.layout.car_park_button) {
                                        id = R.id.CarParkCreditButton
                                        textResource = R.string.car_park_credit_card
                                        isPressed = false
                                    }

                                }.lparams(matchParent, wrapContent)
                            }
                            // input box
                            // octopus Card
                            mOctopusCardLayout = verticalLayout {
                                padding = dimen(R.dimen.padding_small)
                                textView {
                                    textResource = R.string.car_park_octopus_card_text_view
                                    textColorResource = R.color.color_black
                                    textSizeDimen = R.dimen.text_normal
                                }
                                mOctopusCardEditText = include<EditText>(R.layout.car_park_edit_text) {
                                    hintResource = R.string.car_park_octopus_card_edit_text
                                }

                            }
                            // credit Card
                            mCreditCardLayout = verticalLayout {
                                padding = dimen(R.dimen.padding_small)
                                textView {
                                    textResource = R.string.car_park_credit_card_text_view
                                    textColorResource = R.color.color_black
                                    textSizeDimen = R.dimen.text_normal
                                }
                                linearLayout {

                                    mCreditCardFirstEditText = include<EditText>(R.layout.car_park_edit_text) {
                                        hintResource = R.string.car_park_credit_card_edit_view_first
                                    }
                                    mCreditCardLastEditText = include<EditText>(R.layout.car_park_edit_text) {
                                        hintResource = R.string.car_park_credit_card_edit_view_last
                                    }

                                }
                            }
                            // license Plate Number
                            verticalLayout {
                                padding = dimen(R.dimen.padding_small)
                                textView {
                                    textResource = R.string.car_park_license_plate_number_text_view
                                    textColorResource = R.color.color_black
                                    textSizeDimen = R.dimen.text_normal
                                }
                                mLicensePlateNumberEditText = include<EditText>(R.layout.car_park_edit_text) {
                                    hintResource = R.string.car_park_license_plate_number_edit_text
                                }
                            }
                            // parking reference
                            randomCodeTextView = textView {
                                topPadding = dimen(R.dimen.padding_small)
                                bottomPadding = dimen(R.dimen.padding_small)
                                textSizeDimen = R.dimen.text_subject
                                gravity = Gravity.CENTER
                                textResource = R.string.car_park_parking_reference
                            }.lparams(matchParent, wrapContent) {
                            }
                            // only applied
                            textView {
                                textSizeDimen = R.dimen.text_subject
                                gravity = Gravity.CENTER
                                textResource = R.string.car_park_parking_only_applied
                            }.lparams(matchParent, wrapContent) {
                            }
                            // check button
                            mCheckButton = button {
                                backgroundResource = R.drawable.button_sure
                                textResource = R.string.car_park_check_button
                            }.lparams(matchParent, wrapContent) {
                                margin = dimen(R.dimen.margin_normal)
                            }

                        }
                    }

                }.lparams(matchParent, matchParent) {
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                }
            }
        }
    }
}