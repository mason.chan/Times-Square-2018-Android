package com.timessquare.carpark

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 4/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class CarParkKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(CarParkKey))

    override fun createFragment(): BaseFragment = CarParkFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}