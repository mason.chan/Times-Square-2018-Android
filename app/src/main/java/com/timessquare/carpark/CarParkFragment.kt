package com.timessquare.carpark

import android.os.Bundle
import android.util.Log
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.crashlytics.android.Crashlytics
import com.timessquare.BaseFragment
import com.timessquare.BuildConfig
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.apiservice.Path
import com.timessquare.apiservice.datamodel.CarLocationModel
import com.timessquare.carparkstatus.CarParkStatusKey
import com.timessquare.generallistview.generalposter.GeneralPosterKey
import com.timessquare.multipleposter.MultiplePosterKey
import com.timessquare.utils.Constant
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx
import java.io.File
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


/**
 *   Created by Mason on 4/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class CarParkFragment : BaseFragment() {

    var isOctopus = false
//    var mPreviewUrl = Path.PUBLIC_IMAGE_URL + Path.CAR_PARK_IMAGE_URL
    var mPreviewUrl = Path.PRO_IMAGE_URL + "/free_parking_poster_small.jpg"
    var mPosterUrl= arrayListOf("parking_1hours_poster.jpg", "parking_3hours_poster.jpg")
    var securityKey = "Ts1\$Css73"
    lateinit var mView: CarParkUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = CarParkUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mView.randomCodeTextView.text = randomCode()
        Glide.with(view.context).load(mPreviewUrl).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).into(mView.mBannerImageView)
        mView.mBannerImageView.onClick {
            MainActivity.get(view.context).navigateTo(MultiplePosterKey().withArgs {
                putStringArrayList(Constant.TAG_INTENT, mPosterUrl)
            })
        }
        mView.mOctopusCardButton.onClick { switchInput(mView.mOctopusCardButton.id) }
        mView.mCreditCardButton.onClick { switchInput(mView.mCreditCardButton.id) }
        mView.mCheckButton.onClick { checkParkingStatus() }
//        mView.mCheckButton.onClick { MainActivity[view.context].navigateTo(CarParkStatusKey()) }

        mView.mOctopusCardButton.performClick()
    }

    fun randomCode(): String {
        return "" + getText(R.string.car_park_parking_reference) + " #" + UUID.randomUUID().toString().toUpperCase().substring(0, 5)
    }
    fun switchInput(id: Int) {
        isOctopus = (id == mView.mOctopusCardButton.id)
        //set Button
        mView.mOctopusCardButton.isSelected = isOctopus

        mView.mCreditCardButton.isSelected = !isOctopus
        // show Input Box
        if (isOctopus) {
            mView.mOctopusCardLayout.visibility = View.VISIBLE
            mView.mCreditCardLayout.visibility = View.GONE
        } else {
            mView.mOctopusCardLayout.visibility = View.GONE
            mView.mCreditCardLayout.visibility = View.VISIBLE
        }
        i("switchInput", "isOctopus " + isOctopus)
        i("switchInput", "   oct    " + mView.mOctopusCardButton.isPressed)
        i("switchInput", "   credit    " + mView.mCreditCardButton.isPressed)
    }

    private fun checkParkingStatus() {
        /**
         * Check Input
         */
        var cardNo: String
        var lPN: String
        if (isOctopus) {
            if (mView.mOctopusCardEditText.text.length > 8) {
                mView.mOctopusCardEditText.error = getString(R.string.car_park_field_more_than_eight)
                return
            }
            if (mView.mOctopusCardEditText.text.isEmpty()) {
                mView.mOctopusCardEditText.error = getString(R.string.car_park_field_empty)
                return
            }
            if (mView.mLicensePlateNumberEditText.text.isEmpty()) {
                mView.mLicensePlateNumberEditText.error = getString(R.string.car_park_field_empty)
                return
            }
            cardNo = mView.mOctopusCardEditText.text.toString()
            lPN = mView.mLicensePlateNumberEditText.text.toString()
            for (element in lPN) {
                lPN = lPN.replace(" ","")
            }
            Log.i("cardNo", lPN)
        } else {
            if (mView.mCreditCardFirstEditText.text.isEmpty()) {
                mView.mCreditCardFirstEditText.error = getString(R.string.car_park_field_empty)
                return
            }
            if (mView.mCreditCardLastEditText.text.isEmpty()) {
                mView.mCreditCardLastEditText.error = getString(R.string.car_park_field_empty)
                return
            }
            if (mView.mLicensePlateNumberEditText.text.isEmpty()) {
                mView.mLicensePlateNumberEditText.error = getString(R.string.car_park_field_empty)
                return
            }
            cardNo = mView.mCreditCardFirstEditText.text.toString() + '-' + mView.mCreditCardLastEditText.text.toString()
            lPN = mView.mLicensePlateNumberEditText.text.toString()
            for (element in lPN) {
                lPN = lPN.replace(" ","")
            }
            Log.i("cardNo", lPN)
        }
        /**
         * Asking Server
         */

        var randomNo = Math.random().toString().substring(2, 8)
        i("requestUrl", "requestUrl " + randomNo)
        var hachKey = encode(lPN = lPN, cardNo = cardNo, randomNo = randomNo, securityKey = securityKey)

        var requestUrl = Path.CAR_PARK_URL + Path.API_CARPARK_CAR_LOCATION + lPN + "/" + cardNo + "/" + randomNo + "/" + hachKey
        i("observableString", " requestUrl " + requestUrl)

        var getStringObservable = MainActivity[ctx].apiService.carLocation(lPN, cardNo, randomNo, hachKey)
        /**
         *  subscribe
         */
        getStringObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : Observer<CarLocationModel> {
                    override fun onComplete() {
                        i("observableString", " onComplete  ")
                    }

                    override fun onSubscribe(d: Disposable) {
                        i("observableString", " onSubscribe  " + d)
                    }

                    override fun onNext(t: CarLocationModel) {
                        i("observableString", " onNext  " + t)


                        if(t.resCode == 0) {
                            MainActivity[ctx].navigateTo(CarParkStatusKey().withArgs {
                                putString(Constant.TAG_INTENT, carIngressTime(t))
                                putString(Constant.TAG_INTENT2, carDuration(t))
                                putString(Constant.TAG_INTENT3, carLocation(t))
                            })
                        } else {
                            AlertDialog(R.string.car_park_parking_alert_title, R.string.car_park_parking_alert_message).show()
                        }
                    }

                    override fun onError(e: Throwable) {
                        i("observableString", " onError  " + e)
                        AlertDialog(R.string.car_park_parking_alert_title, R.string.car_park_parking_alert_message).show()
                    }
                }
                )
    }

    private fun encode(lPN: String, cardNo: String, randomNo: String, securityKey: String): String {
        var encodeString = lPN + "&" + cardNo + "&" + randomNo + "&" + securityKey
        try {
            val instance: MessageDigest = MessageDigest.getInstance("MD5")
            val digest: ByteArray = instance.digest(encodeString.toByteArray())
            var sb: StringBuffer = StringBuffer()
            for (b in digest) {
                var i: Int = b.toInt() and 0xff//获取低八位有效值
                var hexString = Integer.toHexString(i)//将整数转化为16进制
                if (hexString.length < 2) {
                    hexString = "0" + hexString//如果是一位的话，补0
                }
                sb.append(hexString)
            }
            return sb.toString().toUpperCase()

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return ""
    }

    private fun carIngressTime(carLocationModel: CarLocationModel): String {
        if (carLocationModel.entryTime == null) {
            return ""
        }
        return carLocationModel.entryTime.toString()
    }
    private fun carDuration(carLocationModel: CarLocationModel): String {
        if (carLocationModel.entryTime == null) {
            return ""
        }
        var inDate = SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(carLocationModel.entryTime)
        var durationSec = (Date().time - inDate.time)/1000L

        val days = TimeUnit.SECONDS.toDays(durationSec).toInt()
        val hours = (TimeUnit.SECONDS.toHours(durationSec) - days * 24).toInt()
        val minutes = (TimeUnit.SECONDS.toMinutes(durationSec) - TimeUnit.SECONDS.toHours(durationSec) * 60).toInt()
//        val second = (TimeUnit.SECONDS.toSeconds(durationSec) - TimeUnit.SECONDS.toMinutes(durationSec) * 60).toInt()

        var result = ""

        when (days) {
            0 -> ""
            1 ->  result = result + days +  " " + getText(R.string.car_park_status_day) +  " "
            else ->  result = result + days +  " " + getText(R.string.car_park_status_days) +  " "
        }
        when (hours) {
            0 -> ""
            1 ->  result = result + hours +  " " + getText(R.string.car_park_status_hour) +  " "
            else ->  result = result + hours +  " " + getText(R.string.car_park_status_hours) +  " "
        }
        when (minutes) {
            0 -> ""
            1 ->  result = result + minutes +  " " + getText(R.string.car_park_status_minute) +  " "
            else ->  result = result + minutes +  " " + getText(R.string.car_park_status_minutes) +  " "
        }
        return result
    }
    private fun carLocation(carLocationModel: CarLocationModel): String {
        return carLocationModel.floor + ", " + carLocationModel.bayNo
    }

}
