package com.timessquare.apiservice.datas

import com.google.gson.annotations.SerializedName

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class GoodiesData(val error: Boolean, val goodies : List<Goodies>)

data class Goodies(
        @SerializedName("goodies_id") var goodiesId: Int,
        @SerializedName("goodies_title") var goodiesTitle: String
)
