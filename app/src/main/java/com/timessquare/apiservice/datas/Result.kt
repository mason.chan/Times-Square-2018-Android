//package com.hita.apiservice.datas
//
///**
// *   Created by Mason on 15/5/2018.
// *   Hita Group
// *   mason.chan@sllin.com
// */
//
//data class Result(val total_count: Int, val incomplete_results: Boolean)