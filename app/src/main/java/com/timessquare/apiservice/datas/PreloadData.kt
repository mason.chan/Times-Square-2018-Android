package com.timessquare.apiservice.datas

import com.timessquare.apiservice.datamodel.EventModel
import com.timessquare.apiservice.datamodel.ShopModel

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class PreloadData(
        var shops: List<ShopModel>,
        var shops2: List<ShopModel>,
        var events: List<EventModel>

)