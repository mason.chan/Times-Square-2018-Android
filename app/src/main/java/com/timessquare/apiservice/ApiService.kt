package com.timessquare.apiservice

import com.google.gson.GsonBuilder
import com.timessquare.apiservice.datamodel.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.OkHttpClient





/**
 *   Created by Mason on 26/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

interface ApiService {

    @GET("shop/full_detail")
    fun getShop(): Observable<List<ShopModel>>

    @GET("event/full_detail")
    fun getEvent(): Observable<List<EventModel>>

    /**
     *     Old API
     *     Check Card Info
     */
    @Headers("authentication: ojPhrwatQEY2d6etoWHn")
    @GET("https://timessquare.sllin.com/timessquare_vic/v1.0.0/member/{code}")
    fun checkCardInfo(@retrofit2.http.Path("code") code: String
    ): Observable<OldCardInfoModel>

    @POST("ecard/login")
    @FormUrlEncoded
    fun attemptMemberLogin(@Field("ecard_email") ecard_email: String, @Field("ecard_password") ecard_password: String): Observable<MemberLoginModel>

    @POST("ecard/card_code/verify")
    @FormUrlEncoded
    fun verifyCardCode(@Field("card_code") card_code: String): Observable<VerifyCardModel>

    @POST("ecard/register")
    @FormUrlEncoded
    fun memberRegister(@Field("ecard_name") ecard_name: String,
                       @Field("card_code") card_code: String,
                       @Field("ecard_email") ecard_email: String,
                       @Field("ecard_lang") ecard_lang: String,
                       @Field("ecard_password") ecard_password: String): Observable<MemberRegisterModel>

    /**
     * Change Password
     */
    @POST("ecard/password/change")
    @FormUrlEncoded
    fun changePassword(
            @Header("X-Access-Token") accessToken: String,
            @Field("old_password") oldPassword: String,
            @Field("new_password") newPassword: String
    ): Observable<ChangePasswordModel>

    /**
     * Forgot Password
     */
    @POST("ecard/password/forget")
    @FormUrlEncoded
    fun forgotPassword(
            @Field("ecard_email") ecard_email: String
    ): Observable<ForgotPasswordModel>

    /**
     * Add e Card and get result message
     */
    @POST("ecard/add")
    @FormUrlEncoded
    fun addEcard(
            @Header("X-Access-Token") accessToken: String,
            @Field("card_code") card_code: String
    ): Observable<AddEcardModel>

    /**
     * Remove e Card and get result message
     */
    @POST("ecard/remove")
    @FormUrlEncoded
    fun removeEcard(
            @Header("X-Access-Token") accessToken: String,
            @Field("card_id") card_id: Int
    ): Observable<RemoveEcardModel>

    /**
     * Car Park Location
     */
    @POST("http://210.5.174.246/carlocation/{car_plate}/{card_no}/{random_number}/{hash_key}")

    fun carLocation(
            @retrofit2.http.Path("car_plate") car_plate: String,
            @retrofit2.http.Path("card_no") card_no: String,
            @retrofit2.http.Path("random_number") random_number: String,
            @retrofit2.http.Path("hash_key") hash_key: String
    ): Observable<CarLocationModel>


    @GET
    fun getStringResponse(@Url url: String): Observable<String>

    /**
     * Companion object to create the ApiService
     */
    companion object Factory {
        fun create(): ApiService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(Path.PRO_DATABASE_URL)
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(
                            GsonBuilder()
//                                    .excludeFieldsWithoutExposeAnnotation()
//                                    .serializeNulls()
                                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                                    .create())
                    )
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}

