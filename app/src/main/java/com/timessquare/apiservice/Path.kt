package com.timessquare.apiservice

/**
 *   Created by Mason on 24/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

object Path {
    // Database URL
//    var DATABASE_URL = "http://test.app.timessquare.sllin.com/api/"
//    var CAR_PARK_URL = "http://210.5.174.246/tsapp/getentrytime.svc/"
    var CAR_PARK_URL = "http://210.5.174.246/"


    // Image URL
//    var IMAGE_URL = "http://test.app.timessquare.sllin.com/resource/image/"
//    var PUBLIC_IMAGE_URL = "http://test.app.timessquare.sllin.com/public/images/"
    var CAR_PARK_IMAGE_URL = "app_car_park.jpg"
    var ABOUT_US_IMAGE_URL = "app_about_us.jpg"
    // Api
//    var API_CARPARK_BY_OCTOPUS = "ByLPNwOctopus"
//    var API_CARPARK_BY_CREADITCARD = "ByLPNwCreditCard"
    var API_CARPARK_CAR_LOCATION = "carlocation/"
    var API_CARPARK_VACANCY = "vacancy/"

    // Floor Map
//    var FLOOR_MAP_URL = "http://test.app.timessquare.sllin.com/public/floor_plan_mobile?lang="
    var FLOOR_MAP_FLOOR_URL = "&floor="
    var FLOOR_MAP_SHOP_ID_URL = "&shop="

    /**
     * production URL
      */
    // Database URL
    var PRO_DATABASE_URL = "http://api.timessquare.sllin.com/"
    // Image
    var PRO_IMAGE_URL = "http://resource.timessquare.com.hk/image/"
    var PUBLIC_LMAGE_URL = "http://public.timessquare.sllin.com/images/"
    // Floor Map
    var PRO_FLOOR_MAP_URL = "http://www.timessquare.com.hk/floor_plan_mobile?lang="
}