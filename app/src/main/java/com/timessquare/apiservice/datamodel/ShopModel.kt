package com.timessquare.apiservice.datamodel

import com.google.gson.annotations.SerializedName

/**
 *   Created by Mason on 25/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */


data class ShopModel(
        @SerializedName("shop_id") var Id: Int,
        @SerializedName("shop_number") var number: String,
        @SerializedName("shop_floor_id") var floorId: Int,
        @SerializedName("shop_phone") var phone: String,
        @SerializedName("shop_category_id") var categoryId: Int,
        @SerializedName("shop_class_id") var classId: Int,
        @SerializedName("shop_name") var name: String,
        @SerializedName("shop_name_localized") var nameLocalized: NameLocalized? = null,
        @SerializedName("shop_thumb_image") var thumbImage: String? = null,
        @SerializedName("shop_menu_image") var menu: ArrayList<String>? = null,
        @SerializedName("shop_description") var description: String? = null,
        @SerializedName("shop_description_localized") var descriptionLocalized: DescriptionLocalized? = null,
        @SerializedName("shop_time") var openHours: ArrayList<OpenHour>? = null,
        @SerializedName("shop_detail_image") var detailImage: String,
        @SerializedName("shop_attribute") var attribute: VICAttribute? = null,
        @SerializedName("shop_extra_description") var extraDesctiption: ExtraDesctiption? = null
)


data class NameLocalized(
        @SerializedName("zh") var nameZh: String? = null
)

data class DescriptionLocalized(
        @SerializedName("zh") var descriptionZh: String? = null
)

data class OpenHour(
        @SerializedName("when") var dayOpen: DayOpen,
        @SerializedName("time") var timeOpenList: List<TimeOpen>
)

data class DayOpen(
        var weekday: String,
        var holiday: Boolean,
        var pre_holiday: Boolean
)

data class TimeOpen(
        var open: String,
        var close: String
)

data class VICAttribute(
        @SerializedName("vic_privilege") var vicPrivilege: Boolean? = null,
        @SerializedName("bvic_privilege") var bvicPrivilege: Boolean? = null,
        @SerializedName("gvic_privilege") var gvicPrivilege: Boolean? = null,
        @SerializedName("year_round_offer") var yearRoundOffer: Boolean? = null,
        @SerializedName("birthday_offer") var birthdayOffer: Boolean? = null,
        @SerializedName("office_club") var officeClub: Boolean? = null,
        @SerializedName("club_on") var clubOn: Boolean? = null
)

class ExtraDesctiption(
        @SerializedName("zh") var extraDesctiptionZh: ExtraDesctiptionZh? = null,
        @SerializedName("default") var extraDesctiption: ExtraDesctiptionEn? = null
)

class ExtraDesctiptionZh(
        @SerializedName("birthday_offer") var birrthdayOffer: String? = null,
        @SerializedName("year_round_offer") var yearRoundOffer: String? = null
)

class ExtraDesctiptionEn(
        @SerializedName("birthday_offer") var birrthdayOffer: String? = null,
        @SerializedName("year_round_offer") var yearRoundOffer: String? = null
)