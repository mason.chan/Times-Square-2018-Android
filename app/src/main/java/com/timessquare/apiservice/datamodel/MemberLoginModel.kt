package com.timessquare.apiservice.datamodel

import java.util.*

/**
 *   Created by Mason on 3/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class MemberLoginModel(
        var ecard_id: Int,
        var ecard_email: String,
        var ecard_name: String,
        var ecard_refresh_token: String,
        var access_token: String,
        var message: String,
        var vic_card: ArrayList<VicCard>,
// If error
        var error_message: String,
        var error_code: String
)

data class VicCard(
        var card_pin: String,
        var card_code: String,
        var card_id: Int,
        var card_type_id: Int, //1=vic, 2=tourist vic, 3=beauty vic
        var card_status: Int, //0=freezed or lost, 1=normal
        var member_first_name: String,
        var member_last_name: String,
        var member_email: String,
        var member_tel: String,
        var member_point: Int,
        var member_point_expiry_date: Date
)