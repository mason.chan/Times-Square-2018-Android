package com.timessquare.apiservice.datamodel

/**
 *   Created by Mason on 15/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class MemberRegisterModel(
        var message: String,
        var error_message: String,
        var error_code: Int
)