package com.timessquare.apiservice.datamodel

/**
 *   Created by Mason on 9/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class RemoveEcardModel(
        var message: String? = null
)