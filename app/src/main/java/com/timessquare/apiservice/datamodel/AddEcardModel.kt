package com.timessquare.apiservice.datamodel

import com.google.gson.annotations.SerializedName

/**
 *   Created by Mason on 7/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class AddEcardModel(
        @SerializedName("message") var message: String? = null,
        @SerializedName("error_message") var errorMessage: String? = null,
        @SerializedName("error_code") var errorCode: Int? = null


)