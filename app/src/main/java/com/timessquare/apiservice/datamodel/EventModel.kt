package com.timessquare.apiservice.datamodel

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 *   Created by Mason on 20/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 *
 *   Event is the class of all
 */
data class EventModel(
        @SerializedName("event_id") var id: Int,
        @SerializedName("event_sort_id") var sortId: Int? = null,
        @SerializedName("event_class_id") var classId: Int? = null,
        @SerializedName("event_title") var title: String? = null,
        @SerializedName("event_title_localized") var titleLocalized: TitleLocalized? = null,
        @SerializedName("event_thumb") var thumb: String? = null,
        @SerializedName("event_poster") var poster: ArrayList<String>? = null,
        @SerializedName("event_poster_localized") var posterLocalized: PosterLocalized ? = null,
        @SerializedName("event_terms") var terms: String? = null,
        @SerializedName("event_terms_localized") var termsLocalized: TermsLocalized? = null,
        @SerializedName("event_start_date") var startDate: Date,
        @SerializedName("event_end_date") var endDate: Date

)
data class PosterLocalized(
        @SerializedName("zh") var zh : ArrayList<String>? = null
)
data class TitleLocalized(
        @SerializedName("zh") var zh: String? = null,
        @SerializedName("zhsc") var zhsc: String? = null
)
data class TermsLocalized(
        @SerializedName("zh") var zh: String? = null,
        @SerializedName("zhsc") var zhsc: String? = null
)