package com.timessquare.apiservice.datamodel

import com.google.gson.annotations.SerializedName

/**
 *   Created by Mason on 13/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
data class ChangePasswordModel(
        @SerializedName("message") var message: String? = null,
        @SerializedName("error_code") var errorCode: Int? = null,
        @SerializedName("error_message") var errorMessage: String? = null
)