package com.timessquare.apiservice.datamodel

import java.util.*

/**
 *   Created by Mason on 28/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
data class CarLocationModel(
        var resCode: Int, // 0: Normal, 1: Error
        var resMsg: String = "",
        var floor: String = "",
        var bayNo: String = "",
        var carImage: String = "",
        var entryTime: String = ""
//        var entryTime: Date = Date()
)