package com.timessquare.apiservice.datamodel

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 *   Created by Mason on 5/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
data class OldCardInfoModel(
        @SerializedName("data") var oldCardData: OldCardData,
        @SerializedName("message") var message: String? = null,
        var status: Boolean = false
) {
    class OldCardData(
            var card_pin: String,
//            var card_code: String,
//            var card_type_name: String,
            var member_id: String,
            var member_first_name: String,
            var member_last_name: String ,
            var member_tel: String,
            var member_email: String,
            var member_point: Int,
            var member_point_expiry_date: Date
    )
}
