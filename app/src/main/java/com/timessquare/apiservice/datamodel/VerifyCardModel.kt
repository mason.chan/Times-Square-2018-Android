package com.timessquare.apiservice.datamodel

/**
 *   Created by Mason on 14/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

data class VerifyCardModel(
        var message: String = "",
        var card_type_id: Int = -1,
        var card_code: String = "",
        var card_pin: String = "",
        // if error
        var error_message:  String = "",
        var error_code: String = ""
)