package com.timessquare.dummy

import com.timessquare.menu.MenuItem
import com.timessquare.R
import com.timessquare.aboutus.AboutUsKey
import com.timessquare.apiservice.Path
import com.timessquare.carpark.CarParkKey
import com.timessquare.dine.DineKey
import com.timessquare.goodies.GoodiesItem
import com.timessquare.goodies.GoodiesKey
import com.timessquare.index.IndexItem
import com.timessquare.index.IndexKey
import com.timessquare.settings.SettingsKey
import com.timessquare.shop.ShopKey
import com.timessquare.shopanddine.ShopDineItem
import com.timessquare.vic.VICKey
import com.timessquare.vic.vicprograms.OfferType
import com.timessquare.vic.vicprograms.VICProgramsKey
import com.timessquare.vic.vicprograms.itemmodel.*
import com.timessquare.happenings.HappeningsItem
import com.timessquare.happenings.HappeningsKey
import java.util.Arrays


/**
 * Created by Mason on 18/5/2018.
 * Hita Group
 * mason.chan@sllin.com
 */
object IndexDummyData {

    val indexItems: List<IndexItem>
        get() = toList(
                IndexItem().withKey(HappeningsKey()).withName(R.string.menu_happenings).withImage(Path.PUBLIC_LMAGE_URL + "/app_happenings.jpg"),
                IndexItem().withKey(ShopKey()).withName(R.string.menu_shopping).withImage(Path.PUBLIC_LMAGE_URL +"/app_shopping.jpg"),
                IndexItem().withKey(DineKey()).withName(R.string.menu_foodandbeverages).withImage(Path.PUBLIC_LMAGE_URL + "/app_food.jpg"),
                IndexItem().withKey(GoodiesKey()).withName(R.string.menu_goodies).withImage(Path.PUBLIC_LMAGE_URL +"/app_goodies.jpg"),
                IndexItem().withKey(VICKey()).withName(R.string.menu_vic_programs).withImage(Path.PUBLIC_LMAGE_URL + "/app_vic_programs.jpg"),
                IndexItem().withKey(AboutUsKey()).withName(R.string.menu_about_us).withImage(Path.PUBLIC_LMAGE_URL + "/app_about_us.jpg"),
                IndexItem().withKey(CarParkKey()).withName(R.string.menu_car_park).withImage(Path.PUBLIC_LMAGE_URL + "/app_car_park.jpg")

                )
    val menuItem: List<MenuItem>
        get() = toList(
                MenuItem().withKey(IndexKey()).withName(R.string.menu_home).withImage(R.drawable.ic_menu_index),
                MenuItem().withKey(HappeningsKey()).withName(R.string.menu_happenings).withImage(R.drawable.ic_menu_whats_up),
                MenuItem().withKey(ShopKey()).withName(R.string.menu_shopping).withImage(R.drawable.ic_menu_shop),
                MenuItem().withKey(DineKey()).withName(R.string.menu_foodandbeverages).withImage(R.drawable.ic_menu_dine),
                MenuItem().withKey(GoodiesKey()).withName(R.string.menu_goodies).withImage(R.drawable.ic_menu_goodies),
                MenuItem().withKey(VICKey()).withName(R.string.menu_vic_programs).withImage(R.drawable.ic_menu_vic),
                MenuItem().withKey(AboutUsKey()).withName(R.string.menu_about_us).withImage(R.drawable.ic_menu_about),
                MenuItem().withKey(CarParkKey()).withName(R.string.menu_car_park).withImage(R.drawable.ic_menu_car_park),
//                MenuItem().withKey(SpecialEventKey()).withName(R.string.menu_special_event).withImage(R.drawable.ic_menu_special),
                MenuItem().withKey(SettingsKey()).withName(R.string.menu_settings).withImage(R.drawable.ic_menu_setting)
        )
//    val happeningsItems: List<HappeningsItem>
//        get() = toList(
//                HappeningsItem().withName("A Rendezvous with Hats and Headdress Pieces in Cantonese Opera ")
//                        .withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg")
//                        .withPosterUrl("https://d1csarkz8obe9u.cloudfront.net/posterpreviews/big-sale-poster-template-e7deae6828c68a02661b1b6d941bfd0b.jpg?ts=1506393169")
//                        .withTermsAndConditions("1. All the above discount offers are not applied to fixed price products.\n" +
//                                "2. The offer cannot be used in conjunction with other discounts, promotions, discounted items.\n" +
//                                "3. For other terms and conditions, please contact the participating shops.\n" +
//                                "4. Times Square and the participating shops reserve the right of final decision on any disputes.\n" +
//                                "5. Offers are valid till 31 Dec 2018.\n" +
//                                "6. To enjoy the above privileges, members simply present their \"Times Square Club ON\" membership card before spending.\n" + "1. All the above discount offers are not applied to fixed price products.\n" +
//                                "2. The offer cannot be used in conjunction with other discounts, promotions, discounted items.\n" +
//                                "3. For other terms and conditions, please contact the participating shops.\n" +
//                                "4. Times Square and the participating shops reserve the right of final decision on any disputes.\n" +
//                                "5. Offers are valid till 31 Dec 2018.\n" +
//                                "6. To enjoy the above privileges, members simply present their \"Times Square Club ON\" membership card before spending.\n" + "1. All the above discount offers are not applied to fixed price products.\n" +
//                                "2. The offer cannot be used in conjunction with other discounts, promotions, discounted items.\n" +
//                                "3. For other terms and conditions, please contact the participating shops.\n" +
//                                "4. Times Square and the participating shops reserve the right of final decision on any disputes.\n" +
//                                "5. Offers are valid till 31 Dec 2018.\n" +
//                                "6. To enjoy the above privileges, members simply present their \"Times Square Club ON\" membership card before spending.\n" + "1. All the above discount offers are not applied to fixed price products.\n" +
//                                "2. The offer cannot be used in conjunction with other discounts, promotions, discounted items.\n" +
//                                "3. For other terms and conditions, please contact the participating shops.\n" +
//                                "4. Times Square and the participating shops reserve the right of final decision on any disputes.\n" +
//                                "5. Offers are valid till 31 Dec 2018.\n" +
//                                "6. To enjoy the above privileges, members simply present their \"Times Square Club ON\" membership card before spending.\n" + "1. All the above discount offers are not applied to fixed price products.\n" +
//                                "2. The offer cannot be used in conjunction with other discounts, promotions, discounted items.\n" +
//                                "3. For other terms and conditions, please contact the participating shops.\n" +
//                                "4. Times Square and the participating shops reserve the right of final decision on any disputes.\n" +
//                                "5. Offers are valid till 31 Dec 2018.\n" +
//                                "6. To enjoy the above privileges, members simply present their \"Times Square Club ON\" membership card before spending.\n"),
//                HappeningsItem().withName("90 Years of Hong Kong Porcelain Art Exhibition")
//                        .withPosterUrl("https://cdn.shopify.com/s/files/1/1722/0531/products/lost-times-sq_1024x1024.jpg?v=1512408757")
//                        .withTermsAndConditions("withTermsAndConditions")
//                        .withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
//                HappeningsItem()
//                        .withPosterUrl("http://www.timessquare.com.hk/cms/upload/ArtComp_Poster_2018_OP.JPG")
//                        .withName("DINE")
//                        .withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
//                HappeningsItem().withName("WHATS UP").withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg"),
//                HappeningsItem().withName("YO WHATS UP").withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg"),
//                HappeningsItem().withName("SHOP").withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
//                HappeningsItem().withName("DINE").withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
//                HappeningsItem().withName("WHATS UP").withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg")
//        )
    val goodiesItems: List<GoodiesItem>
        get() = toList(
                GoodiesItem().withName("Movie - Molly's Game").withImage("http://mcfcomplex.in/wp-content/uploads/2017/05/1200px-7_playing_cards.jpg")
                        .withPosterUrl("https://i.pinimg.com/736x/46/67/6d/46676da834595328670a486559eb63f0.jpg"),
                GoodiesItem().withName("Room Guest Exclusive Offer").withImage("https://www.visa.ca/en_CA/pay-with-visa/cards/credit-cards/_jcr_content/par/cardstack/cardStackColumn3/image_441293779.img.jpg/1498761058678.jpg")
                        .withPosterUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7fzLLEZJzDIozMVIfsl_Q2h6s_kaKUUEWxm4E126Q2hTfBoaO"),
                GoodiesItem().withName("DINE").withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
                GoodiesItem().withName("WHATS UP").withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg"),
                GoodiesItem().withName("WHATS UP").withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg"),
                GoodiesItem().withName("SHOP").withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
                GoodiesItem().withName("DINE").withImage("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/shadegan_refuge-iran-12.jpg"),
                GoodiesItem().withName("WHATS UP").withImage("https://cdn.ndtv.com/tech/images/gadgets/pikachu_hi_pokemon.jpg")
        )

//
//    private fun toList(vararg imageItems: ShopDineItem): List<ShopDineItem> {
//        return Arrays.asList(*imageItems)
//    }

    private fun toList(vararg imageItems: GoodiesItem): List<GoodiesItem> {
        return Arrays.asList(*imageItems)
    }

//    private fun toList(vararg imageItems: HappeningsItem): List<HappeningsItem> {
//        return Arrays.asList(*imageItems)
//    }

    private fun toList(vararg imageItems: IndexItem): List<IndexItem> {
        return Arrays.asList(*imageItems)
    }

    private fun toList(vararg imageItems: MenuItem): List<MenuItem> {
        return Arrays.asList(*imageItems)
    }
}
