package com.timessquare.memberactivation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 16/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberActivationFragment : BaseFragment() {

    private lateinit var mView: MemberActivationUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberActivationUI()
        return mView.createView(AnkoContext.Companion.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}