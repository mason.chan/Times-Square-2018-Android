package com.timessquare.memberactivation

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 16/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MemberActivationKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberActivationKey))

    override fun createFragment(): BaseFragment = MemberActivationFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}