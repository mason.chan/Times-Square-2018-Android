package com.timessquare.memberactivation

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.Gravity
import android.view.View
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.index.IndexKey
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 16/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberActivationUI : AnkoComponent<MemberActivationFragment> {

    override fun createView(ui: AnkoContext<MemberActivationFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                titleBar {
                }.addBackgroundColor(R.color.color_cover).addTitle(R.string.member_activation_title)
                verticalLayout {
                    textView {
                        textResource = R.string.member_activation_subject
                        textSizeDimen = R.dimen.text_double_title
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView {
                        textResource = R.string.member_activation_message
                        textSizeDimen = R.dimen.text_menu_size
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams() {
                        margin = dimen(R.dimen.margin_double)
                    }
                    button {
                        textResource = R.string.member_activation_button
                        textSizeDimen = R.dimen.text_menu_size
                        backgroundResource = R.drawable.button_sure
                        onClick { MainActivity[ctx].replaceHistory(IndexKey()) }
                    }.lparams(matchParent, wrapContent) {
                        margin = dimen(R.dimen.margin_double)
                    }
                }.lparams() {
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                }
            }
        }
    }
}