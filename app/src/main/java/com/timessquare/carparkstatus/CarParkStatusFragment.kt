package com.timessquare.carparkstatus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.v
import com.timessquare.BaseFragment
import com.timessquare.R
import com.timessquare.utils.Constant
import kotlinx.android.synthetic.main.car_park_status_item.view.*
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 27/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class CarParkStatusFragment : BaseFragment() {

    val ingress: String by lazy { arguments?.getString(Constant.TAG_INTENT) ?: "" }
    val duration: String by lazy { arguments?.getString(Constant.TAG_INTENT2) ?: "" }
    val location: String by lazy { arguments?.getString(Constant.TAG_INTENT3) ?: "" }

    lateinit var mView: CarParkStatusUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = CarParkStatusUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        mView.dateTimeConstraintLayout.key.text = getText(R.string.car_park_status_date_title)
        mView.dutationConstraintLayout.key.text = getText(R.string.car_park_status_duration_title)
        mView.locationLayout.key.text = getText(R.string.car_park_status_location_title)
        mView.dateTimeConstraintLayout.value.text = ingress
        mView.dutationConstraintLayout.value.text = duration
        mView.locationLayout.value.text = if (location == ", ") { getText(R.string.car_park_parking_no_postitioning) } else {location}
    }
}