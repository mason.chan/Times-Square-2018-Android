package com.timessquare.carparkstatus

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.View
import android.widget.TextView
import com.timessquare.R
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.car_park_status_item.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout

/**
 *   Created by Mason on 27/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class CarParkStatusUI : AnkoComponent<CarParkStatusFragment> {

    lateinit var dateTimeConstraintLayout: ConstraintLayout
    lateinit var dutationConstraintLayout: ConstraintLayout
    lateinit var locationLayout: ConstraintLayout

    override fun createView(ui: AnkoContext<CarParkStatusFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = PARENT_ID
                }
                titleBar {
                    id = R.id.titlebar
                }.addTitle(R.string.car_park_status_title)
                        .addBackButton()
                        .addBackgroundColor(R.color.color_cover)
                        .lparams(width = matchParent) {
                            topToTop = PARENT_ID
                        }
                dateTimeConstraintLayout = include<ConstraintLayout>(R.layout.car_park_status_item) {
                    id = R.id.dateTimeConstraintLayout
                }.lparams() {
                    topToBottom = R.id.titlebar
                }
                dutationConstraintLayout = include<ConstraintLayout>(R.layout.car_park_status_item) {
                    id = R.id.dutationConstraintLayout
                }.lparams() {
                    topToBottom = R.id.dateTimeConstraintLayout
                }
                locationLayout = include<ConstraintLayout>(R.layout.car_park_status_item) {
                    find<TextView>(R.id.value).textResource = R.string.car_park_parking_no_postitioning
                }.lparams() {
                    topToBottom = R.id.dutationConstraintLayout
                }
            }
        }
    }
}

