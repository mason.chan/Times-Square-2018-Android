package com.timessquare.carparkstatus

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 27/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class CarParkStatusKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(CarParkStatusKey))

    override fun createFragment(): BaseFragment = CarParkStatusFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}