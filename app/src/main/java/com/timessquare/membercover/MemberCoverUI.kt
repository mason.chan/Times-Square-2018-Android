package com.timessquare.membercover

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.View
import android.widget.ImageView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.memberlogin.MemberLoginKey
import com.timessquare.memberscancard.MemberScanCardKey
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.menufooter_view.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.guideline
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 9/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberCoverUI() : AnkoComponent<MemberCoverFragment> {
    override fun createView(ui: AnkoContext<MemberCoverFragment>): View {
        return with(ui) {
            constraintLayout {
                guideline {
                    id = R.id.memberCoverGuideLine
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guidePercent = 0.25f
                }
                guideline {
                    id = R.id.memberCoverGuideLine2
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guidePercent = 0.75f
                }
                guideline {
                    id = R.id.memberCoverGuideLine3
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.HORIZONTAL
                    guidePercent = 0.8f
                }
                guideline {
                    id = R.id.memberCoverGuideLine4
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guidePercent = 0.5f
                }
                /**
                 * Background
                 */
                imageView(R.drawable.bg_member_cover) {
                    adjustViewBounds = true
                    scaleType = ImageView.ScaleType.CENTER_CROP
                }.lparams(width = matchParent, height = matchParent) {
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                }

                imageView(R.drawable.ic_logo_white) {
                    adjustViewBounds = true
                }.lparams() {
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                    leftToLeft = R.id.memberCoverGuideLine
                    rightToRight = R.id.memberCoverGuideLine2
                }
                /**
                 * Titlebar
                 */
                titleBar {
                }.addBackButton(true)

                /**
                 * Register
                 */
                button {
                    R.id.memberCoverRegister
                    textResource = R.string.member_cover_register
                    textColorResource = R.color.color_white
                    backgroundResource = R.drawable.button_cancel
                    onClick { MainActivity[ctx].navigateTo(MemberScanCardKey()) }
                }.lparams(0, wrapContent) {
                    topToTop = R.id.memberCoverGuideLine3
                    bottomToBottom = PARENT_ID
                    leftToLeft = PARENT_ID
                    rightToRight = R.id.memberCoverGuideLine4
                    margin = dimen(R.dimen.margin_normal)
                }
                /**
                 * Login
                 */
                button {
                    textResource = R.string.member_cover_login
                    R.id.memberCoverLogin
                    backgroundResource = R.drawable.button_sure
                    onClick { MainActivity[ctx].navigateTo(MemberLoginKey()) }
                }.lparams(0, wrapContent) {
                    topToTop = R.id.memberCoverGuideLine3
                    bottomToBottom = PARENT_ID
                    rightToRight = PARENT_ID
                    leftToLeft = R.id.memberCoverGuideLine4
                    margin = dimen(R.dimen.margin_normal)
                }

            }
        }
    }
}