package com.timessquare.membercover

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import org.jetbrains.anko.AnkoContext

/**
 * Created by Mason on 9/7/2018.
 * Hita Group
 * mason.chan@sllin.com
 */
class MemberCoverFragment : BaseFragment() {

    lateinit var mView: MemberCoverUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberCoverUI()

        return mView.createView(AnkoContext.create(activity!!, this))

    }
}