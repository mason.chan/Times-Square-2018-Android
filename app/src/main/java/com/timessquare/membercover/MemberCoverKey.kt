package com.timessquare.membercover

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 9/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MemberCoverKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberCoverKey))

    override fun createFragment(): BaseFragment = MemberCoverFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}