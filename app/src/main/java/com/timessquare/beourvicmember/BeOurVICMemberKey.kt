package com.timessquare.beourvicmember

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 27/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class BeOurVICMemberKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(BeOurVICMemberKey))

    override fun createFragment(): BaseFragment = BeOurVICMemberFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}