package com.timessquare.beourvicmember

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.BaseFragment
import com.timessquare.R
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 27/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class BeOurVICMemberFragment: BaseFragment() {

    lateinit var mView: BeOurVICMemberUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = BeOurVICMemberUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(view.context).load(R.drawable.ic_vic).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(mView.vicVic)
        Glide.with(view.context).load(R.drawable.ic_bvic).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(mView.vicBvic)

    }
}