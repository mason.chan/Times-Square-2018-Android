package com.timessquare.beourvicmember

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import com.timessquare.R
import com.timessquare.R.id.vicGvic
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

/**
 *   Created by Mason on 27/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class BeOurVICMemberUI() : AnkoComponent<BeOurVICMemberFragment> {

    lateinit var vicVic: ImageView
    lateinit var vicBvic: ImageView
    lateinit var vicGvic: ImageView

    override fun createView(ui: AnkoContext<BeOurVICMemberFragment>): View {
        return with(ui) {
            constraintLayout() {
                titleBar {
                    id = R.id.BeOurVICMemberTitleBar
                }.addBackgroundColor(R.color.color_cover).addBackButton().addTitle(R.string.be_our_vic_member_title)
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = PARENT_ID
                }
                scrollView {
                    verticalLayout {
                        padding = dimen(R.dimen.padding_item)
                        setGravity(Gravity.CENTER_HORIZONTAL)
                        cardView {
                            radius = 20f
                            vicVic = imageView() {
                                id = R.id.vicVic
                                adjustViewBounds = true
                            }
                        }.lparams() {
                            margin = dimen(R.dimen.margin_small)
                        }
                        textView() {
                            textResource = R.string.be_our_vic_member_vic_message
                            textSizeDimen = R.dimen.text_normal
                        }.lparams(){
                            margin = dimen(R.dimen.margin_small)
                        }
                        cardView {
                            radius = 20f
                            vicBvic = imageView() {
                                id = R.id.vicBvic
                                adjustViewBounds = true
                            }
                        }.lparams() {
                            margin = dimen(R.dimen.margin_small)
                        }
                        textView() {
                            textResource = R.string.be_our_bvic_member_vic_message
                            textSizeDimen = R.dimen.text_normal
                        }.lparams(){
                            margin = dimen(R.dimen.margin_small)
                        }
                    }.lparams(matchParent, wrapContent) {

                    }
                }.lparams(matchParent, matchConstraint) {
                    topToBottom = R.id.BeOurVICMemberTitleBar
                    bottomToBottom = PARENT_ID
                }
            }
        }
    }
}
