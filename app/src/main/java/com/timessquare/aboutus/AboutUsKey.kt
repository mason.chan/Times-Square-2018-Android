package com.timessquare.aboutus

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 5/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class AboutUsKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(AboutUsKey))

    override fun createFragment(): BaseFragment = AboutUsFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}