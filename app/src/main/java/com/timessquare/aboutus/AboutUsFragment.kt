package com.timessquare.aboutus

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.BaseFragment
import com.timessquare.R
import com.timessquare.apiservice.Path
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx

/**
 *   Created by Mason on 5/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class AboutUsFragment : BaseFragment() {

    lateinit var mView: AboutUsUI
    var ogcioURL = "https://www.ogcio.gov.hk/"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = AboutUsUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(ctx).load(Path.PUBLIC_LMAGE_URL + Path.ABOUT_US_IMAGE_URL).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(mView.aboutUsImageView)

        mView.ogcioImageView.onClick { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(ogcioURL))) }
    }
}