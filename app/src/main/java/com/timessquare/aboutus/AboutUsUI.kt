package com.timessquare.aboutus

import android.support.constraint.ConstraintLayout
import android.text.Html
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import com.timessquare.R
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.abc_alert_dialog_material.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

/**
 *   Created by Mason on 5/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class AboutUsUI() : AnkoComponent<AboutUsFragment> {
    lateinit var ogcioImageView: ImageView
    lateinit var aboutUsImageView: ImageView
    override fun createView(ui: AnkoContext<AboutUsFragment>): View {
        return with(ui) {
            constraintLayout() {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
            verticalLayout {
                titleBar {
                }.addTitle(R.string.about_us_title).addBackgroundColor(R.color.color_cover).addMenuButton()
                scrollView {
                    verticalLayout {
                        aboutUsImageView = imageView {
                            //                            imageResource = R.drawable.about_cover
                            adjustViewBounds = true
                        }
                        textView {
                            padding = dimen(R.dimen.padding_small)
                            text = Html.fromHtml(resources.getString(R.string.about_us_content))
                        }
                        ogcioImageView = imageView {
                            imageResource = R.drawable.ic_ogcio2
                            adjustViewBounds = true
                        }.lparams(wrapContent, dimen(R.dimen.logo_double_height)) {
                            margin = dimen(R.dimen.margin_normal)
                            gravity = Gravity.LEFT

                        }
                        textView {
                            padding = dimen(R.dimen.padding_small)
                            text = Html.fromHtml(resources.getString(R.string.about_us_content2))
                        }
                    }
                }
            }.lparams(matchParent, matchConstraint) {

            }
            }

        }
    }
}