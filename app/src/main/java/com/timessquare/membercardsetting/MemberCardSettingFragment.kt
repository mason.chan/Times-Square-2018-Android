package com.timessquare.membercardsetting

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log.i
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.member.MemberKey
import com.timessquare.utils.Constant
import com.timessquare.utils.IOSDialog
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import com.timessquare.apiservice.datamodel.MemberLoginModel
import com.timessquare.database.entity.MemberEntity
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx

/**
 *   Created by Mason on 16/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberCardSettingFragment : BaseFragment() {

    val vicType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: -1 }

    lateinit var mView: MemberCardSettingUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mView = MemberCardSettingUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView.remove.onClick { dialog(view.context).show() }
    }

    fun dialog(ctx: Context): IOSDialog {
        return IOSDialog.Builder(ctx).setMessage(R.string.member_card_setting_confirm_remove_card)
                .setNegativeButton(R.string.member_card_setting_confirm_remove_card_cancel, R.color.color_black,
                        DialogInterface.OnClickListener { dialog, which ->
                            //                            dialog.dismiss()
                        }).setPositiveButton(R.string.member_card_setting_confirm_remove_card_remove, R.color.color_text_red,
                        DialogInterface.OnClickListener { dialog, which ->
                            //                                                        dialog.dismiss()
                            /**
                             * Delete Card
                             */
                            var codeId = -1
                            var member: MemberEntity = MainActivity[ctx].database.memberDao().getFirst()
                            member.card!!.forEach {
                                if (it.cardTypeId == vicType) {
                                    codeId = it.cardId
                                }
                            }
                            MainActivity[ctx].removeEcard(codeId)
                                    .subscribeOn(Schedulers.io())
                                    .subscribeBy(
                                            onComplete = {
                                                i("MemberCardSetting    ", " onComplete 1")
                                            },
                                            onNext = {

                                                MainActivity[ctx].login()
                                                        .subscribeOn(Schedulers.io())
                                                        .doFinally { }
                                                        .subscribeBy(
                                                                onNext = {
                                                                    i("MemberCardSetting    ", " onNext 2")
                                                                    MainActivity[ctx].updateMemberData(it)
                                                                },
                                                                onComplete = {
                                                                    i("MemberCardSetting    ", " onComplete 2")
                                                                    MainActivity[ctx].runOnUiThread {
                                                                        MainActivity[ctx].AlertDialog("Success", "go to login page.", true)
                                                                    }
                                                                },
                                                                onError = {
                                                                    i("MemberCardSetting    ", " onError 2")
                                                                }
                                                        )
                                            },
                                            onError = {
                                                i("MemberCardSetting    ", "onError 1")

                                            }
                                    )
                            MainActivity[ctx].replaceHistoryWithIndex(MemberKey())
                        })
                .create()

    }
}