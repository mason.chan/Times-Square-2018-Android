package com.timessquare.membercardsetting

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.v7.widget.CardView
import android.view.View
import android.widget.TextView
import com.timessquare.R
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

/**
 *   Created by Mason on 16/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberCardSettingUI : AnkoComponent<MemberCardSettingFragment> {

    lateinit var remove : CardView

    override fun createView(ui: AnkoContext<MemberCardSettingFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = PARENT_ID
                }
                titleBar{
                    id = R.id.member_card_setting_title_bar
                }.addBackgroundColor(R.color.color_cover).addBackButton().addTitle(R.string.member_card_title)
                remove = include<CardView>(R.layout.settings_item) {
                    var text_view = find<TextView>(R.id.text_view)
                    text_view.textResource = R.string.member_card_setting_remove_card
                }.lparams(){
                    topToBottom = R.id.member_card_setting_title_bar
                }
            }
        }
    }
}