package com.timessquare.membercardsetting

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 16/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class MemberCardSettingKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberCardSettingKey))

    override fun createFragment(): BaseFragment = MemberCardSettingFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}