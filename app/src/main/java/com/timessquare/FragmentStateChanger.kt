package com.timessquare

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log.i
import com.timessquare.menu.MenuKey
import com.zhuinden.simplestack.StateChange

/**
 *   Created by Mason on 15/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class FragmentStateChanger(
        private val fragmentManager: FragmentManager,
        private val containerId: Int
) {
    fun handleStateChange(stateChange: StateChange) {

        val fragmentTransaction = fragmentManager.beginTransaction().apply {
            var nkey = stateChange.getNewState<BaseKey>().lastOrNull()
            val okey = stateChange.getPreviousState<BaseKey>().lastOrNull()
            i("key", "--------------------------------------------------------------------------------------------------")

            if (nkey == MenuKey()) {
                when (stateChange.direction) {
                    StateChange.FORWARD -> {
                        i("nkey", "FORWARD")
                        setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_to_bottom, android.R.anim.fade_in, android.R.anim.fade_out)
                    }
                    StateChange.BACKWARD -> {
                        i("nkey", "BACKWARD")
                        setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_to_top, android.R.anim.fade_out, android.R.anim.fade_in)
                    }
                }
            } else if (okey == MenuKey()){
                //replace
                setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_to_top, android.R.anim.fade_out, android.R.anim.fade_in)

                when (stateChange.direction) {
                    StateChange.FORWARD -> {
                        i("okey", "FORWARD")
                        setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in, R.anim.slide_in_bottom, R.anim.slide_out_to_top)
                    }
                    StateChange.BACKWARD -> {
                        i("okey", "BACKWARD")
                        setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_to_top, android.R.anim.fade_out, android.R.anim.fade_in)
                    }
                }
            } else {
                when (stateChange.direction) {
                    StateChange.FORWARD -> {
                        i("nokey", "FORWARD")
                        setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_out_to_left, R.anim.slide_in_from_right)
                    }
                    StateChange.BACKWARD -> {
                        i("nokey", "BACKWARD")
                        setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    }
                }
            }
            val previousState = stateChange.getPreviousState<BaseKey>()
            val newState = stateChange.getNewState<BaseKey>()

            for (oldKey in previousState) {
                i("FragmentStateChanger", "oldKey   " + oldKey.tag)
                val fragment = fragmentManager.findFragmentByTag(oldKey.fragmentTag)
                if (fragment != null) {
                    if (!newState.contains(oldKey)) {
                        remove(fragment)
                    } else if (!fragment.isDetached) {
                        detach(fragment)
                    }
                }
            }
            for (newKey in newState) {
                i("FragmentStateChanger", "newKey   " + newKey.tag)
                var fragment: Fragment? = fragmentManager.findFragmentByTag(newKey.fragmentTag)
                if (newKey == stateChange.topNewState<Any>()) {
                    if (fragment != null) {
                        if (fragment.isDetached) {
                            attach(fragment)
                        }
                    } else {
                        fragment = newKey.newFragment()
                        add(containerId, fragment, newKey.fragmentTag)
                    }
                } else {
                    if (fragment != null && !fragment.isDetached) {
                        detach(fragment)
                    }
                }
            }
        }
        fragmentTransaction.commitAllowingStateLoss()
    }
}