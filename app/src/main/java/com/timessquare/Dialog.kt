package com.timessquare

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.timessquare.utils.Constant
import com.timessquare.utils.IOSDialog

/**
 *   Created by Mason on 8/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

open class Dialog(var activity: Activity) : IOSDialog(activity) {
    /**
     * Dialog implementation
     */
    lateinit var alertDialog: AlertDialog
    var isDialogRunning = false

    fun popDialog(msgId : Int, buttonMsg : String? = null, cancelable : Boolean) {
        popDialog(msgId = context.getString(msgId),  buttonMsg = buttonMsg, cancelable = cancelable)
    }

    fun popDialog(msgId : String, buttonMsg : String? = null, cancelable : Boolean) {
        // dismiss if it is showing
        dismissDialog()
        isDialogRunning = true

        activity.runOnUiThread {
            alertDialog = AlertDialog.Builder(activity!!)
                    .setMessage(msgId)
                    .setCancelable(cancelable)
                    .create()
            buttonMsg?.run { alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, this, { _, _ -> }) }
            alertDialog.show()
        }

    }

    fun dismissDialog(){
        if (::alertDialog.isInitialized && alertDialog.isShowing) {
            isDialogRunning = false
            alertDialog.dismiss()
        }
    }

    fun AlertDialog(title: Int, message: Int): IOSDialog {
        return IOSDialog.Builder(activity!!)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(R.string.member_register_register_failed_dialog_ok, R.color.color_text_red,
                        DialogInterface.OnClickListener { dialog, which ->
                        })
                .create()
    }
}