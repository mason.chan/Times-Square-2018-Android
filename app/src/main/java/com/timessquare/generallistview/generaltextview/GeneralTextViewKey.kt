package com.timessquare.generallistview.generaltextview

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class GeneralTextViewKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(GeneralTextViewKey))

    override fun createFragment(): BaseFragment = GeneralTextViewFragment()

    override fun stackIdentifier(): String = ""

    companion object {}
}