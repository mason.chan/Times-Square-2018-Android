package com.timessquare.generallistview.generaltextview

import android.graphics.Typeface
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.View
import com.timessquare.R
import com.timessquare.views.textViewBaskerville
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.abc_alert_dialog_material.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class GeneralTextViewUI(val titleResource: Int, val termsAndConditions: String) : AnkoComponent<GeneralTextViewFragment> {

    override fun createView(ui: AnkoContext<GeneralTextViewFragment>): View {

        return with(ui) {
            constraintLayout {
                // Background
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(matchParent, wrapContent) {
                    bottomToBottom = PARENT_ID
                }
                // TitleBar
                titleBar {
                    id = R.id.GeneralTextViewTitleBar
                }
                        .addBackButton()
                        .addTitle(titleResource)
                        .addBackgroundColor(R.color.color_cover)
                        .lparams(matchParent, wrapContent)
                scrollView {
                    //Content
                    textView {
                        text = termsAndConditions
                        padding = dimen(R.dimen.padding_small)
                    }.lparams(matchParent, matchParent)
                }.lparams(matchParent, matchConstraint) {
                    topToBottom = R.id.GeneralTextViewTitleBar
                    bottomToBottom = PARENT_ID
                }

            }
        }
    }
}