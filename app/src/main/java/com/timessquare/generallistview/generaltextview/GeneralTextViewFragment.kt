package com.timessquare.generallistview.generaltextview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.utils.Constant
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class GeneralTextViewFragment : BaseFragment() {

    private val titleResource: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: -1 }
    private val termsAndConditions: String by lazy { arguments?.getString(Constant.TAG_INTENT2) ?: "" }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return GeneralTextViewUI(titleResource, termsAndConditions).createView(AnkoContext.create(activity!!, this))
    }

}