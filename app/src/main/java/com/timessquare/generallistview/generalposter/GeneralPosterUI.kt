package com.timessquare.generallistview.generalposter

import android.support.constraint.ConstraintLayout
import android.view.Gravity
import android.view.View
import android.widget.ProgressBar
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.generallistview.generaltextview.GeneralTextViewKey
import com.timessquare.utils.Constant
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.sdk25.coroutines.onClick
import uk.co.senab.photoview.PhotoView

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class GeneralPosterUI(val termsAndConditions: String) : AnkoComponent<GeneralPosterFragment> {

    lateinit var progressBar: ProgressBar
    lateinit var photoView: PhotoView

    override fun createView(ui: AnkoContext<GeneralPosterFragment>): View {
        return with(ui) {
            constraintLayout {
                backgroundColorResource = R.color.color_black
                titleBar {
                    id = R.id.whatsUpPosterTitleBar
                }
                        .addBackButton(true)
                        .lparams(matchParent, wrapContent)

                // Progress Bar
                progressBar = progressBar {
                    id = R.id.whatsUpPosterProgressBar
                }.lparams(wrapContent, wrapContent) {
                    topToBottom = R.id.whatsUpPosterTitleBar
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
                    rightToRight = ConstraintLayout.LayoutParams.PARENT_ID

                }
                // Photo View
                photoView = customView<PhotoView> {
                    id = R.id.whatsUpPosterPhotoView
                }.lparams(matchParent, matchConstraint) {
                    topToBottom = R.id.whatsUpPosterTitleBar
                    if(termsAndConditions.isNotEmpty()) {
                        bottomToTop = R.id.whatsUpPosterTermsAndConditions
                    } else {
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }
                }
                if (termsAndConditions.isNotEmpty()) {
                    // Bottom bar
                    textView {
                        id = R.id.whatsUpPosterTermsAndConditions
                        textResource = R.string.happenings_poster_terms_and_conditions
                        backgroundColorResource = R.color.color_white
                        gravity = Gravity.CENTER
                        padding = dimen(R.dimen.padding_small)
                        onClick { MainActivity.get(view.context).navigateTo(GeneralTextViewKey().withArgs {
                            putInt(Constant.TAG_INTENT, R.string.happenings_terms_and_conditions_title)
                            putString(Constant.TAG_INTENT2, termsAndConditions)
                        }) }
                    }.lparams(matchParent, wrapContent) {
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }
                }
            }
        }
    }
}