package com.timessquare.generallistview.generalposter

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.daimajia.slider.library.R.id.rect
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.apiservice.Path
import com.timessquare.utils.Constant
import org.jetbrains.anko.AnkoContext
import uk.co.senab.photoview.PhotoViewAttacher
import java.lang.Exception
import java.util.logging.Level.ALL

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class GeneralPosterFragment : BaseFragment() {

    private val posterUrl: String by lazy { arguments?.getString(Constant.TAG_INTENT) ?: "" }
    private val termsAndConditions: String by lazy {
        arguments?.getString(Constant.TAG_INTENT2) ?: ""
    }

    lateinit var mView: GeneralPosterUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = GeneralPosterUI(termsAndConditions)
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        i("PosterFragment", "termsAndConditions  " + termsAndConditions)
        i("PosterFragment", "posterUrl  :   " + Path.PRO_IMAGE_URL + posterUrl)

        Glide.with(view.context).asDrawable().load(Path.PRO_IMAGE_URL + posterUrl).thumbnail(0.2f).apply(RequestOptions().override(1600, 1600).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter())
                .listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        mView.progressBar.visibility = View.GONE
                        i("PosterFragment", "onResourceReady  " + resource!!.intrinsicWidth)
                        i("PosterFragment", "onResourceReady  " + resource!!.intrinsicHeight)
                        return false
                    }

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        mView.progressBar.visibility = View.GONE
                        mView.photoView.setImageResource(R.drawable.default_pic_black)
                        i("PosterFragment", "onLoadFailed  " + e)
                        return true
                    }
                })
                .into(mView.photoView)


    }
}
