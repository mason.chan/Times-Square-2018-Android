package com.timessquare.generallistview.generalposter

import android.annotation.SuppressLint
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class GeneralPosterKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(GeneralPosterKey))

    override fun createFragment() = GeneralPosterFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}