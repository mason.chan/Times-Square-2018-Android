package com.timessquare.generallistview

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.generallistview.generalposter.GeneralPosterKey
import com.timessquare.utils.Constant
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

abstract class GeneralListViewFragment : BaseFragment() {

    private var mFastItemAdapter: FastItemAdapter<GeneralListViewItem>? = null

    lateinit var mView : GeneralListViewUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = GeneralListViewUI(setTitle())
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFastItemAdapter = FastItemAdapter()
        mFastItemAdapter!!.withOnClickListener { v, adapter, item, position ->
            MainActivity.get(view.context).navigateTo(GeneralPosterKey().withArgs {
                putString(Constant.TAG_INTENT, item.mPosterUrl)
                putString(Constant.TAG_INTENT2, item.mTermsAndConditions)
            })
            false
        }
        mView.recyclerView.layoutManager = LinearLayoutManager(context)
        mView.recyclerView.adapter = mFastItemAdapter
        mFastItemAdapter!!.add(addData())
    }
        abstract fun addData(): List<GeneralListViewItem>
        abstract fun setTitle(): Int

}