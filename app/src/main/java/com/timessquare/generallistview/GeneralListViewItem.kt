package com.timessquare.generallistview

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.R
import com.timessquare.apiservice.Path
import com.mikepenz.fastadapter.items.AbstractItem

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class GeneralListViewItem(
        var mName: String,
        var mImageUrl: String,
        var mPosterUrl: String? = null,
        var mTermsAndConditions: String? = null
) : AbstractItem<GeneralListViewItem, GeneralListViewItem.ViewHolder>() {


    override fun getType(): Int {
        return R.id.GeneralListViewID
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.general_list_view
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        val ctx = holder.itemView.context
        //define our data for the view
        holder.textView!!.text = mName
        Glide.with(ctx).load(Path.PRO_IMAGE_URL + mImageUrl).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).into(holder.imageView!!)
    }

    /**
     * General List View Item
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {

        var textView: TextView
        var imageView: ImageView

        init {
            textView = view.findViewById(R.id.text_view)
            imageView = view.findViewById(R.id.image_view)
        }
    }
}