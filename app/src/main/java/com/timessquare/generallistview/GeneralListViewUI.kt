package com.timessquare.generallistview

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import com.timessquare.R
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class GeneralListViewUI(val titleResource: Int) : AnkoComponent<GeneralListViewFragment> {

    lateinit var recyclerView: RecyclerView

    override fun createView(ui: AnkoContext<GeneralListViewFragment>): View {
        return with(ui) {
            constraintLayout() {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    titleBar {

                        id = R.id.GeneralListViewTitle
                    }
                            .addMenuButton()
                            .addBackgroundColor(R.color.color_cover)
                            .addTitle(titleResource)
                    recyclerView = recyclerView {
                    }.lparams(matchParent, matchParent) {
                    }
                }.lparams(matchParent, matchConstraint) {

                }
            }
        }
    }
}