package com.timessquare.selectlanguage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.index.IndexKey
import com.timessquare.utils.LocaleHelper
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 25/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class SelectLanguageFragment : BaseFragment(){

    @BindView(R.id.selectLanguageTraditionalChinese) lateinit var textViewTC : TextView
    @BindView(R.id.selectLanguageEnglish) lateinit var textViewEN : TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = SelectLanguageUI().createView(AnkoContext.Companion.create(activity!!, this))
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textViewTC.onClick {
//            MainActivity.get(view.context).updateLanguage(getString(R.string.language_hk))
            MainActivity[view.context].setLanguage(LocaleHelper.hkLocale())
            MainActivity.get(view.context).replaceHistory(IndexKey())}
        textViewEN.onClick {
//            MainActivity.get(view.context).updateLanguage(getString(R.string.language_en))
            MainActivity[view.context].setLanguage(LocaleHelper.englishLocale())
            MainActivity.get(view.context).replaceHistory(IndexKey())}
    }
}