package com.timessquare.selectlanguage

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.Gravity
import android.view.View
import com.timessquare.R
import kotlinx.android.synthetic.main.notification_template_lines_media.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.guideline

/**
 *   Created by Mason on 25/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class SelectLanguageUI() : AnkoComponent<SelectLanguageFragment> {
    override fun createView(ui: AnkoContext<SelectLanguageFragment>): View {
        return with(ui) {
            constraintLayout() {
                backgroundColorResource = R.color.color_cover
                guideline {
                    id = R.id.selectLanguageGuideLine
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.HORIZONTAL
                    guidePercent = 0.5f
                }

                button {
                    id = R.id.selectLanguageTraditionalChinese
                    text = "繁體中文"
                    textSizeDimen = R.dimen.text_normal
                    verticalPadding = dimen(R.dimen.padding_small)
                    horizontalPadding = dimen(R.dimen.padding_double)
                    gravity = Gravity.CENTER_HORIZONTAL
                    backgroundResource = R.drawable.button_cancel
                }.lparams(wrapContent, wrapContent) {
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                    bottomToTop = R.id.selectLanguageGuideLine
                    margin = dimen(R.dimen.margin_normal)

                }
                button {
                    id = R.id.selectLanguageEnglish
                    text = "English"
                    verticalPadding = dimen(R.dimen.padding_small)
                    horizontalPadding = dimen(R.dimen.padding_double)
                    textSizeDimen = R.dimen.text_normal
                    gravity = Gravity.CENTER_HORIZONTAL
                    backgroundResource = R.drawable.button_cancel
                }.lparams(wrapContent, wrapContent) {
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                    topToBottom = R.id.selectLanguageGuideLine
                    margin = dimen(R.dimen.margin_normal)

                }
            }
        }
    }
}