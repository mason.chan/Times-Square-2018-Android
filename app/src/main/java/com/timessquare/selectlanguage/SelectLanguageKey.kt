package com.timessquare.selectlanguage

import android.annotation.SuppressLint
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 25/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class SelectLanguageKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(SelectLanguageKey))

    override fun createFragment() = SelectLanguageFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}