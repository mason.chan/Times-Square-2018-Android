package com.timessquare.account

import android.accounts.*
import android.content.Context
import android.os.Bundle
import android.text.TextUtils

/**
 *   Created by Mason on 3/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class TimesSquareAccountAuthenticator(private val context: Context): AbstractAccountAuthenticator(context) {

    @Throws(NetworkErrorException::class)
    override fun addAccount(response: AccountAuthenticatorResponse,
                            accountType: String,
                            authTokenType: String?,
                            requiredFeatures: Array<out String>?,
                            options: Bundle?
    ): Bundle? {
        // TODO: Change the redirect page to add account
/*
        val intent = Intent(context, Loading::class.java)
        intent.putExtra(Constant.ACCOUNT_TYPE, accountType)
        intent.putExtra(Constant.ACCOUNT_AUTH_TOKEN_TYPE, authTokenType)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
*/

        val bundle = Bundle()
//        bundle.putParcelable(AccountManager.KEY_INTENT, intent)

        return bundle
    }

    override fun getAuthTokenLabel(p0: String?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Throws(NetworkErrorException::class)
    override fun getAuthToken(response: AccountAuthenticatorResponse,
                              account: Account,
                              authTokenType: String,
                              bundle: Bundle
    ): Bundle {
        val accountManager = AccountManager.get(context)

        val authToken = accountManager.peekAuthToken(account, authTokenType)

        if (TextUtils.isEmpty(authToken)) {
            /**
             * Get the user auth from the server.
             */
        } else {
            /**
             * THe user auth is stored.
             */
            val result = Bundle()
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name!!)
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type!!)
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            return result
        }
        /**
         * The user needs to login again or sign up.
         */
//        val intent = Intent(context, Login::class.java)
/*
        intent.putExtra("ACCOUNT_TYPE", account.type!!)
        intent.putExtra("full_access", authTokenType)

        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
*/

        return bundle
    }

    override fun updateCredentials(p0: AccountAuthenticatorResponse?, p1: Account?, p2: String?, p3: Bundle?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun confirmCredentials(p0: AccountAuthenticatorResponse?, p1: Account?, p2: Bundle?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hasFeatures(p0: AccountAuthenticatorResponse?, p1: Account?, p2: Array<out String>?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun editProperties(p0: AccountAuthenticatorResponse?, p1: String?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}