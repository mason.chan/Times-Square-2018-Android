package com.timessquare.account

import android.app.Service
import android.content.Intent
import android.os.IBinder

/**
 *   Created by Mason on 3/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class TimesSquareAuthenticatorService: Service() {
    override fun onBind(p0: Intent?): IBinder {
        val authenticator = TimesSquareAccountAuthenticator(this)
        return authenticator.iBinder
    }
}