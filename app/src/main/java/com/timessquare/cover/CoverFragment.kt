package com.timessquare.cover

import android.os.Bundle
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R

/**
 *   Created by Mason on 15/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class CoverFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        i("CoverFragment", "onCreateView" )
        return inflater.inflate(R.layout.cover_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        i("CoverFragment", "onViewCreated" )

        // Get init data
        MainActivity.get(view.context).dataLocalization()

        val coverKey = getKey<CoverKey>()
    }
}