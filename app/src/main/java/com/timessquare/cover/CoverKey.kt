package com.timessquare.cover

import android.annotation.SuppressLint
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 15/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class CoverKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(CoverKey))

    override fun createFragment() = CoverFragment()

    override fun stackIdentifier(): String = ""
    companion object {}
}