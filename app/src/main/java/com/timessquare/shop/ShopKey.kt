package com.timessquare.shop

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 6/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class ShopKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(ShopKey))

    override fun createFragment(): BaseFragment = ShopFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}