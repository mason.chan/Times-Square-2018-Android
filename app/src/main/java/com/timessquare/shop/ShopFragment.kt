package com.timessquare.shop

import com.timessquare.R
import com.timessquare.shopanddine.ShopDineFragment

/**
 *   Created by Mason on 6/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopFragment : ShopDineFragment() {

    override fun setTitle(): Int = R.string.shop_title

    override fun isShopNotFood(): Boolean = true
}