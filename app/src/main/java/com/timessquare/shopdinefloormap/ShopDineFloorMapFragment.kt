package com.timessquare.shopdinefloormap

import android.os.Bundle
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.apiservice.Path
import com.timessquare.utils.Constant
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 26/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopDineFloorMapFragment : BaseFragment() {

    val shopNumber: String by lazy { arguments?.getString(Constant.TAG_INTENT) ?: "" }

    val shopFloor: Int by lazy { arguments?.getInt(Constant.TAG_INTENT2) ?: -1 }

    lateinit var mView: ShopDineFloorMapUI

    var lang = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = ShopDineFloorMapUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lang = if (MainActivity[view.context].isEnglish()) "en" else "zh"
        var url: String = Path.PRO_FLOOR_MAP_URL + lang + Path.FLOOR_MAP_FLOOR_URL + shopFloor + Path.FLOOR_MAP_SHOP_ID_URL + shopNumber
       i("ShopDineFloorMap", "onViewCreated     " + url)
        mView.webView.settings.javaScriptEnabled = true
        mView.webView.loadUrl(url)
    }
}
