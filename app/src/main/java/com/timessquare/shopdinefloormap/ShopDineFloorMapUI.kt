package com.timessquare.shopdinefloormap

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.View
import android.webkit.WebView
import com.timessquare.R
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

/**
 *   Created by Mason on 26/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class ShopDineFloorMapUI() : AnkoComponent<ShopDineFloorMapFragment> {

    lateinit var webView : WebView
    override fun createView(ui: AnkoContext<ShopDineFloorMapFragment>): View {
        return with(ui) {
            constraintLayout() {
                titleBar() {
                    id = R.id.titlebar
                }.addTitle(R.string.shop_dine_floor_map_title)
                        .addBackButton()
                        .addBackgroundColor(R.color.color_cover)
                        .lparams(matchParent, wrapContent) {
                            leftToLeft = PARENT_ID
                            rightToRight = PARENT_ID
                            topToTop = PARENT_ID
                        }
                webView = webView() {
                }.lparams(width = matchParent, height = matchConstraint) {
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                    topToBottom = R.id.titlebar
                    bottomToBottom = PARENT_ID
                }
            }
        }
    }
}