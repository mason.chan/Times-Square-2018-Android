package com.timessquare.shopdinefloormap

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 26/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class ShopDineFloorMapKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(ShopDineFloorMapKey))

    override fun createFragment(): BaseFragment = ShopDineFloorMapFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}