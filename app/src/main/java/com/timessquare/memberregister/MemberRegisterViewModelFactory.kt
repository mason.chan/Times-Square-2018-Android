package com.timessquare.memberregister

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import com.timessquare.apiservice.ApiService
import android.arch.lifecycle.ViewModel



/**
 *   Created by Mason on 14/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberRegisterViewModelFactory(
        private val mApplication: Application,
        private val mApiService: ApiService) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MemberRegisterViewModel(mApplication, mApiService) as T
    }

}