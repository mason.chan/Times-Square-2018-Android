package com.timessquare.memberregister

import android.support.constraint.ConstraintLayout
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.text.InputType
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.memberlogin.MemberLoginKey
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.abc_alert_dialog_material.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.design.textInputLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 13/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberRegisterUI : AnkoComponent<MemberRegisterFragment> {
    lateinit var mECardCodeTextView: TextView
    lateinit var mNameEditText: EditText
    lateinit var mNameTextInputLayout: TextInputLayout
    lateinit var mEmailEditText: EditText
    lateinit var mEmailTextInputLayout: TextInputLayout
    lateinit var mPasswordEditText: EditText
    lateinit var mPasswordTextInputLayout: TextInputLayout
    lateinit var mConfirmPasswordEditText: EditText
    lateinit var mconfirmPasswordTextInputLayout: TextInputLayout

    lateinit var mCompleteRegistrationButton: Button

    override fun createView(ui: AnkoContext<MemberRegisterFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                titleBar {
                    id = R.id.memeber_register_title_bar
                }.addBackgroundColor(R.color.color_cover).addBackButton().addTitle(R.string.member_register_title)
                scrollView {
                    verticalLayout {
                        padding = dimen(R.dimen.padding_small)
                        mECardCodeTextView = textView {
                        }
                        /**
                         * Name
                         */
                        textView(R.string.member_register_name) {
                            textColorResource = R.color.color_black
                            textSizeDimen = R.dimen.text_normal
                        }
                        mNameEditText = include<EditText>(R.layout.car_park_edit_text) {
                            hintResource = R.string.member_register_name_hint
                        }
                        mNameTextInputLayout = textInputLayout {  }

                        /**
                         * Email
                         */
                        textView(R.string.member_register_email) {
                            textColorResource = R.color.color_black
                            textSizeDimen = R.dimen.text_normal
                        }
                        mEmailEditText  = include<EditText>(R.layout.car_park_edit_text) {
                            hintResource = R.string.member_register_email_hint
                        }
                        mEmailTextInputLayout = textInputLayout {  }
                        /**
                         * Password
                         */
                        textView(R.string.member_register_password) {
                            textColorResource = R.color.color_black
                            textSizeDimen = R.dimen.text_normal
                        }
                        mPasswordEditText = include<EditText>(R.layout.car_park_edit_text) {
                            inputType = TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
                            hintResource = R.string.member_register_password_hint
                        }
                        mPasswordTextInputLayout = textInputLayout {  }
                        /**
                         * Confirm Password
                         */
                        textView(R.string.member_register_confirm_password) {
                            textColorResource = R.color.color_black
                            textSizeDimen = R.dimen.text_normal
                        }
                        mConfirmPasswordEditText = include<EditText>(R.layout.car_park_edit_text) {
                            inputType = TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
                            hintResource = R.string.member_register_confirm_password_hint
                        }
                        mconfirmPasswordTextInputLayout = textInputLayout {  }
                        /**
                         * Complete Registration
                         */
                        mCompleteRegistrationButton = button {
                            textResource = R.string.member_register_complete_registration
                            textSizeDimen = R.dimen.text_menu_size
                            backgroundResource = R.drawable.button_sure
                        }.lparams(matchParent, wrapContent) {
                            margin = dimen(R.dimen.margin_double)
                        }
                    }
                }.lparams(matchParent, wrapContent){
                    topToBottom = R.id.memeber_register_title_bar
                }

            }
        }
    }
}