package com.timessquare.memberregister

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log.i
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.BuildConfig
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.memberactivation.MemberActivationKey
import com.timessquare.utils.Constant
import com.timessquare.utils.IOSDialog
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.regex.Pattern

/**
 *   Created by Mason on 13/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberRegisterFragment : BaseFragment() {

    private lateinit var mView: MemberRegisterUI

    val ecardCode: String by lazy { arguments?.getString(Constant.TAG_INTENT) ?: "" }

    lateinit var mViewModel: MemberRegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberRegisterUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel = ViewModelProviders.of(
                this, MemberRegisterViewModelFactory(
                activity!!.application, MainActivity[activity!!].apiService)
        ).get(MemberRegisterViewModel::class.java)


        mViewModel.cardCode.value = ecardCode

        mViewModel.cardVerify()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { popDialog(msgId = R.string.member_register_card_code_verify_loading_dialog, cancelable = false) }
                .doFinally { dismissDialog() }
                .subscribeBy(
                        onNext = {
                            i("mViewModel.cardVerify()", "onNext   " + it.message + it.error_message)
                            if (it.message.isNullOrBlank()) {
                                activity!!.runOnUiThread {
                                    invalidCardCodeDialog(view.context, it.error_message).show()
                                }
                            }
                        },
                        onError = {
                            i("mViewModel.cardVerify()", "onError   " + it.message)
                            activity!!.runOnUiThread {
                                invalidCardCodeDialog(view.context, it.message!!).show()
                            }
                        }
                )

        mView.mCompleteRegistrationButton.onClick {
            attemptRegister(view.context)
        }
    }

    /**
     * Data Checking & Register
     */
    fun attemptRegister(ctx: Context) {
        i("attemptRegister", "isEnglish:    " + if (MainActivity[ctx].isEnglish()) "en" else "zh")
        mViewModel.name.value = mView.mNameEditText.text.toString()
        mViewModel.email.value = mView.mEmailEditText.text.toString()
        mViewModel.language.value = if (MainActivity[ctx].isEnglish()) "en" else "zh"
        mViewModel.password.value = mView.mPasswordEditText.text.toString()
        mViewModel.confirmPassword.value = mView.mConfirmPasswordEditText.text.toString()


        /**
         * Clean Error
         */
        mView.mNameTextInputLayout.error = null
        mView.mEmailTextInputLayout.error = null
        mView.mPasswordTextInputLayout.error = null
        mView.mconfirmPasswordTextInputLayout.error = null

        var namePattern = Pattern.compile("^[a-zA-Z\\s]*\$")
        var passwordPattern = Pattern.compile("^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*\$")
        /**
         * Data Checking
         */
        // Name valid
        if (mViewModel.name.value.toString().isEmpty() or !namePattern.matcher(mViewModel.name.value.toString()).matches()) {
            mView.mNameTextInputLayout.error = getString(R.string.member_register_name_error)
            return
        }
        // Email valid
        if (mViewModel.email.value.toString().isEmpty() or !Patterns.EMAIL_ADDRESS.matcher(mViewModel.email.value.toString()).matches()) {
            mView.mEmailTextInputLayout.error = getString(R.string.member_register_email_error)
            return
        }
        // Password valid
        if ((mViewModel.password.value.toString().length < 8) or !passwordPattern.matcher(mViewModel.password.value.toString()).matches()) {
            mView.mPasswordTextInputLayout.error = getString(R.string.member_register_password_error)
            return
        }
        // Confirm Passowrd valid
        if (!mViewModel.confirmPassword.value.toString().equals(mViewModel.password.value.toString())) {
            mView.mconfirmPasswordTextInputLayout.error = getString(R.string.member_register_confirm_password_error)
            return
        }
        /**
         * Attempt Register
         */
        mViewModel.attemptRegister()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { popDialog(msgId = R.string.member_register_register_loading_dialog, cancelable = false) }
                .doFinally { dismissDialog() }
                .subscribeBy(
                        onNext = {
                            if (it.message.isNullOrBlank()) {
                                activity!!.runOnUiThread {
                                    registerionFailedDialog(ctx, it.error_message!!).show()
                                }
                            } else {
                                //go to completed page
                                    MainActivity[ctx].navigateTo(MemberActivationKey())
                            }
                        },
                        onError = {
                            activity!!.runOnUiThread {
                                registerionFailedDialog(ctx, it.message!!).show()
                            }
                        }
                )


    }

    fun registerionFailedDialog(context: Context, dialogMessage: String): IOSDialog {
        return IOSDialog.Builder(context)
                .setTitle(R.string.member_register_invalid_card_dialog_title)
                .setMessage(dialogMessage)
                .setNegativeButton(R.string.member_register_register_failed_dialog_ok, R.color.color_text_red,
                        DialogInterface.OnClickListener { dialog, which ->
                        })
                .create()

    }

    fun invalidCardCodeDialog(context: Context, dialogMessage: String): IOSDialog {
        return IOSDialog.Builder(context)
                .setTitle(R.string.member_register_register_failed_dialog_title)
                .setMessage(dialogMessage)
                .setPositiveButton(R.string.member_register_invalid_card_dialog_ok, R.color.color_text_red,
                        DialogInterface.OnClickListener { dialog, which ->
                            dialog.dismiss()
                            MainActivity[context].onBackPressed()
                        })
                .create()

    }
}