package com.timessquare.memberregister

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 13/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class MemberRegisterKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberRegisterKey))

    override fun createFragment(): BaseFragment = MemberRegisterFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}