package com.timessquare.memberregister

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.timessquare.apiservice.ApiService
import com.timessquare.apiservice.datamodel.MemberRegisterModel
import com.timessquare.apiservice.datamodel.VerifyCardModel
import io.reactivex.Observable

/**
 *   Created by Mason on 14/8/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberRegisterViewModel(val context: Application, val mApiService: ApiService) : AndroidViewModel(context) {

    var cardCode: MutableLiveData<String> = MutableLiveData()
    var name: MutableLiveData<String> = MutableLiveData()
    var email: MutableLiveData<String> = MutableLiveData()
    var language: MutableLiveData<String> = MutableLiveData()
    var password: MutableLiveData<String> = MutableLiveData()
    var confirmPassword: MutableLiveData<String> = MutableLiveData()

    fun memberRegister() {

    }

    fun cardVerify(): Observable<VerifyCardModel> {
        return mApiService.verifyCardCode(cardCode.value ?: "")
    }

    fun attemptRegister(): Observable<MemberRegisterModel> {
        return mApiService.memberRegister(name.value ?: "",
                cardCode.value ?: "",
                email.value ?: "",
                language.value ?: "",
                password.value ?: "")
    }
}