package com.timessquare.happenings

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 23/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class HappeningsKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(HappeningsKey))

    override fun createFragment(): BaseFragment = HappeningsFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}