package com.timessquare.happenings

import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.database.dao.EventType
import com.timessquare.generallistview.GeneralListViewFragment
import com.timessquare.generallistview.GeneralListViewItem
import org.jetbrains.anko.support.v4.ctx
import java.util.*

/**
 *   Created by Mason on 23/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class HappeningsFragment : GeneralListViewFragment() {

    override fun setTitle(): Int {
        return R.string.happenings_title
    }

    override fun addData(): List<GeneralListViewItem> {
        /**
         * Get Raw Data
         */
        var rawData = MainActivity[ctx].database.eventDao().getEventType(EventType.whatsup.value, Date(), Date())
        /**
         * Convert to GeneralListViewItem
         */
        var result = ArrayList<GeneralListViewItem>()
        for (item in rawData) {
            if (MainActivity[ctx].isEnglish()) {
                result.add(GeneralListViewItem(item.title, item.thumb
                        ?: "", if (item.poster!!.size > 0) item.poster!![0] else "", item.terms))
            } else {
                result.add(GeneralListViewItem(item.titleZh, item.thumb
                        ?: "", if (item.posterZh!!.size > 0) item.posterZh!![0] else "", item.termsZh))
            }
        }
        return result
    }

}