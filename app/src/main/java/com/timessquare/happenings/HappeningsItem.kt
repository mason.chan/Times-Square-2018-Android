package com.timessquare.happenings

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.R
import com.mikepenz.fastadapter.items.AbstractItem

/**
 *   Created by Mason on 23/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class HappeningsItem : AbstractItem<HappeningsItem, HappeningsItem.ViewHolder>() {


    lateinit var mName: String
    lateinit var mImageUrl: String
     var mPosterUrl: String? = null
     var mTermsAndConditions: String? = null


    fun withImage(imageUrl: String): HappeningsItem {
        this.mImageUrl = imageUrl
        return this
    }

    fun withName(name: String): HappeningsItem {
        this.mName = name
        return this
    }

    fun withPosterUrl(posterUrl: String): HappeningsItem {
        this.mPosterUrl = posterUrl
        return this
    }

    fun withTermsAndConditions(termsAndConditions: String): HappeningsItem {
        this.mTermsAndConditions = termsAndConditions
        return this
    }

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override fun getType(): Int {
        return R.id.index_item_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.whats_up_item_view
    }


    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        val ctx = holder.itemView.context

        //define our data for the view
        holder.textView!!.text = mName


        Glide.with(ctx).load(mImageUrl).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).into(holder.imageView!!)
    }

    /**
     * HappeningsItem ViewHolder
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {


        lateinit var textView: TextView
        lateinit var imageView: ImageView

        init {
//            ButterKnife.bind(this, view)
            textView = view.findViewById(R.id.text_view)
            imageView = view.findViewById(R.id.image_view)
        }

    }

}