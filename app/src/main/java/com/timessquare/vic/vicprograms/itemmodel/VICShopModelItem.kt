package com.timessquare.vic.vicprograms.itemmodel

import android.view.View
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.utils.Constant
import com.timessquare.vicoffers.VICOffersKey

/**
 *   Created by Mason on 13/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICShopModelItem(override val model: VICShopModel) : VICGenericModelItem(model) {
    override fun getType(): Int = R.id.vicShopModelItemId

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(override val view: View) : VICGenericModelItem.ViewHolder(view) {
        override fun bindView(item: VICGenericModelItem, payloads: MutableList<Any>?) {
            super.bindView(item, payloads)
            item as VICShopModelItem
            vicTextView.setOnClickListener {
                MainActivity[view.context].navigateTo(VICOffersKey().withArgs {
                    putString(Constant.TAG_INTENT, item.model.title)
                    putInt(Constant.TAG_INTENT2, item.model.offerType)
                })
            }
        }

        override fun unbindView(item: VICGenericModelItem?) {
            super.unbindView(item)
        }
    }

}