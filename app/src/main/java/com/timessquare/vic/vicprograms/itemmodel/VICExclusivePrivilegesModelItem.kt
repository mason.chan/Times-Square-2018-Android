package com.timessquare.vic.vicprograms.itemmodel

import android.util.Log
import android.view.View
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.vic.vicexclusiveprivileges.VICExclusivePrivilegesKey

/**
 *   Created by Mason on 15/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICExclusivePrivilegesModelItem(override val model: VICExclusivePrivilegesModel) : VICGenericModelItem(model) {

    override fun getType(): Int = R.id.vicExclusivePrivilegesItemId

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(override val view: View) : VICGenericModelItem.ViewHolder(view) {
        override fun bindView(item: VICGenericModelItem, payloads: MutableList<Any>?) {
            super.bindView(item, payloads)
            vicTextView.setOnClickListener {
                Log.i("VIC Exclusive", "On Click test.")
                MainActivity[view.context].navigateTo(VICExclusivePrivilegesKey())
            }
        }

        override fun unbindView(item: VICGenericModelItem?) {
            super.unbindView(item)
        }
    }

}