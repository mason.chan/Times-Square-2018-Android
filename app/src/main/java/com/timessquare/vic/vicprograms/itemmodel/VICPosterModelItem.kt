package com.timessquare.vic.vicprograms.itemmodel

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.generallistview.generalposter.GeneralPosterKey
import com.timessquare.utils.Constant
import com.timessquare.vicoffers.VICOffersKey

/**
 *   Created by Mason on 13/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICPosterModelItem(override val model: VICPosterModel): VICGenericModelItem(model) {
    override fun getType(): Int = R.id.vicPosterModelItemId

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)



    class ViewHolder(override val view: View): VICGenericModelItem.ViewHolder(view) {
         override fun bindView(item: VICGenericModelItem, payloads: MutableList<Any>?) {
             // Cast the item into correct one.
             item as VICPosterModelItem
             super.bindView(item, payloads)
             vicTextView.setOnClickListener {
                 MainActivity[view.context].navigateTo(GeneralPosterKey().withArgs { putString(Constant.TAG_INTENT, item.model.posterUrl) })
             }
        }

        override fun unbindView(item: VICGenericModelItem?) {
            super.unbindView(item)
        }
    }
}