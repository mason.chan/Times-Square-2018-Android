package com.timessquare.vic.vicprograms

import android.support.constraint.ConstraintLayout
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.RecyclerView
import android.view.View
import com.timessquare.R
import com.timessquare.views.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.nestedScrollView

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICProgramsUI(var isLog: Boolean = false, var vicType: Int) : AnkoComponent<VICProgramsFragment> {

    lateinit var mNestedScrollView: NestedScrollView
    lateinit var mTitleBar: TitleBar
    lateinit var mRecyclerView: RecyclerView

    override fun createView(ui: AnkoContext<VICProgramsFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                mNestedScrollView = nestedScrollView {
                    verticalLayout {
                        imageView {
                            when (vicType) {
                                VICType.VIC.value -> imageResource = R.drawable.ic_vic
                                VICType.BVIC.value -> imageResource = R.drawable.ic_bvic
                                VICType.GVIC.value -> imageResource = R.drawable.ic_gvic
                            }
                            id = R.id.VICProgramsImageView
                            adjustViewBounds = true
                        }.lparams(matchParent, wrapContent)
                        mRecyclerView = recyclerView {
                            id = R.id.vicProgramsRecyclerView
                        }.lparams(matchParent, wrapContent)
                    }.lparams(matchParent, wrapContent)
                }
                mTitleBar = titleBar { bkc.alpha = 0f }.addBackButton(true).addBackgroundColor(R.color.color_cover)
            }
        }
    }
}