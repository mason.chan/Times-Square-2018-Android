package com.timessquare.vic.vicprograms

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.utils.Constant
import com.timessquare.vic.vicprograms.itemmodel.*
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IInterceptor
import com.mikepenz.fastadapter.adapters.ModelAdapter
import com.timessquare.database.AppDataBase
import com.timessquare.database.entity.EventEntity
import com.timessquare.utils.SParem
import com.timessquare.utils.SharedPreferenceUtils
import org.jetbrains.anko.AnkoContext
import java.util.*
import kotlin.collections.ArrayList

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class VICProgramsFragment : BaseFragment() {

    var Year_Round_Offers = 1066
    var Birthday_Offers = 1067
    var Exclusive_Privileges = 1071
    var Tourist_Monthly_Offers = 1037
    var Tourist_Birthday_Offers = 1146

    lateinit var mView: VICProgramsUI

    var hasSpecialNotice: Boolean = false

    lateinit var layoutManager: LinearLayoutManager

    lateinit var fastAdapter: FastAdapter<*>

    var modelList = ArrayList<VICGenericModel>()

    var isLog: Boolean = false
//    var isEnglish = true

    val vicType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: 0 }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = VICProgramsUI(isLog, vicType)
        Log.i("VICPrograms", "onCreateView")

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("VICPrograms", "onViewCreated")
        titleAlpha()
        addItem(MainActivity[view.context].sp, MainActivity[view.context].database, MainActivity[view.context].isEnglish())
        initRecyclerView()

    }

    fun addItem(sp: SharedPreferenceUtils, database: AppDataBase, isEnglish: Boolean) {
        // Get Login Status
        isLog = sp.getBoolanValue(SParem.Member_Logined, false)
        Log.d("VICProgram", "" + isLog)

        // Get List of Programs
        if (isLog) {
            when (vicType) {
                VICType.VIC.value -> getVICProgramesList(isEnglish, database.eventDao().getVICPrograms())
                VICType.BVIC.value -> getVICProgramesList(isEnglish, database.eventDao().getBVICPrograms())
                VICType.GVIC.value -> getVICProgramesList(isEnglish, database.eventDao().getGVICPrograms())
                else -> ArrayList<VICGenericModel>()
            }
        } else {
            when (vicType) {
                VICType.VIC.value -> getVICProgramesList(isEnglish, database.eventDao().getVICGuestPrograms())
                VICType.BVIC.value -> getVICProgramesList(isEnglish, database.eventDao().getBVICGuestPrograms())
                VICType.GVIC.value -> getVICProgramesList(isEnglish, database.eventDao().getGVICGuestPrograms())
                else -> ArrayList<VICGenericModel>()
            }
        }
    }

    // Modify to be model
    private fun getVICProgramesList(isEnglish: Boolean, eventEntity: List<EventEntity>) {
        modelList.clear()
        eventEntity.forEach {
            var programItem: VICGenericModel

            if (it.id == Year_Round_Offers || it.id == Tourist_Monthly_Offers) {
//            if (it.id == Year_Round_Offers && (vicType == VICType.VIC.value || vicType == VICType.GVIC.value)) {
                programItem = VICShopModel(if (isEnglish) {
                    it.title
                } else {
                    it.titleZh ?: it.title
                }, OfferType.VIC_Year_Round_Offers.value)
            } else if (it.id == Birthday_Offers || it.id == Tourist_Birthday_Offers) {
                programItem = VICShopModel(if (isEnglish) {
                    it.title
                } else {
                    it.titleZh ?: ""
                }, OfferType.VIC_Birthday_Offers.value)
            } else if (it.id == Exclusive_Privileges) {
                programItem = VICExclusivePrivilegesModel(if (isEnglish) {
                    it.title
                } else {
                    it.titleZh ?: ""
                })
            } else {
                programItem = VICPosterModel(if (isEnglish) {
                    it.title
                } else {
                    it.titleZh ?: ""
                }
                        , if (isEnglish) {
                    if (it.poster!!.size > 0) {
                        it.poster!![0]
                    } else {
                        ""
                    }
                } else {
                    if (it.posterZh!!.size > 0) {
                        it.posterZh!![0]
                    } else {
                        ""
                    }
                })
            }
            modelList.add(programItem)
        }
        for (item in modelList) Log.i("getVICProgramesList", "item " + item.title)
    }

    private fun initRecyclerView() {
        val itemAdapter = ModelAdapter<VICGenericModel, VICGenericModelItem>(IInterceptor {
            // navigate to poster page
            if (it is VICPosterModel)
                return@IInterceptor VICPosterModelItem(it)
//            navigate to shop list
            else if (it is VICShopModel)
                return@IInterceptor VICShopModelItem(it)
//            TVIC -> 尊享優惠
            else if (it is VICExclusivePrivilegesModel)
                return@IInterceptor VICExclusivePrivilegesModelItem(it)
//            廢
            else
                return@IInterceptor VICGenericModelItem(it)
        })



        fastAdapter = FastAdapter.with<VICGenericModelItem, ModelAdapter<VICGenericModel, VICGenericModelItem>>(Arrays.asList(itemAdapter))
        itemAdapter.add(
                modelList
        )
        layoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        mView.mRecyclerView.adapter = fastAdapter
        mView.mRecyclerView.layoutManager = layoutManager
        mView.mRecyclerView.setNestedScrollingEnabled(false)

    }


    fun titleAlpha() {
        mView.mNestedScrollView.getViewTreeObserver().addOnScrollChangedListener {
            var scrollY = mView.mNestedScrollView.getScrollY() / 1
            if (scrollY >= 0) {
                if (scrollY > 100) {
                    scrollY = 100
                }
                scrollY = scrollY / 10
                var alphaPercentage = scrollY.toFloat() / 10
                mView.mTitleBar.bkc.alpha = alphaPercentage
            }
        }
    }
}

enum class VICType(val value: Int) {
    VIC(1),
    BVIC(2),
    GVIC(3)
}

enum class OfferType(val value: Int) {
    VIC_Year_Round_Offers(1),
    VIC_Birthday_Offers(2),
    BVIC_Year_Round_Offers(3),
    BVIC_Birthday_Offers(4),
    TVIC_Year_Round_Offers(5),
    TVIC_Birthday_Offers(6)
}