package com.timessquare.vic.vicprograms.itemmodel

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.timessquare.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.ModelAbstractItem
import org.jetbrains.anko.*

/**
 *   Created by Mason on 13/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
open class VICGenericModelItem(open val model: VICGenericModel) : ModelAbstractItem<VICGenericModel, VICGenericModelItem, VICGenericModelItem.ViewHolder>(model) {


    override fun getLayoutRes(): Int = 0

    override fun getType(): Int = R.id.vicGenericModelItemId

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun createView(ctx: Context, parent: ViewGroup?): View {
        val viewContainer = ctx.UI {
            frameLayout() {
                layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
                textView {
                    id = R.id.vicGenericModelItemTextView
                    gravity = Gravity.CENTER
                    padding = dimen(R.dimen.padding_small)
                    backgroundResource = R.drawable.bg_vic_item
//                    layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
                }.lparams(matchParent, wrapContent) {
                    margin = dimen(R.dimen.margin_small)
                }
            }
        }
        return viewContainer.view
    }

    open class ViewHolder(open val view: View) : FastAdapter.ViewHolder<VICGenericModelItem>(view) {
        @BindView(R.id.vicGenericModelItemTextView)
        lateinit var vicTextView: TextView

        override fun bindView(item: VICGenericModelItem, payloads: MutableList<Any>?) {
            ButterKnife.bind(this, view)

            vicTextView.text = item.model.title
        }

        override fun unbindView(item: VICGenericModelItem?) {
            vicTextView.text = ""
        }

    }
}
