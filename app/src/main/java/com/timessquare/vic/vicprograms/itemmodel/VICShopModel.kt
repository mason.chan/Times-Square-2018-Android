package com.timessquare.vic.vicprograms.itemmodel

import com.timessquare.vic.vicprograms.OfferType
import com.timessquare.vic.vicprograms.VICType

/**
 *   Created by Mason on 13/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICShopModel(override val title: String, val offerType: Int): VICGenericModel(title) {
}