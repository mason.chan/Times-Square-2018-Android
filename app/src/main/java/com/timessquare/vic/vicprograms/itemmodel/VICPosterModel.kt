package com.timessquare.vic.vicprograms.itemmodel

/**
 *   Created by Mason on 13/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICPosterModel(override val title: String, val posterUrl: String): VICGenericModel(title) {
}