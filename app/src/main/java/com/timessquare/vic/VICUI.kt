package com.timessquare.vic

import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.membercardsetting.MemberCardSettingKey
import com.timessquare.memberscancard.MemberScanCardKey
import com.timessquare.utils.Constant
import com.timessquare.vic.vicprograms.VICProgramsKey
import com.timessquare.vic.vicprograms.VICType
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk25.coroutines.onClick


/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class VICUI() : AnkoComponent<VICFragment> {

    lateinit var vicVic: ImageView
    lateinit var vicBvic: ImageView
    lateinit var vicGvic: ImageView

    override fun createView(ui: AnkoContext<VICFragment>): View {

        return with(ui) {
            constraintLayout() {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    titleBar {
                        id = R.id.vic_titlebar
                    }
                            .addMenuButton()
                            .addBackgroundColor(R.color.color_cover)
                            .addTitle(R.string.vic_title)
                            .addButton(imgRes = R.drawable.ic_old_scan_qr, gravity = Gravity.END, marginRes = R.dimen.margin_normal, buttonId = R.id.member_card_setting,
                                    onClickListener = View.OnClickListener {
                                        MainActivity[ctx].navigateTo(MemberScanCardKey().withArgs {
                                            putInt(Constant.TAG_INTENT, -2)
                                        })
                                    })
                    scrollView {
//                        backgroundColorResource = R.color.color_white
                        verticalLayout {
                            padding = dimen(R.dimen.padding_item)
                            setGravity(Gravity.CENTER_HORIZONTAL)
                            cardView {
                                radius = 20f
                                vicVic = imageView() {
                                    id = R.id.vicVic
                                    adjustViewBounds = true
                                    onClick {
                                        MainActivity.get(view.context).navigateTo(VICProgramsKey().withArgs {
                                            putInt(Constant.TAG_INTENT, VICType.VIC.value)
                                        })
                                    }
                                }
                            }.lparams() {
                                margin = dimen(R.dimen.margin_small)
                            }
                            cardView {
                                radius = 20f
                                vicBvic = imageView() {
                                    id = R.id.vicBvic
                                    adjustViewBounds = true
                                    onClick {
                                        MainActivity.get(view.context).navigateTo(VICProgramsKey().withArgs {
                                            putInt(Constant.TAG_INTENT, VICType.BVIC.value)
                                        })
                                    }
                                }
                            }.lparams() {
                                margin = dimen(R.dimen.margin_small)
                            }
                            cardView {
                                radius = 20f
                                vicGvic = imageView() {
                                    id = R.id.vicGvic
                                    adjustViewBounds = true
                                    onClick {
                                        MainActivity.get(view.context).navigateTo(VICProgramsKey().withArgs {
                                            putInt(Constant.TAG_INTENT, VICType.GVIC.value)
                                        })
                                    }
                                }
                            }.lparams() {
                                margin = dimen(R.dimen.margin_small)
                            }
                        }
                    }
                }.lparams(matchParent, matchConstraint) {

                }
            }

        }
    }
}
