package com.timessquare.vic.vicexclusiveprivileges

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 15/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class VICExclusivePrivilegesKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(VICExclusivePrivilegesKey))

    override fun createFragment(): BaseFragment = VICExclusivePrivilegesFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}