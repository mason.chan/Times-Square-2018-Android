package com.timessquare.vic.vicexclusiveprivileges

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.timessquare.MainActivity
import com.timessquare.database.AppDataBase
import com.timessquare.database.entity.EventEntity
import com.timessquare.utils.SharedPreferenceUtils
import com.timessquare.vic.vicprograms.OfferType
import com.timessquare.vic.vicprograms.VICType
import com.timessquare.vic.vicprograms.itemmodel.*
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 15/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class VICExclusivePrivilegesFragment : BaseFragment() {
    lateinit var mView: VICExclusivePrivilegesUI

    lateinit var mFastItemAdapter: FastItemAdapter<VICPosterModelItem>
    var itemList = ArrayList<VICPosterModelItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = VICExclusivePrivilegesUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleAlpha()
        addItem(MainActivity[view.context].sp, MainActivity[view.context].database, MainActivity[view.context].isEnglish())
        initRecyclerView()
    }

    // Add the global
    fun addItem(sp: SharedPreferenceUtils, database: AppDataBase, isEnglish: Boolean) {
        itemList.clear()
        database.eventDao().getGVICExclusivePrograms().forEach {
            var programItem: VICPosterModel
                programItem = VICPosterModel(if (isEnglish) {
                    it.title
                } else {
                    it.titleZh ?: ""
                }, if (isEnglish) {
                    if (it.poster!!.size > 0) {
                        it.poster!![0]
                    } else {
                        ""
                    }
                } else {
                    if (it.posterZh!!.size > 0) {
                        it.posterZh!![0]
                    } else {
                        ""
                    }
                }

                )
            itemList.add(VICPosterModelItem(programItem))
        }
    }


    private fun initRecyclerView() {
        mFastItemAdapter = FastItemAdapter()

        var layoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        mView.recyclerView.layoutManager = layoutManager
        mView.recyclerView.adapter = mFastItemAdapter
        mView.recyclerView.setNestedScrollingEnabled(false)
        mFastItemAdapter.add(itemList)
        Log.i("VICExclusivePrivi", "eventEntity" + itemList.size )

    }

    fun titleAlpha() {
        mView.nestedScrollView.getViewTreeObserver().addOnScrollChangedListener {
            var scrollY = mView.nestedScrollView.getScrollY() / 1
            if (scrollY >= 0) {
                if (scrollY > 100) {
                    scrollY = 100
                }
                scrollY = scrollY / 10
                var alphaPercentage = scrollY.toFloat() / 10
                mView.titleBar.bkc.alpha = alphaPercentage
            }
        }
    }

}