package com.timessquare.vic.vicexclusiveprivileges

import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.RecyclerView
import android.view.View
import com.timessquare.vic.vicprograms.VICType
import com.timessquare.views.*
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout

/**
 *   Created by Mason on 15/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */


class VICExclusivePrivilegesUI : AnkoComponent<VICExclusivePrivilegesFragment> {

    lateinit var nestedScrollView: NestedScrollView
    lateinit var recyclerView: RecyclerView
    lateinit var titleBar: TitleBar

    override fun createView(ui: AnkoContext<VICExclusivePrivilegesFragment>): View {
        return with(ui) {
            frameLayout {
                vicProgramesView(VICType.GVIC.value) {
                    nestedScrollView = mNestedScrollView
                    recyclerView = mRecyclerView
                    titleBar = mTitleBar
                }
            }

        }
    }
}