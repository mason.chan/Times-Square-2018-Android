package com.timessquare.vic

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class VICKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(VICKey))

    override fun createFragment(): BaseFragment = VICFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}