package com.timessquare.vic

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.BaseFragment
import com.timessquare.R
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class VICFragment : BaseFragment() {

    lateinit var mView: VICUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = VICUI()
        Log.i("VICFragment", "onCreateView")

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("VICFragment", "onViewCreated")
        Glide.with(view.context).load(R.drawable.ic_vic).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(mView.vicVic)
        Glide.with(view.context).load(R.drawable.ic_bvic).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(mView.vicBvic)
        Glide.with(view.context).load(R.drawable.ic_gvic).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(mView.vicGvic)

        }

}