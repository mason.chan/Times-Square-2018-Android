package com.timessquare.index

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.dummy.IndexDummyData
import com.timessquare.views.TitleBar
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import kotlinx.android.synthetic.main.index_view.*

/**
 *   Created by Mason on 15/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class IndexFragment : BaseFragment() {


    private var mFastItemAdapter: FastItemAdapter<IndexItem>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = LinearLayout(context)
        view.orientation = LinearLayout.VERTICAL
        var content = inflater.inflate(R.layout.index_view, null)

        view.addView(TitleBar(context, false, true)
                .addBackground(R.drawable.ic_background_title)
                .addMenuButton()
                .addMemberButton(false)
                .addLogo()
        )
        view.addView(content)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val indexKey = getKey<IndexKey>()

        mFastItemAdapter = FastItemAdapter()
        mFastItemAdapter!!.withOnClickListener { v, adapter, item, position ->
//            Toast.makeText(v!!.context, item.mName, Toast.LENGTH_SHORT).show()
            if (item.mKey != null) {
                MainActivity.get(view.context).navigateTo(item.mKey!!)
            }
            false
        }

        recyclerview.layoutManager = LinearLayoutManager(context)
        recyclerview.itemAnimator = DefaultItemAnimator()
        recyclerview.adapter = mFastItemAdapter
        //fill with some sample data
        mFastItemAdapter!!.add(IndexDummyData.indexItems)

    }


}

