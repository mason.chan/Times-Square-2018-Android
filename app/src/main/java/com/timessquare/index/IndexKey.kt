package com.timessquare.index

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 15/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class IndexKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(IndexKey))

    override fun createFragment(): BaseFragment = IndexFragment()

    override  fun stackIdentifier(): String = ""

    companion object {}
}