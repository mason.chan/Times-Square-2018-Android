package com.timessquare.index


import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.BaseKey
import com.timessquare.R
import com.mikepenz.fastadapter.items.AbstractItem
import org.jetbrains.anko.textResource


/**
 *   Created by Mason on 18/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class IndexItem : AbstractItem<IndexItem, IndexItem.ViewHolder>() {


     var mName: Int = 0
    lateinit var mImageUrl: String
     var mKey: BaseKey? = null


    fun withImage(imageUrl: String): IndexItem {
        this.mImageUrl = imageUrl
        return this
    }

    fun withName(name: Int): IndexItem {
        this.mName = name
        return this
    }

    fun withKey(key: BaseKey): IndexItem {
        this.mKey = key
        return this
    }

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override fun getType(): Int {
        return R.id.index_item_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.index_item_view
    }


    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        //get the context
        val ctx = holder.itemView.context

        //define our data for the view
        holder.textView!!.textResource = mName


        Glide.with(ctx).load(mImageUrl).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(holder.imageView!!)
    }

//    override fun unbindView(holder: ViewHolder) {
//        super.unbindView(holder)
//        Glide.clear(holder.imageView!!)
//        holder.imageView.setImageResource(null)
//    }

    /**
     * ImdexItem ViewHolder
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {


//        @BindView(R.id.text_view) internal var textView: TextView? = null
//        @BindView(R.id.image_view) internal var imageView: ImageView? = null
        lateinit var textView: TextView
        lateinit var imageView: ImageView

        init {
//            ButterKnife.bind(this, view)
            textView = view.findViewById(R.id.text_view)
            imageView = view.findViewById(R.id.image_view)
        }
//        override fun bindView(item: IndexItem, payloads: MutableList<Any>?) {
//
//            textView = view.findViewById(R.id.text_view)
//
//            textView!!.text = item.mName
////            Glide.with(imageView)
//            view.text_view.
//        }
//
//        override fun unbindView(item: IndexItem?) {
//            textView!!.text = ""
//
//        }

    }

}