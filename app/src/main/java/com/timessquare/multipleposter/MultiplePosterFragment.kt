package com.timessquare.multipleposter

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.utils.Constant
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MultiplePosterFragment : BaseFragment() {

    private val posterUrlList: ArrayList<String> by lazy {
        arguments?.getStringArrayList(Constant.TAG_INTENT) ?: arrayListOf()
    }

    lateinit var mView: MultiplePosterUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MultiplePosterUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        i("MultiplePosterFragment", posterUrlList.toString())
        setPageNumber(0)
        initViewPager(view)
        mView.pageNextImageView.onClick{
            if(mView.viewPager.currentItem < mView.viewPager.adapter!!.count)
            mView.viewPager.currentItem = mView.viewPager.currentItem + 1
        }
        mView.pageBackImageView.onClick{
            if(mView.viewPager.currentItem > 0)
                mView.viewPager.currentItem = mView.viewPager.currentItem - 1
        }
    }

    fun initViewPager(view: View) {
        mView.viewPager.adapter = SlidingImagePagerAdapter(view.context, posterUrlList)

        mView.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                setPageNumber(position)
            }

            override fun onPageSelected(position: Int) {
            }
        })
    }

    fun setPageNumber(position: Int) {
        mView.pageNumberTextView.text = (position + 1).toString() + " | " + posterUrlList.size
    }


}