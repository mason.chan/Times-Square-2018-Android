package com.timessquare.multipleposter

import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.timessquare.R
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.viewPager
import org.w3c.dom.Text

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */


class MultiplePosterUI() : AnkoComponent<MultiplePosterFragment> {

    lateinit var viewPager: ViewPager
    lateinit var pageNumberTextView: TextView
    lateinit var pageNextImageView: ImageView
    lateinit var pageBackImageView: ImageView
    override fun createView(ui: AnkoContext<MultiplePosterFragment>): View {
        return with(ui) {
            verticalLayout() {
                backgroundColorResource = R.color.color_black
                titleBar().addBackButton(true)
                viewPager = viewPager { }.lparams(matchParent, matchParent, 1f)
                relativeLayout() {
                    backgroundColorResource = R.color.color_white
                    pageNumberTextView = textView {
                        textSizeDimen = R.dimen.text_normal
                    }.lparams(){
                        centerInParent()
                    }
                    pageBackImageView = imageView {
                        imageResource = R.drawable.ic_backto
                        padding = dimen(R.dimen.padding_menu)
                    }.lparams(dimen(R.dimen.navigation_button),dimen(R.dimen.navigation_button)) {
                        alignParentLeft()
                    }
                    pageNextImageView = imageView {
                        imageResource = R.drawable.ic_next
                        padding = dimen(R.dimen.padding_menu)
                    }.lparams(dimen(R.dimen.navigation_button),dimen(R.dimen.navigation_button)) {
                        alignParentRight()
                    }
                }
            }
        }
    }
}