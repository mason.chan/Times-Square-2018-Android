package com.timessquare.multipleposter

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.timessquare.R
import com.timessquare.apiservice.Path
import uk.co.senab.photoview.PhotoViewAttacher
/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class SlidingImagePagerAdapter(private val ctx: Context, val images: ArrayList<String>) : PagerAdapter() {

    private val inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(ctx)
    }
//    val inflater2 = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    lateinit var mPhotoViewAttacher: PhotoViewAttacher

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false)

        val imageView = imageLayout.findViewById(R.id.image) as ImageView
        Log.i("instantiateItem", Path.PRO_IMAGE_URL + images[position])
        Glide.with(ctx).load(Path.PRO_IMAGE_URL + images[position]).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                return false
            }

            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                mPhotoViewAttacher = PhotoViewAttacher(imageView)
                Log.i("zoom", "ing")
                return false
            }
        }).into(imageView)

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}