package com.timessquare.multipleposter

import android.annotation.SuppressLint
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MultiplePosterKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MultiplePosterKey))

    override fun createFragment() = MultiplePosterFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}