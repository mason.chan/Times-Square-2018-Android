package com.timessquare.shopfilter.ViewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.timessquare.shopfilter.shopfilterdetail.ShopFilterDetailItem
import android.content.Context
import android.content.res.Resources
import android.support.v4.app.Fragment
import android.util.Log.i
import android.view.View
import com.timessquare.MainActivity
import com.timessquare.R


/**
 *   Created by Mason on 26/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

open class ShopFilterViewModel(application: Application) : AndroidViewModel(application) {
    // Create a LiveData with a String
    var byCategory: MutableLiveData<ShopFilterDetailItem> = MutableLiveData<ShopFilterDetailItem>()
    var byFloor: MutableLiveData<ShopFilterDetailItem> = MutableLiveData<ShopFilterDetailItem>()
    var byPrefix: MutableLiveData<ShopFilterDetailItem> = MutableLiveData<ShopFilterDetailItem>()


    init {
        i("reset", " had reset")
        reset()
    }

    fun getCategory(): LiveData<ShopFilterDetailItem> {
        if (byCategory == null) {
            byCategory = MutableLiveData()
        }
        return byCategory as MutableLiveData<ShopFilterDetailItem>
    }

    fun getFloor(): LiveData<ShopFilterDetailItem> {
        if (byFloor == null) {
            byFloor = MutableLiveData()
        }
        return byFloor as MutableLiveData<ShopFilterDetailItem>
    }

    fun getPrefix(): LiveData<ShopFilterDetailItem> {

        return byPrefix as MutableLiveData<ShopFilterDetailItem>
    }

    fun setCategory(category: ShopFilterDetailItem) {
        byCategory!!.value = category
    }

    fun setFloor(floor: ShopFilterDetailItem) {
        byFloor!!.value = floor
    }

    fun setPrefix(prefix: ShopFilterDetailItem) {
        byPrefix!!.value = prefix
    }


    fun reset() {
        setCategory(ShopFilterDetailItem(0, getApplication<Application>().resources.getStringArray(R.array.shop_category_list)[0], View.VISIBLE))
        setFloor(ShopFilterDetailItem(0, getApplication<Application>().resources.getStringArray(R.array.shop_floor_list)[0], View.VISIBLE))
        setPrefix(ShopFilterDetailItem(0, getApplication<Application>().resources.getStringArray(R.array.shop_prefix_list)[0], View.VISIBLE))

    }
    fun reset(fragment: Fragment) {
        setCategory(ShopFilterDetailItem(0, fragment.resources.getStringArray(R.array.shop_category_list)[0], View.VISIBLE))
        setFloor(ShopFilterDetailItem(0, fragment.resources.getStringArray(R.array.shop_floor_list)[0], View.VISIBLE))
        setPrefix(ShopFilterDetailItem(0, fragment.resources.getStringArray(R.array.shop_prefix_list)[0], View.VISIBLE))
    }
}