package com.timessquare.shopfilter.shopfilterdetail

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log.i
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.timessquare.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.wrapContent

/**
 *   Created by Mason on 27/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class ShopFilterDetailItem(var mId: Int, var mName: String, var mVisable: Int) : AbstractItem<ShopFilterDetailItem, ShopFilterDetailItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.ShopFilterDetailID
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.shop_filter_item
    }

    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>?) {
        super.bindView(holder, payloads)
        if (mVisable === View.GONE) {
            i("bindView", "GONE la")
            holder.constraint_layout.visibility = mVisable
            holder.constraint_layout.setLayoutParams(RecyclerView.LayoutParams(0, 0))
    } else {
            holder.constraint_layout.visibility = mVisable
            holder.constraint_layout.setLayoutParams(RecyclerView.LayoutParams(matchParent, wrapContent))
        }
        i("isSelected", isSelected.toString())
        if (isSelected) {
            holder.imageView.visibility = View.VISIBLE
            holder.textView.textColorResource = R.color.color_black
        } else {
            holder.imageView.visibility = View.GONE
            holder.textView.textColorResource = R.color.color_text_gray

        }
        holder.textView.text = mName
    }

    /**
     * Shop Filter Detail Item
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {
        var textView: TextView
        var imageView: ImageView
        var constraint_layout: ConstraintLayout

        init {
            textView = view.findViewById(R.id.text_view)
            imageView = view.findViewById(R.id.image_view)
            constraint_layout = view.findViewById(R.id.constraint_layout)
        }

    }

     open class ShopFilterDetailItemClickEvent : ClickEventHook<ShopFilterDetailItem>() {
        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return if (viewHolder is ShopFilterDetailItem.ViewHolder) {
                viewHolder.constraint_layout
            } else null

    }
        override fun onClick(v: View?, position: Int, fastAdapter: FastAdapter<ShopFilterDetailItem>, item: ShopFilterDetailItem) {
            if (!item.isSelected) {
                val selections = fastAdapter.selections
                if (!selections.isEmpty()) {
                    val selectedPosition = selections.iterator().next()
                    fastAdapter.deselect()
                    fastAdapter.notifyItemChanged(selectedPosition)
                }
                fastAdapter.select(position)
            }
        }

    }

}