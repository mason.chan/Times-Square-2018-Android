package com.timessquare.shopfilter.shopfilterdetail

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 27/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class ShopFilterDetailKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(ShopFilterDetailKey))

    override fun createFragment(): BaseFragment = ShopFilterDetailFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}