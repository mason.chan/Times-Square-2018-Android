package com.timessquare.shopfilter.shopfilterdetail

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.shopfilter.FilterType
import com.timessquare.shopfilter.ViewModel.ShopFilterViewModel
import com.timessquare.shopfilter.ViewModel.DineFilterViewModel
import com.timessquare.utils.Constant
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.*

/**
 *   Created by Mason on 27/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class ShopFilterDetailFragment : BaseFragment() {

    val filterType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: -1 }
    val isShopNotFood: Boolean by lazy { arguments?.getBoolean(Constant.TAG_INTENT2) ?: true }

    lateinit var mView: ShopFilterDetailUI
    private var mFastItemAdapter = FastItemAdapter<ShopFilterDetailItem>()
    lateinit var mModel: ShopFilterViewModel

    var selectedItem: ShopFilterDetailItem? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = ShopFilterDetailUI(filterType)
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * Get the ViewModel
         */
        mModel = ViewModelProviders.of(activity!!).get(if (isShopNotFood) ShopFilterViewModel::class.java else DineFilterViewModel::class.java)

        initContent()
        mFastItemAdapter.add(getOpions())
        presetOpion()
        mView.applyButton.onClick {
            selectedItem = mFastItemAdapter.selectedItems.toList()[0]
            if (selectedItem != null) {
                when (filterType) {
                    FilterType.byCategory.value -> mModel.setCategory(selectedItem!!)
                    FilterType.byFloor.value -> mModel.setFloor(selectedItem!!)
                    FilterType.byPrefix.value -> mModel.setPrefix(selectedItem!!)
                }
            }
            MainActivity[view.context].onBackPressed()
        }
    }

    private fun initContent() {
        mFastItemAdapter = FastItemAdapter()
        mFastItemAdapter.withSelectable(true)

        //configure our fastAdapter
        mFastItemAdapter!!.withOnClickListener { v, adapter, item, position ->
            i("mFastItemAdapter", item.mId.toString() + "   " + item.mName)

            when (filterType) {
                FilterType.byCategory.value -> mModel.setCategory(item)
                FilterType.byFloor.value -> mModel.setFloor(item)
                FilterType.byPrefix.value -> mModel.setPrefix(item)
            }
            false
        }
        mFastItemAdapter!!.withOnPreClickListener { v, adapter, item, position ->
            // consume otherwise radio/checkbox will be deselected
            i("withOnPreClickListener", item.mId.toString() + "   " + item.mName)

            true
        }
        mFastItemAdapter!!.withEventHook(ShopFilterDetailItem.ShopFilterDetailItemClickEvent())

        mView.recyclerView.layoutManager = LinearLayoutManager(context)
        mView.recyclerView.itemAnimator = DefaultItemAnimator()
        mView.recyclerView.adapter = mFastItemAdapter
    }

    private fun getOpions(): List<ShopFilterDetailItem> {
        val result = ArrayList<ShopFilterDetailItem>()
        val sourceList: Array<String>
        when (filterType) {
            FilterType.byCategory.value -> sourceList = resources.getStringArray(if (isShopNotFood) R.array.shop_category_list else R.array.food_beverages_category_list)

            FilterType.byFloor.value -> sourceList = resources.getStringArray(R.array.shop_floor_list)

            FilterType.byPrefix.value -> sourceList = resources.getStringArray(R.array.shop_prefix_list)

            else -> sourceList = arrayOf()
        }
        sourceList.forEachIndexed { index, s ->
            if (s.isNotEmpty()) {
                result.add(ShopFilterDetailItem(index, s, View.VISIBLE))
            } else {
                result.add(ShopFilterDetailItem(index, s, View.GONE))
            }
        }

        return result
    }

    fun presetOpion() {
        when (filterType) {
            FilterType.byCategory.value -> mFastItemAdapter.select(mModel.byCategory.value!!.mId)

            FilterType.byFloor.value -> mFastItemAdapter.select(mModel.byFloor.value!!.mId)
            FilterType.byPrefix.value -> mFastItemAdapter.select(mModel.byPrefix.value!!.mId)

        }
    }
}