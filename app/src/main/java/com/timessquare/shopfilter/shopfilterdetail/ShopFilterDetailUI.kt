package com.timessquare.shopfilter.shopfilterdetail

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import com.timessquare.R
import com.timessquare.shopfilter.FilterType
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 *   Created by Mason on 27/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopFilterDetailUI(var filterType: Int) : AnkoComponent<ShopFilterDetailFragment> {

    var titleRes = R.string.shop_filter_title
    lateinit var recyclerView: RecyclerView
    lateinit var applyButton: Button

    override fun createView(ui: AnkoContext<ShopFilterDetailFragment>): View {
        return with(ui) {
            constraintLayout {
                when (filterType) {
                    FilterType.byCategory.value -> titleRes = R.string.shop_filter_by_category
                    FilterType.byFloor.value -> titleRes = R.string.shop_filter_by_floor
                    FilterType.byPrefix.value -> titleRes = R.string.shop_filter_by_prefix
                }
                titleBar {
                    id = R.id.shop_filter_titlebar
                }.lparams(matchParent, wrapContent) {
                    topToTop = PARENT_ID
                }.addBackgroundColor(R.color.color_cover)
                        .addBackButton()
                        .addTitle(titleRes)
                recyclerView = recyclerView {

                }.lparams(matchParent, matchConstraint) {
                    topToBottom = R.id.shop_filter_titlebar
                    bottomToTop = R.id.shop_filter_apply_button
                }
                applyButton = button {
                    id = R.id.shop_filter_apply_button
                    textResource = R.string.shop_filter_apply
                    backgroundResource = R.drawable.button_sure
                }.lparams(matchParent, wrapContent) {
                    bottomToBottom = PARENT_ID
                    margin = dimen(R.dimen.margin_normal)
                }
            }
        }
    }
}