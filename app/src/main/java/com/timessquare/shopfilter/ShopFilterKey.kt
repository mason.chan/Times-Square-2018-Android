package com.timessquare.shopfilter

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.shopdetail.ShopDetailFragment
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 26/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class ShopFilterKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(ShopFilterKey))

    override fun createFragment(): BaseFragment = ShopFilterFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}