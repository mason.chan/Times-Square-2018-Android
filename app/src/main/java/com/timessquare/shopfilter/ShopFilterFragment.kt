package com.timessquare.shopfilter

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import android.util.Log.i
import com.timessquare.MainActivity
import com.timessquare.shopfilter.ViewModel.DineFilterViewModel
import com.timessquare.shopfilter.ViewModel.ShopFilterViewModel
import com.timessquare.utils.Constant


/**
 *   Created by Mason on 26/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

open class ShopFilterFragment : BaseFragment() {

    val isShopNotFood: Boolean by lazy { arguments?.getBoolean(Constant.TAG_INTENT) ?: true }

    lateinit var mView: ShopFilterUI
    lateinit var mModel: ShopFilterViewModel

//    var isreset = false
//    var firstTime = false

    override fun onDestroy() {
        super.onDestroy()
        i("ShopFilterFragment", "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        i("ShopFilterFragment", "onDetach")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        i("ShopFilterFragment", "onDestroyView")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        i("ShopFilterFragment", "onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        i("ShopFilterFragment", "onCreateView")
        mView = ShopFilterUI(isShopNotFood)
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        i("ShopFilterFragment", "onViewCreated")

        /**
         * Get the ViewModel
         */

        mModel = ViewModelProviders.of(activity!!).get(if (isShopNotFood) ShopFilterViewModel::class.java else DineFilterViewModel::class.java)
        /**
         *   init the ShopFilterViewModel
         */

        if(mModel.byCategory.value!!.mId == 0 && mModel.byFloor.value!!.mId == 0 && mModel.byPrefix.value!!.mId == 0) {
            mModel.reset(this)
        }
//       mModel.reset(view.context)w

        // Create the observer which updates the UI.
//        val nameObserver = object : Observer<ShopFilterDetailItem> {
//            override fun onChanged(t: ShopFilterDetailItem?) {
//                mView.byCatagoryTextView.text = t!!.mName
//            }
//
//        }
        mModel.getCategory().observe(this, Observer { item ->
            mView.byCatagoryTextView.text = item!!.mName
        })
        mModel.getFloor().observe(this, Observer { item ->
            mView.byFloorTextView.text = item!!.mName
        })
        mModel.getPrefix().observe(this, Observer { item ->
            mView.byPrefixTextView.text = item!!.mName
        })
//        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
//        mModel.getCategory().observe(this, nameObserver)
        mView.resetButton.onClick {
            mModel.reset(this@ShopFilterFragment)
        }

//        if(isreset) {
//            mModel.reset(activity!!)
//            isreset = !isreset
//        }
        mView.applyButton.onClick {
            i("category", "category" + (mModel.byCategory.value!!.mId))
            i("category", "floor" + (mModel.byFloor.value!!.mId))
            i("category", "prefix" + (mModel.byPrefix.value!!.mId))
//            if (isShopNotFood) {
//                MainActivity.get(view.context).sp.setValue(SParem.Shop_Filter_Category_Id, mModel.byCategory.value!!.mId)
//                MainActivity.get(view.context).sp.setValue(SParem.Shop_Filter_Floor_Id, mModel.byFloor.value!!.mId)
//                MainActivity.get(view.context).sp.setValue(SParem.Shop_Filter_Prefix, mModel.byPrefix.value!!.mId)
//            } else {
//                MainActivity.get(view.context).sp.setValue(SParem.Food_Beverages_Filter_Category_Id, mModel.byCategory.value!!.mId)
//                MainActivity.get(view.context).sp.setValue(SParem.Food_Beverages_Filter_Floor_Id, mModel.byFloor.value!!.mId)
//                MainActivity.get(view.context).sp.setValue(SParem.Food_Beverages_Filter_Prefix, mModel.byPrefix.value!!.mId)
//            }
            MainActivity[view.context].onBackPressed()
        }
    }

    //    fun recoverFilter(ctx: Context) {
//        mModel.setCategory = MainActivity[ctx].sp.getIntValue(SParem.Shop_Filter_Category_Id, 0)
//    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        i("ShopFilterFragment", "onAttach")
//        isreset = !isreset
    }
}

enum class FilterType(val value: Int) {
    byCategory(0),
    byFloor(1),
    byPrefix(2)
}
