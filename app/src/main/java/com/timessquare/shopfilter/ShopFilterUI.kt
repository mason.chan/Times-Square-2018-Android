package com.timessquare.shopfilter

import android.os.Build
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.v7.widget.CardView
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.settings.language.LanguageKey
import com.timessquare.shopfilter.shopfilterdetail.ShopFilterDetailKey
import com.timessquare.utils.Constant
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.menufooter_view.view.*
import kotlinx.android.synthetic.main.notification_template_lines_media.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.guideline
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 26/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopFilterUI(var isShopNotFood: Boolean) : AnkoComponent<ShopFilterFragment> {
    lateinit var byCatagoryTextView: TextView
    lateinit var byFloorTextView: TextView
    lateinit var byPrefixTextView: TextView
    lateinit var resetButton: Button
    lateinit var applyButton: Button


    override fun createView(ui: AnkoContext<ShopFilterFragment>): View {
        return with(ui) {
            constraintLayout {
                guideline {
                    id = R.id.guideline1
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guidePercent = 0.5f
                }
                verticalLayout {
                    titleBar {}
                            .addBackgroundColor(R.color.color_cover)
                            .addBackButton()
                            .addTitle(R.string.shop_filter_title)

                    include<CardView>(R.layout.settings_item) {
                        onClick {
                            MainActivity.get(view.context).navigateTo(ShopFilterDetailKey().withArgs {
                                putInt(Constant.TAG_INTENT, FilterType.byCategory.value)
                                putBoolean(Constant.TAG_INTENT2, isShopNotFood)
                            })
                        }
                        byCatagoryTextView = find<TextView>(R.id.text_view)
                    }
                    include<CardView>(R.layout.settings_item) {
                        onClick {
                            MainActivity.get(view.context).navigateTo(ShopFilterDetailKey().withArgs {
                                putInt(Constant.TAG_INTENT, FilterType.byFloor.value)
                                putBoolean(Constant.TAG_INTENT2, isShopNotFood)
                            })
                        }
                        byFloorTextView = find<TextView>(R.id.text_view)
                    }
                    include<CardView>(R.layout.settings_item) {
                        onClick {
                            MainActivity.get(view.context).navigateTo(ShopFilterDetailKey().withArgs {
                                putInt(Constant.TAG_INTENT, FilterType.byPrefix.value)
                                putBoolean(Constant.TAG_INTENT2, isShopNotFood)
                            })
                        }
                        byPrefixTextView = find<TextView>(R.id.text_view)
                    }
                }.lparams(matchParent, matchConstraint) {
                    topToTop = PARENT_ID
                }
                resetButton = button {
                    textResource = R.string.shop_filter_reset
                    backgroundResource = R.drawable.button_cancel
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        elevation = 0f
                    }
                }.lparams(0, wrapContent) {
                    leftToLeft = PARENT_ID
                    rightToLeft = R.id.guideline1
                    bottomToBottom = PARENT_ID
                    margin = dimen(R.dimen.margin_normal)
                }
                applyButton = button {
                    textResource = R.string.shop_filter_apply
                    backgroundResource = R.drawable.button_sure
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        elevation = 0f
                    }
                }.lparams(0, wrapContent) {
                    rightToRight = PARENT_ID
                    leftToRight = R.id.guideline1
                    bottomToBottom = PARENT_ID
                    margin = dimen(R.dimen.margin_normal)
                }
            }
        }
    }
}