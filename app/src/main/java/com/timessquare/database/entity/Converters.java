package com.timessquare.database.entity;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.timessquare.apiservice.datamodel.OpenHour;
import com.timessquare.apiservice.datamodel.VicCard;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.util.Log.i;

/**
 * Created by Mason on 24/7/2018.
 * Hita Group
 * mason.chan@sllin.com
 */
public class Converters {
    @TypeConverter
    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }
    @TypeConverter
    public static ArrayList<String> fromString(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<String> list) {
        if (list == null) {
            return (null);
        }
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static String fromOpenHourList(ArrayList<OpenHour> optionValues) {
        if (optionValues == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<OpenHour>>() {
        }.getType();
        String json = gson.toJson(optionValues, type);
        return json;
    }

    @TypeConverter
    public static ArrayList<OpenHour> toOpenHourList(String optionValuesString) {

        i("toOpenHourList" ,"toOpenHourList");

        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<OpenHour>>() {
        }.getType();
        ArrayList<OpenHour> productCategoriesList = gson.fromJson(optionValuesString, type);
        return productCategoriesList;
    }
    @TypeConverter
    public static String fromVicCardList(ArrayList<vicCard> vicCardValues) {
        if(vicCardValues == null) {
            return null;
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<vicCard>>() {
        }.getType();
        String json = gson.toJson(vicCardValues, type);
        return json;
    }
    @TypeConverter
    public static ArrayList<vicCard> toVicCardList(String vicCardValuesString) {

        if (vicCardValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<vicCard>>() {
        }.getType();
        ArrayList<vicCard> productCategoriesList = gson.fromJson(vicCardValuesString, type);
        return productCategoriesList;
    }
}
