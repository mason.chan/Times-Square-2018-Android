package com.timessquare.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.ColumnInfo.NOCASE
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.timessquare.apiservice.datamodel.OpenHour


/**
 *   Created by Mason on 1/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@Entity(tableName = "shop") // database query name
data class ShopEntity(
        @PrimaryKey(autoGenerate = false) var id: Int,
        var number: String = "",
        var isShopNotFood: Boolean = true,
        @ColumnInfo(collate = NOCASE)var name: String = "",
        var nameZh: String? = "",
        var floorId: Int = 0,
        var categoryId: Int = 0,
        var thumbImage: String? = "",
        var description: String? = "",
        var descriptionZh: String? = "",
        var openHours: ArrayList<OpenHour>? = null,
        var contact: String? = "",
        var detailImage:  String? = "",
        var menu: ArrayList<String>? = null,
        var vicPrivilege: Boolean? = false,
        var bvicPrivilege: Boolean? = false,
        var gvicPrivilege: Boolean? = false,
        var vicYearRoundOffer: Boolean? = false,
        var vicBirthdayOffer: Boolean? = false,
        var officeClub: Boolean? = false,
        var clubOn: Boolean? = false,
        var vicYearRoundOfferDescription: String? = "VIC YRO",
        var vicYearRoundOfferDescriptionZh: String? = "VIC YRO zh",
        var vicBirthdayOfferDesctiption: String? = "VIC BO",
        var vicBirthdayOfferDesctiptionZh: String? = "VIC BO zh "
)

data class SubShop(
        var id: Int,
        var categoryId: Int = 0,
        var isShopNotFood: Boolean = true,
        var number: String = "",
        var thumbImage: String? = "",
        var name: String = "",
        var nameZh: String? = "",
        var floorId: Int = 0
)