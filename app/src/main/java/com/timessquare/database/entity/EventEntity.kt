package com.timessquare.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*


/**
 *   Created by Mason on 23/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@Entity(tableName = "event")
data class EventEntity(
        @PrimaryKey(autoGenerate = false) var id: Int,
        var sortId: Int,
        var classId: Int,
        var title: String,
        var titleZh: String = "",
        var titleZhsc: String = "",
        var thumb: String? = null,
        var poster: ArrayList<String>? = null,
        var posterZh: ArrayList<String>? = null,
        var terms: String? = null,
        var termsZh: String? = "",
        var termsZhsc: String? = "",
        var startDate: Date? = null,
        var endDate: Date? = null,
        var valid: Boolean = true
)

