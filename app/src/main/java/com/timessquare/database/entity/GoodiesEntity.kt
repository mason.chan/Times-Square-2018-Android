package com.timessquare.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 *   Created by Mason on 4/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@Entity(tableName = "goodies")
data class GoodiesEntity(
        @PrimaryKey(autoGenerate = false) var id: Int,
        @ColumnInfo(name = "goodies_title") var title: String,
        @ColumnInfo(name = "goodies_title_chi") var title_cn: String,
        @ColumnInfo(name = "goodies_description") var description: String,
        @ColumnInfo(name = "goodies_description_chi") var description_cn: String,
        @ColumnInfo(name = "goodies_image") var image: String,
        @ColumnInfo(name = "goodies_thumb") var thumb: String,
        @ColumnInfo(name = "goodies_code") var code: String,
        @ColumnInfo(name = "goodies_redeemable") var redeemable: Boolean,
        @ColumnInfo(name = "goodies_needed_days") var needed_days: Boolean

)