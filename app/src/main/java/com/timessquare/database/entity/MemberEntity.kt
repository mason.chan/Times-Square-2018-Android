package com.timessquare.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*
import kotlin.collections.ArrayList

/**
 *   Created by Mason on 5/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@Entity(tableName = "member")
data class MemberEntity(
        @PrimaryKey() var id: Int,
        var email: String? = null,
        var name: String? = null,
        var refreshToken: String? = null,
        var card: ArrayList<vicCard>? = null,
        var accessToken: String? = null,
        var message: String? = null,
        var errorMessage: String? = null,
        var errorCode: String? = null
)
class vicCard(
        var cardPin: String,
        var cardCode: String,
        var cardId: Int,
        var cardTypeId: Int,
        var cardStatus: Int,
        var firstName: String,
        var lastName: String,
        var email: String,
        var tel: String,
        var point: Int,
        var expiryDate: Date
)