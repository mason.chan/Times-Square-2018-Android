package com.timessquare.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.timessquare.database.dao.EventDao
import com.timessquare.database.dao.GoodiesDao
import com.timessquare.database.dao.MemberDao
import com.timessquare.database.dao.ShopDao
import com.timessquare.database.entity.*


/**
 *   Created by Mason on 1/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@Database(entities = arrayOf(ShopEntity::class, GoodiesEntity::class, EventEntity::class, MemberEntity::class),version = 6, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDataBase : RoomDatabase() {

    abstract fun shopDao(): ShopDao
    abstract fun goodiesDao(): GoodiesDao
    abstract fun eventDao(): EventDao
    abstract fun memberDao(): MemberDao


    companion object {

        @Volatile private var INSTANCE: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        AppDataBase::class.java, "Times-Square.db")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration() // for update database version do not crash
                        .build()
    }
}
