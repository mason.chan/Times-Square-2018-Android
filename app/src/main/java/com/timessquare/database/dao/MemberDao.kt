package com.timessquare.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.timessquare.database.entity.MemberEntity

/**
 *   Created by Mason on 5/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@Dao
interface MemberDao {

    @Query("SELECT * FROM member")
    fun getAll(): List<MemberEntity>

    @Query("SELECT * FROM member LIMIT 1")
    fun getFirst(): MemberEntity

    @Insert(onConflict = REPLACE)
    fun insert(member: MemberEntity)

    @Query("DELETE FROM member")
    fun deleteAll()
}