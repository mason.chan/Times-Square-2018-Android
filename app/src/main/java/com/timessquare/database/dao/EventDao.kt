package com.timessquare.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.timessquare.database.entity.EventEntity
import java.util.*

/**
 *   Created by Mason on 24/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@Dao
interface EventDao {

    @Query("SELECT * FROM event")
    fun getAll(): List<EventEntity>

    @Query("SELECT * FROM event WHERE id = :id")
    fun getById(id: Int): EventEntity

    @Insert(onConflict = REPLACE)
    fun insert(events: EventEntity)

    @Insert(onConflict = REPLACE)
    fun insertAll(eventeList: List<EventEntity>)

    @Query("DELETE FROM event")
    fun deleteAll()

    /**
     * EventType
     */
    @Query("SELECT * FROM event WHERE classId = :eventType AND startDate <= :startDate AND endDate >= :endDate ORDER BY sortId")
    fun getEventType(eventType: Int, startDate: Date, endDate: Date): List<EventEntity>
    /**
     * VIC Programs
     */
    @Query("SELECT * FROM event WHERE classId IN (3, 7) AND valid ORDER BY sortId")
    fun getVICPrograms(): List<EventEntity>

    @Query("SELECT * FROM event WHERE classId IN (4, 8) AND valid ORDER BY sortId ")
    fun getBVICPrograms(): List<EventEntity>

    @Query("SELECT * FROM event WHERE classId IN (5, 9) AND valid ORDER BY sortId")
    fun getGVICPrograms(): List<EventEntity>

    @Query("SELECT * FROM event WHERE classId = 6 AND valid ORDER BY sortId")
    fun getGVICExclusivePrograms(): List<EventEntity>

    @Query("SELECT * FROM event WHERE classId = 7 AND valid ORDER BY sortId")
    fun getVICGuestPrograms(): List<EventEntity>

    @Query("SELECT * FROM event WHERE classId = 8 AND valid ORDER BY sortId")
    fun getBVICGuestPrograms(): List<EventEntity>

    @Query("SELECT * FROM event WHERE classId = 9 AND valid ORDER BY sortId")
    fun getGVICGuestPrograms(): List<EventEntity>

}

enum class EventType(var value: Int) {
    whatsup(1),
    goodies(2),
    vic(3),
    beautyVic(4),
    globalVic(5),
    globalVicExclusive(6),
    vicGuest(7),
    beautyVicGuest(8),
    globalVicGuest(9)
}