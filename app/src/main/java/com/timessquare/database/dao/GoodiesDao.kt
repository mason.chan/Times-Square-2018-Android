package com.timessquare.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.timessquare.database.entity.GoodiesEntity

/**
 *   Created by Mason on 11/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@Dao
interface GoodiesDao {

    @Query("SELECT * FROM goodies")
    fun getAll(): List<GoodiesEntity>

    @Query("SELECT * FROM goodies WHERE id = :id")
    fun getById(id: Int) : GoodiesEntity

    @Insert(onConflict = REPLACE)
    fun insert(goodies: GoodiesEntity)

    @Insert(onConflict = REPLACE)
    fun insertAll(goodiesList: List<GoodiesEntity>)

    @Query("DELETE FROM goodies")
    fun deleteAll()
}