package com.timessquare.database.dao

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.ColumnInfo.NOCASE
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.timessquare.database.entity.ShopEntity
import com.timessquare.database.entity.SubShop

/**
 *   Created by Mason on 1/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@Dao
interface ShopDao {

    /**
     * Get All shop
     */
    @Query("SELECT * FROM shop")
    fun getAll(): List<ShopEntity>

    @Query("SELECT * FROM shop WHERE isShopNotFood = :isShopNotFood")
    fun getAll(isShopNotFood: Boolean): List<ShopEntity>

    /**
     * Get All shop BaseInfo
     */
    @Query("SELECT id, categoryId, contact, isShopNotFood, number, thumbImage, name, nameZh,floorId FROM shop WHERE isShopNotFood = :isShopNotFood ORDER BY name")
    fun getAllBaseInfo(isShopNotFood: Boolean): List<SubShop>
    /**
     * Get By Id
     */
    @Query("SELECT * FROM shop WHERE id = :id LIMIT 1")
    fun getById(id: Int): ShopEntity

    /**
     * Get By VIC Type
     */
    @Query("SELECT * FROM shop WHERE vicPrivilege = 1 ORDER BY name")
    fun getByVICType(): List<ShopEntity>

    @Query("SELECT * FROM shop WHERE bvicPrivilege = 1 ORDER BY name")
    fun getByBVICType(): List<ShopEntity>

    @Query("SELECT * FROM shop WHERE gvicPrivilege = 1 ORDER BY name")
    fun getByTVICType(): List<ShopEntity>

    /**
     * Privilege
     */
    @Query("SELECT * FROM shop WHERE vicPrivilege ORDER BY name")
    fun getVICPrivilege(): List<ShopEntity>

    @Query("SELECT * FROM shop WHERE bvicPrivilege ORDER BY name")
    fun getBVICPrivilege(): List<ShopEntity>

    /**
     * Birthday Offers
     */
    @Query("SELECT * FROM shop WHERE vicBirthdayOffer ORDER BY name")
    fun getVICBirthdayOffers(): List<ShopEntity>
    /**
     * Year Round Offers
     */
    @Query("SELECT * FROM shop WHERE vicYearRoundOffer ORDER BY name")
    fun getVICYearRoundOffers(): List<ShopEntity>

    /**
     * Shop Filter
     */
    // consider select * will very slow, so only select the following field.
    @Query("SELECT id, categoryId, contact, isShopNotFood, number, thumbImage, name, nameZh,floorId FROM shop WHERE name LIKE :prefix AND isShopNotFood = :isShopNotFood ORDER BY name")
    fun filterShopP(isShopNotFood: Boolean, prefix: String): List<SubShop>

    @Query("SELECT id, categoryId, contact, isShopNotFood, number, thumbImage, name, nameZh,floorId FROM shop WHERE categoryId = :categoryId AND name LIKE :prefix AND isShopNotFood = :isShopNotFood ORDER BY name")
    fun filterShopCP(isShopNotFood: Boolean, categoryId: Int, prefix: String): List<SubShop>

    @Query("SELECT id, categoryId, contact, isShopNotFood, number, thumbImage, name, nameZh,floorId FROM shop WHERE floorId = :floorId AND name LIKE :prefix AND isShopNotFood = :isShopNotFood ORDER BY name")
    fun filterShopFP(isShopNotFood: Boolean, floorId: Int, prefix: String): List<SubShop>

    @Query("SELECT id, categoryId, contact, isShopNotFood, number, thumbImage, name, nameZh,floorId FROM shop WHERE categoryId = :categoryId AND floorId = :floorId AND name LIKE :prefix AND isShopNotFood = :isShopNotFood ORDER BY name")
    fun filterShopCFP(isShopNotFood: Boolean, categoryId: Int, floorId: Int, prefix: String): List<SubShop>


    /**
     * Insert Shop
     */
    @Insert(onConflict = REPLACE)
    fun insert(shop: ShopEntity)

    @Insert(onConflict = REPLACE)
    fun insertAll(shopList: List<ShopEntity>)

    /**
     * Clean All Shop
     */
    @Query("DELETE FROM shop")
    fun deleteAll()
}