package com.timessquare.utils

import android.content.Context
import android.util.Log.i
import com.timessquare.MainActivity
import com.timessquare.database.entity.ShopEntity
import com.timessquare.shopanddine.ShopDineItem

/**
 *   Created by Mason on 21/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
object EntityToItem {


    /**
     * Shop List
     */
    fun getShopList(ctx: Context, shopList: List<ShopEntity>): List<ShopDineItem> {
        var shopDineList = ArrayList<ShopDineItem>()
        shopList.forEach {
            if(MainActivity[ctx].getLanguage() == LocaleHelper.englishLocale()) {
//            if (MainActivity[ctx].isEnglish) {
                shopDineList.add(ShopDineItem(it.id, it.thumbImage, it.name, it.number, it.floorId, ctx))
            } else {
                shopDineList.add(ShopDineItem(it.id, it.thumbImage, it.nameZh, it.number, it.floorId, ctx))
            }
        }
        i("getShopList", "shopDineList  " + shopDineList.size.toString())
        return shopDineList
    }
}