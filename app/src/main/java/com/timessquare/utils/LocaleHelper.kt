package com.timessquare.utils

import android.annotation.TargetApi
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.preference.PreferenceManager

import java.util.Locale

/**
 * Created by Mason on 7/6/2018.
 * Hita Group
 * mason.chan@sllin.com
 */
object LocaleHelper {

    fun englishLocale(): Locale {
        return Locale("en")
    }

    fun hkLocale(): Locale {
        return Locale("zh", "HK")
    }

    fun setSharedPreference(locale: Locale): String {
        if (locale == englishLocale()) {
            return "en"
        } else {
            return "zh-rhk"
        }
    }

    fun enlish(): String {
        return "en"
    }

    fun hk(): String {
        return "zh-rhk"
    }
}