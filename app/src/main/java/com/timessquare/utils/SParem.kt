package com.timessquare.utils

/**
 *   Created by Mason on 24/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

object SParem {
   const val  Terms_Of_Service_Agreed = "Terms_Of_Service_Agreed"
   const val  Language_Selected = "Language_Selected"

   /**
    * Member Login
    */
   const val Member_Email = "Member_Email"
   const val Member_Password = "Member_Password"
//   const val Member_Status = "Member_Status"
   const val Member_Logined = "Member_Logined"
}