package com.timessquare.utils

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.support.v4.content.ContextCompat
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.timessquare.R
import com.orhanobut.dialogplus.DialogPlus

/**
 *   Created by Mason on 17/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

open class IOSDialog(context: Context) : Dialog(context, R.style.ios_dialog_style) {

    class Builder(private val mContext: Context) {
        private var mIosDialog: IOSDialog? = null
        private var mTitle: CharSequence? = null
        private var mMessage: CharSequence? = null
        private var mPositiveButtonText: CharSequence? = null
        private var mPositiveButtonTextColorRes: Int? = null
        private var mNegativeButtonText: CharSequence? = null
        private var mNegativeButtonTextColorRes: Int? = null
        private var mContentView: View? = null
        private var mPositiveButtonClickListener: DialogInterface.OnClickListener? = null
        private var mNegativeButtonClickListener: DialogInterface.OnClickListener? = null
        private var mCancelable = true

        fun setTitle(titleId: Int): Builder {
            this.mTitle = mContext.getText(titleId)
            return this
        }

        fun setTitle(title: CharSequence): Builder {
            this.mTitle = title
            return this
        }

        fun setMessage(messageId: Int): Builder {
            this.mMessage = mContext.getText(messageId)
            return this
        }

        fun setMessage(message: CharSequence): Builder {
            this.mMessage = message
            return this
        }

        fun setPositiveButton(textId: Int, listener: DialogInterface.OnClickListener?): Builder {
            this.mPositiveButtonText = mContext.getText(textId)
            this.mPositiveButtonClickListener = listener
            return this
        }
        fun setPositiveButton(textId: Int,  colorRes: Int, listener: DialogInterface.OnClickListener?): Builder {
            this.mPositiveButtonText = mContext.getText(textId)
            this.mPositiveButtonTextColorRes = ContextCompat.getColor(mContext, colorRes)
            this.mPositiveButtonClickListener = listener
            return this
        }
        fun setPositiveButton(text: CharSequence, listener: DialogInterface.OnClickListener): Builder {
            this.mPositiveButtonText = text
            this.mPositiveButtonClickListener = listener
            return this
        }

        fun setNegativeButton(textId: Int, listener: DialogInterface.OnClickListener): Builder {
            this.mNegativeButtonText = mContext.getText(textId)
            this.mNegativeButtonClickListener = listener
            return this
        }

        fun setNegativeButton(textId: Int, colorRes: Int, listener: DialogInterface.OnClickListener): Builder {
            this.mNegativeButtonText = mContext.getText(textId)
            this.mNegativeButtonTextColorRes = ContextCompat.getColor(mContext, colorRes)
            this.mNegativeButtonClickListener = listener
            return this
        }

        fun setNegativeButton(text: CharSequence, listener: DialogInterface.OnClickListener): Builder {
            this.mNegativeButtonText = text
            this.mNegativeButtonClickListener = listener
            return this
        }

        fun setCancelable(cancelable: Boolean): Builder {
            this.mCancelable = cancelable
            return this
        }

        fun setContentView(contentView: View): Builder {
            this.mContentView = contentView
            return this
        }

        fun create(): IOSDialog {
            val inflater = LayoutInflater.from(mContext)
            val dialogView = inflater.inflate(R.layout.ios_dialog, null)
            mIosDialog = IOSDialog(mContext)
            mIosDialog!!.setCancelable(mCancelable)

            val tvTitle = dialogView.findViewById<View>(R.id.title) as TextView
            val tvMessage = dialogView.findViewById<View>(R.id.message) as TextView
            val btnCancel = dialogView.findViewById<View>(R.id.cancel_btn) as Button
            val btnConfirm = dialogView.findViewById<View>(R.id.confirm_btn) as Button
            val horizontal_line = dialogView.findViewById<View>(R.id.horizontal_line)
            val vertical_line = dialogView.findViewById<View>(R.id.vertical_line)
            val btns_panel = dialogView.findViewById<View>(R.id.btns_panel)

            // 设置标题
            if(mTitle!= null) {
                tvTitle.text = mTitle
            } else {
                tvTitle.visibility = View.GONE
            }
            // 设置内容区域
            if (mContentView != null) {
                // if no message set add the contentView to the dialog body
                val rl = dialogView
                        .findViewById<View>(R.id.message_layout) as LinearLayout
                rl.removeAllViews()
                val params = LinearLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT)
                rl.addView(mContentView, params)
            } else {
                tvMessage.text = mMessage
            }
            // 设置按钮区域
            if (mPositiveButtonText == null && mNegativeButtonText == null) {
                setPositiveButton(R.string.ios_dialog_default_ok, null)
                btnConfirm.setBackgroundResource(R.drawable.iosdialog_sigle_btn_selector)
                btnCancel.visibility = View.GONE
                vertical_line.visibility = View.GONE
            } else if (mPositiveButtonText != null && mNegativeButtonText == null) {
                btnConfirm.setBackgroundResource(R.drawable.iosdialog_sigle_btn_selector)
                btnCancel.visibility = View.GONE
                vertical_line.visibility = View.GONE
            } else if (mPositiveButtonText == null && mNegativeButtonText != null) {
                btnConfirm.visibility = View.GONE
                btnCancel.setBackgroundResource(R.drawable.iosdialog_sigle_btn_selector)
                vertical_line.visibility = View.GONE
            }
            if (mPositiveButtonText != null) {
                btnConfirm.text = mPositiveButtonText
                if(mPositiveButtonTextColorRes != null) btnConfirm.setTextColor(mPositiveButtonTextColorRes!!)
                btnConfirm.setOnClickListener {
                    if (mPositiveButtonClickListener != null) {
                        mPositiveButtonClickListener!!.onClick(mIosDialog,
                                DialogInterface.BUTTON_POSITIVE)
                    }
                    mIosDialog!!.dismiss()
                }
            }
            if (mNegativeButtonText != null) {
                btnCancel.text = mNegativeButtonText
                if(mNegativeButtonTextColorRes != null) btnCancel.setTextColor(mNegativeButtonTextColorRes!!)
                btnCancel.setOnClickListener {
                    if (mNegativeButtonClickListener != null) {
                        mNegativeButtonClickListener!!.onClick(mIosDialog,
                                DialogInterface.BUTTON_NEGATIVE)
                    }
                    mIosDialog!!.dismiss()
                }
            }

            // 调整一下dialog的高度，如果高度充满屏幕不好看
            // 计算一下Dialog的高度,如果超过屏幕的4/5，则最大高度限制在4/5
            // 1.计算dialog的高度
            // TODO 测试发现的问题：如果放入一大串没有换行的文本到message区域，会导致测量出来的高度偏小，从而导致实际显示出来dialog充满了整个屏幕
            dialogView.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            val dialogHeight = dialogView.measuredHeight
            // 2.得到屏幕高度
            val wm = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val metrics = DisplayMetrics()
            wm.defaultDisplay.getMetrics(metrics)
            val maxHeight = (metrics.heightPixels * 0.8).toInt()
            val dialogParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            // 3.如果高度超限，限制dialog的高度
            if (dialogHeight >= maxHeight) {
                dialogParams.height = maxHeight
            }
            mIosDialog!!.setContentView(dialogView, dialogParams)

            return mIosDialog as IOSDialog
        }

        fun show(): IOSDialog {
            mIosDialog = create()
            mIosDialog!!.show()
            return mIosDialog as IOSDialog
        }
    }
}
