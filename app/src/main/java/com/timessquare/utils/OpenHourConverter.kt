package com.timessquare.utils

import android.content.Context
import com.timessquare.R
import com.timessquare.apiservice.datamodel.OpenHour

/**
 *   Created by Mason on 21/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
object OpenHourConverter {

    fun openHourToReadableString(ctx: Context, openHour: ArrayList<OpenHour>): String {
        var rule = ArrayList<String>()
        openHour.forEach {
            var dayOpen = it.dayOpen
            var timeOpenList = it.timeOpenList
            var dayArr = ArrayList<String>()
            var timeArr = ArrayList<String>()

            /**
             * This part for Set Day
             */
            if (dayOpen.weekday.isNotEmpty()) {
                if (dayOpen.weekday.indexOf("-") != -1) {
                    var weekdayCond = dayOpen.weekday.split("-")
                    var startWeekday = weekdayCond[0].toInt()
                    var endWeekday = weekdayCond[1].toInt()
                    dayArr.add(ctx.resources.getStringArray(R.array.shop_week_list)[startWeekday]
                            + " "
                            + ctx.resources.getString(R.string.shop_to)
                            + " "
                            + ctx.resources.getStringArray(R.array.shop_week_list)[endWeekday])
                } else {
                    var weekdayCond = dayOpen.weekday.split(",")
                    weekdayCond.forEach {
                        dayArr.add(ctx.resources.getStringArray(R.array.shop_week_list)[it.toInt()])
                    }
                }
            }
            if (dayOpen.pre_holiday) {
                dayArr.add(ctx.resources.getString(R.string.shop_pre_holiday))
            }
            if (dayOpen.holiday) {
                dayArr.add(ctx.resources.getString(R.string.shop_holiday))
            }

            /**
             * This part for Set Time
             */
            timeOpenList.forEach {
                timeArr.add(it.open + " - " + it.close)
            }
            if (timeArr.isEmpty()) {
                timeArr.add(ctx.resources.getString(R.string.shop_closed))
            }
            rule.add(dayArr.joinToString(", ") + " " + timeArr.joinToString(", "))
        }
        return rule.joinToString("\n")
    }
}