package com.timessquare.utils


import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
//import com.hita.apiservice.datas.OpenHour
//import com.hita.apiservice.datas.VICAttribute

import java.lang.reflect.Type
import java.util.Collections

import android.util.Log.i

/**
 * Created by Mason on 20/6/2018.
 * Hita Group
 * mason.chan@sllin.com
 */
object StringConverters {

    internal var gson = Gson()

    /**
     * For List String
     * @param data
     * @return List<String>
    </String> */
    fun stringToListString(data: String?): List<String> {
        if (data == null) {
            return emptyList()
        }
        val listType = object : TypeToken<List<String>>() {

        }.type

        return gson.fromJson(data, listType)
    }

    /**
     *
     * @param list
     * @return String
     */
    fun listStringToString(list: List<String>): String {
        return gson.toJson(list)
    }
}