package com.timessquare.utils

import android.support.v4.app.Fragment

/**
 *   Created by Mason on 23/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

inline fun keyName(clazz: Any): String {
    val lastIndex = clazz.toString().indexOf("$").takeIf { it != -1 } ?: clazz.toString().lastIndex + 1
    return clazz.toString().substring(
            clazz.toString().lastIndexOf(".") + 1,
            lastIndex
    )
}
val Fragment.requireArguments
    get() = this.arguments ?: throw IllegalStateException("Arguments should exist!")