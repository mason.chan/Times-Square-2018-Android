package com.timessquare.utils

/**
 *   Created by Mason on 23/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

object Constant {
    const val TAG_INTENT = "TAG_INTENT"
    const val TAG_INTENT2 = "TAG_INTENT__2"
    const val TAG_INTENT3 = "TAG_INTENT____3"

    // Account Manager
    const val ACCOUNT_TYPE = "com.timessquare"
    const val API_EMAIL = "email"
    const val API_PASSWORD = "password"
    const val API_ACCESS_TOKEN = "access_token"
}