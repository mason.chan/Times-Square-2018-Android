package com.timessquare.vicoffers

import android.support.v7.widget.RecyclerView
import android.view.View
import com.timessquare.views.shopDineView
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout

/**
 *   Created by Mason on 19/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class VICOffersUI(var titleString: String) : AnkoComponent<VICOffersFragment> {

lateinit var mRecyclerView: RecyclerView

    override fun createView(ui: AnkoContext<VICOffersFragment>): View {
        return with(ui) {
            frameLayout {
                shopDineView {
                    this@VICOffersUI.mRecyclerView = mRecyclerView
                    mTitleBar.addBackButton().addTitle(titleString)
                }
            }
        }
    }
}