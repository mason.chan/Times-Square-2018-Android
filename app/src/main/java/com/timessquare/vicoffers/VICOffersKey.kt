package com.timessquare.vicoffers

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 19/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class VICOffersKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(VICOffersKey))

    override fun createFragment(): BaseFragment = VICOffersFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}