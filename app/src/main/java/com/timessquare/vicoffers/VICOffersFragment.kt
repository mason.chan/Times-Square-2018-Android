package com.timessquare.vicoffers

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.database.entity.ShopEntity
import com.timessquare.offershopdetail.OfferShopDetailKey
import com.timessquare.shopanddine.ShopDineItem
import com.timessquare.utils.Constant
import com.timessquare.utils.EntityToItem
import com.timessquare.vic.vicprograms.OfferType
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 19/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class VICOffersFragment : BaseFragment() {
    val titleString: String by lazy { arguments?.getString(Constant.TAG_INTENT) ?: "" }
    val offerType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT2) ?: 0 }

    lateinit var mView: VICOffersUI
    private var mFastItemAdapter: FastItemAdapter<ShopDineItem> = FastItemAdapter()
    var shopEntityList = ArrayList<ShopEntity>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.i("VICOffersFragment", "onCreateView" + titleString)
        mView = VICOffersUI(titleString)
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("VICOffersFragment", "onViewCreated" + titleString)
        mFastItemAdapter!!.withOnClickListener { v, adapter, item, position ->
            MainActivity.get(view.context).navigateTo(OfferShopDetailKey().withArgs {
                putInt(Constant.TAG_INTENT, item.mId)
                putInt(Constant.TAG_INTENT2, offerType)
            })
            false
        }
        mView.mRecyclerView.layoutManager = GridLayoutManager(context, 2)
        mView.mRecyclerView.adapter = mFastItemAdapter
        addItem(view)
    }

    fun addItem(view: View) {
         shopEntityList.clear()
        when (offerType) {
            OfferType.VIC_Birthday_Offers.value -> shopEntityList.addAll(MainActivity.get(view.context).database.shopDao().getVICBirthdayOffers())
            OfferType.VIC_Year_Round_Offers.value -> shopEntityList.addAll(MainActivity.get(view.context).database.shopDao().getVICYearRoundOffers())
        }
        mFastItemAdapter.clear()
        mFastItemAdapter!!.add(EntityToItem.getShopList(view.context, shopEntityList))
    }

}