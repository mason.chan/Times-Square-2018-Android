package com.timessquare.memberchangepassword

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.timessquare.R
import com.timessquare.memberaccountsettings.MemberAccountSettingsFragment
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.design.textInputLayout

/**
 *   Created by Mason on 12/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberChangePasswordUI() : AnkoComponent<MemberChangePasswordFragment> {
    lateinit var mTitleBar: TitleBar
    lateinit var mOldPasswordEditText: EditText
    lateinit var mOldPasswordEextLayout: TextInputLayout
    lateinit var mNewPasswordEditText: EditText
    lateinit var mNewPasswordEextLayout: TextInputLayout
    lateinit var mOKButton: Button

    override fun createView(ui: AnkoContext<MemberChangePasswordFragment>): View {
        return with(ui) {
            constraintLayout {
                mTitleBar = titleBar {
                    id = R.id.memberLoginTitleBar
                }.addTitle(R.string.member_change_password_title).addBackButton().addBackgroundColor(R.color.color_cover)
                        .lparams(width = matchParent) {
                            topToTop = PARENT_ID
                        }
                verticalLayout {
                    padding = dimen(R.dimen.padding_small)
                    // Old Password
                    textView(R.string.member_change_password_old_password_text_view) {
                        textColorResource = R.color.color_black
                        textSizeDimen = R.dimen.text_normal
                    }
                    mOldPasswordEditText = include<EditText>(R.layout.car_park_edit_text) {
                        hintResource = R.string.member_change_password_old_password_edit_text_hint
                    }
                    mOldPasswordEextLayout = textInputLayout { }
                    // New Password
                    textView(R.string.member_change_password_new_password_text_view) {
                        textColorResource = R.color.color_black
                        textSizeDimen = R.dimen.text_normal
                    }
                    mNewPasswordEditText = include<EditText>(R.layout.car_park_edit_text) {
                        hintResource = R.string.member_change_password_new_password_edit_text_hint
                    }
                    mNewPasswordEextLayout = textInputLayout { }
                }.lparams(matchParent, wrapContent) {
                    topToBottom = R.id.memberLoginTitleBar
                }
                mOKButton = button {
                    textResource = R.string.member_change_password_ok_button
                    textSizeDimen = R.dimen.text_subject
                    backgroundResource = R.drawable.button_sure
                    paddingHorizontal = dimen(R.dimen.padding_double)
                }.lparams(wrapContent, wrapContent) {
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                }
            }
        }
    }
}