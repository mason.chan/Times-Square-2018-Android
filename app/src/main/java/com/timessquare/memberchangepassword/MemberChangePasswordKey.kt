package com.timessquare.memberchangepassword

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 12/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreatot")
@Parcelize
data class MemberChangePasswordKey(override val tag: String) : BaseKey(tag) {

    constructor() : this(keyName(MemberChangePasswordKey))

    override fun createFragment(): BaseFragment = MemberChangePasswordFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}