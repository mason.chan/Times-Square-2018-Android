package com.timessquare.memberchangepassword

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.i
import com.pawegio.kandroid.runOnUiThread
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.apiservice.datamodel.ChangePasswordModel
import com.timessquare.memberaccountsettings.MemberAccountSettingsUI
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx

/**
 *   Created by Mason on 12/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberChangePasswordFragment : BaseFragment() {

    lateinit var mView: MemberChangePasswordUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        i("MemberChangePasswordFragment", "onCreateView")
        mView = MemberChangePasswordUI()
        return mView.createView(AnkoContext.create(activity!!, this))

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView.mOKButton.onClick {
            AttemptChangePassword()
        }
    }

    private fun AttemptChangePassword() {
        var oldPassword = mView.mOldPasswordEditText.text.toString()
        var newPassword = mView.mNewPasswordEditText.text.toString()
        /**
         * field Checking
         */
        if (oldPassword.isEmpty()) {
            mView.mOldPasswordEextLayout.error = getString(R.string.member_login_password_error)
            return
        }
        if (newPassword.isEmpty()) {
            mView.mNewPasswordEextLayout.error = getString(R.string.member_login_password_error)
            return
        }
        var accessToken = MainActivity[ctx].database.memberDao().getFirst().accessToken!!
        /**
         * Create Observable
         */
        val changePasswordObervable = MainActivity[ctx].apiService.changePassword(accessToken, oldPassword, newPassword)
        /**
         * Create Observer
         */
        var changePasswordObserver = object : Observer<ChangePasswordModel> {
            override fun onComplete() {
                i("change Password", " onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                i("change Password", " onSubscribe" + d)
                dismissDialog()
            }

            override fun onNext(t: ChangePasswordModel) {
                i("change Password", " onNext   " + t.message + t.errorCode + t.errorMessage)
                if (t.message != null) {
                    runOnUiThread() {
                        MainActivity[ctx].AlertDialog("Success", t.message!!, true).show()
                    }
                } else {
                    runOnUiThread() {
                        MainActivity[ctx].AlertDialog("Error", t.errorMessage!!, false).show()
                    }
                }
            }

            override fun onError(e: Throwable) {
                i("change Password", " onError" + e)
            }
        }
        /**
         * Subscribe
         */

        changePasswordObervable.subscribeOn(Schedulers.io())
                .doOnSubscribe { popDialog(msgId = R.string.member_login_forgot_password_loading_dialog_message, cancelable = false) }
                .doFinally { }
                .subscribeWith(changePasswordObserver)
    }
}