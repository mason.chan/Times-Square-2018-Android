package com.timessquare.views

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.*
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.index.IndexKey
import com.timessquare.menu.MenuKey
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7._Toolbar
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.custom.ankoView

/**
 * Created by elton on 19/4/2018.
 **/
class TitleBar : FrameLayout {

    lateinit var mView: AnkoContext<TitleBar>
    lateinit var mTitleTextView: TextView
    lateinit var mSubTitleTextView: TextView
    lateinit var layout: FrameLayout
    lateinit var vertiallayout: LinearLayout
    lateinit var toolbar: Toolbar
    lateinit var bkg: ImageView
    lateinit var bkc: ImageView
    lateinit var backButton: ImageView

    private fun init(): AnkoContext<TitleBar> {

        mView = AnkoContext.createDelegate(this).apply {
            layout = frameLayout {
                bkc = imageView().lparams(matchParent, matchParent)
                bkg = imageView {

                    adjustViewBounds = true
                }.lparams(matchParent, wrapContent)

                // Add toobar
                toolbar = toolbar {
                    id = R.id.titlebar
                    layoutParams = Toolbar.LayoutParams(matchParent, wrapContent)


                }.lparams(
                        matchParent,
                        matchParent
                )
                bottomPadding = 8
                clipToPadding = false
                layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
            }
            layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
        }
        return mView
    }

    fun addLogo(): TitleBar {
        (toolbar as _Toolbar).apply {
            imageView(R.drawable.ic_logo) {
                adjustViewBounds = true
            }.lparams(wrapContent, dimen(R.dimen.logo_height)) {
                gravity = Gravity.CENTER
                verticalMargin = dimen(R.dimen.margin_normal)
            }
        }
        return mView.owner
    }

    fun addTitle(titleRes: Int): TitleBar {
        (toolbar as _Toolbar).apply {
            mTitleTextView = include<TextView>(R.layout.title_textview) {
                textResource = titleRes
                textSize = 18f
                textColor = Color.BLACK
            }.lparams(
                    wrapContent,
                    wrapContent
            ) {
                gravity = Gravity.CENTER
            }
        }
        return mView.owner
    }

    fun addTitle(titleString: String): TitleBar {
        (toolbar as _Toolbar).apply {
            mTitleTextView = include<TextView>(R.layout.title_textview) {
                text = titleString
                textSize = 18f
                textColor = Color.BLACK
            }.lparams(
                    wrapContent,
                    wrapContent
            ) {
                gravity = Gravity.CENTER
            }
        }
        return mView.owner
    }

    fun addBackgroundColor(backgroundColorRes: Int): TitleBar {
//        (layout as _FrameLayout).apply {
        bkc.apply {
            backgroundColorResource = backgroundColorRes
        }
        return mView.owner
    }

    fun addBackground(backgroundRes: Int): TitleBar {
        bkg.apply {
            imageResource = backgroundRes
        }
        return mView.owner
    }

    fun addButton(imgRes: Int = R.drawable.ic_cancel, gravity: Int = Gravity.START, buttonId: Int = 0, marginRes: Int? = null, onClickListener: View.OnClickListener? = null): TitleBar {
//        val attrs = intArrayOf(android.R.attr.selectableItemBackground)
        (toolbar as _Toolbar).apply {
            backButton = imageView(imgRes) {
               id = buttonId
                isClickable = true
                verticalPadding = dimen(R.dimen.navigation_padding_button)
                horizontalPadding = dimen(R.dimen.navigation_padding_button)
                if (onClickListener != null) {
                    setOnClickListener(onClickListener)
                }
            }.lparams(dimen(R.dimen.navigation_button), dimen(R.dimen.navigation_button)) {
                this.gravity = gravity
                if (gravity == Gravity.END) {
                    if (marginRes != null) {
                        marginEnd = dimen(marginRes)
                    }
                }
            }
        }
        return mView.owner
    }

    fun addBackButton(onClickListener: OnClickListener? = null): TitleBar {
        // Default behaviour as clicking the back button on Navigation bar
        return addButton(R.drawable.ic_back, buttonId = R.id.shop_search_button, onClickListener = OnClickListener { (context as MainActivity).onBackPressed() })
    }

    fun addCancelButton(onClickListener: OnClickListener? = null): TitleBar {
        // Default behaviour as clicking the back button on Navigation bar
        return addButton(R.drawable.ic_cancel, onClickListener = OnClickListener { (context as MainActivity).onBackPressed() })
    }

    fun addBackButton(isWhite: Boolean, onClickListener: OnClickListener? = null): TitleBar {
        return addButton(R.drawable.ic_back_white, onClickListener = OnClickListener { (context as MainActivity).onBackPressed() })
    }

    fun addMenuButton(onClickListener: OnClickListener? = null): TitleBar {
        // Default behaviour as clicking the back button on Navigation bar
//        return addButton(R.drawable.ic_menu, onClickListener = OnClickListener { (context as MainActivity).openDropDownMenu() })
        return addButton(R.drawable.ic_menu, onClickListener = OnClickListener { (context as MainActivity).navigateTo(MenuKey()) }, buttonId = R.id.menuButton)
    }

    fun addMemberButton(isWhite: Boolean, onClickListener: OnClickListener? = null): TitleBar {
        if (isWhite) {
            return addButton(R.drawable.ic_login_white, Gravity.END, marginRes = R.dimen.margin_normal, onClickListener = OnClickListener { (context as MainActivity).requestToGoToMemberPage() })

        } else {
            return addButton(R.drawable.ic_login, Gravity.END, marginRes = R.dimen.margin_normal, onClickListener = OnClickListener { (context as MainActivity).requestToGoToMemberPage() })
        }
    }

    fun addClickableText(text: String? = null, textRes: Int? = null, gravity: Int = Gravity.START, id: Int = 0, onClickListener: OnClickListener? = null): TitleBar {
        val attrs = intArrayOf(android.R.attr.selectableItemBackground)
        (toolbar as _Toolbar).apply {
            textView {
                if (text != null)
                    this.text = text
                if (textRes != null)
                    this.textResource = textRes
                this.id = id
                background = mView.ctx.obtainStyledAttributes(attrs).getDrawable(0)
                isClickable = true
                textSize = 18f
                verticalPadding = dip(16)
                horizontalPadding = dip(16)

                if (onClickListener != null) {
                    setOnClickListener(onClickListener)
                }
            }.lparams(Toolbar.LayoutParams(wrapContent, wrapContent)) {
                this.gravity = gravity
                if (gravity == Gravity.END) {
                    marginEnd = dip(16)
                }
            }
        }
        return mView.owner
    }


    constructor(context: Context?, hasBackground: Boolean, isIndexPage: Boolean) : super(context) {
        init()
    }


    constructor(context: Context?, hasBackground: Boolean, isIndexPage: Boolean, titleString: CharSequence) : super(context) {
        init()
    }


    constructor(context: Context?, hasBackground: Boolean, isIndexPage: Boolean, titleStringRes: Int) : super(context) {
        init()
    }

    constructor(context: Context?) : super(context) {
        init()
    }
}

@Suppress("NOTHING_TO_INLINE")

inline fun ViewManager.titleBar(init: TitleBar.() -> Unit = {}) =
        ankoView({ ctx -> TitleBar(ctx) }, 0, { init() })


inline fun ViewManager.titleBar(isIndexPage: Boolean, hasBackground: Boolean, init: TitleBar.() -> Unit = {}) =
        ankoView({ ctx -> TitleBar(ctx, hasBackground, isIndexPage) }, 0, { init() })


inline fun ViewManager.titleBar(isIndexPage: Boolean, titleString: CharSequence, hasBackground: Boolean, init: TitleBar.() -> Unit = {}) =
        ankoView({ ctx -> TitleBar(ctx, hasBackground, isIndexPage, titleString) }, 0, { init() })


inline fun ViewManager.titleBar(isIndexPage: Boolean, titleStringRes: Int, hasBackground: Boolean, init: TitleBar.() -> Unit = {}) =
        ankoView({ ctx -> TitleBar(ctx, hasBackground, isIndexPage, titleStringRes) }, 0, { init() })