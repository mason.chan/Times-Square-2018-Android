package com.timessquare.views

import android.graphics.Typeface
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.util.Log.i
import android.view.Gravity
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.generallistview.generalposter.GeneralPosterKey
import com.timessquare.utils.Constant
import com.timessquare.utils.LocaleHelper
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 8/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

inline fun ViewManager.textViewBaskerville(init: TextView.() -> Unit): TextView {
    return textView {
        //        if (MainActivity.get(context).mLanguage == context.getString(R.string.language_en))
        if (MainActivity[context].getLanguage() == LocaleHelper.englishLocale())
            typeface = Typeface.createFromAsset(context.assets, "baskerville.ttf")
        else
            typeface = Typeface.createFromAsset(context.assets, "pingfang.ttf")

        init()
    }
}

