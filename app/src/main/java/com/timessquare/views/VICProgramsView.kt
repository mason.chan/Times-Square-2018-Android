package com.timessquare.views


import android.content.Context
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.RecyclerView
import android.view.ViewManager
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.timessquare.R
import com.timessquare.vic.vicprograms.VICType
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.nestedScrollView

/**
 *   Created by Mason on 15/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class VICProgramsView : FrameLayout {

    lateinit var mView: AnkoContext<VICProgramsView>
    lateinit var layout: RelativeLayout
    lateinit var mNestedScrollView: NestedScrollView
    lateinit var mTitleBar: TitleBar
    lateinit var mRecyclerView: RecyclerView

    private fun init(vicType: Int? = null): AnkoContext<VICProgramsView> {
        mView = AnkoContext.createDelegate(this).apply {
            layout = relativeLayout {
                // Background
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(matchParent, wrapContent) {
                    alignParentBottom()
                }
                mNestedScrollView = this.nestedScrollView {
                    verticalLayout {
                        imageView {
                            when (vicType) {
                                VICType.VIC.value -> imageResource = R.drawable.ic_vic
                                VICType.BVIC.value -> imageResource = R.drawable.ic_bvic
                                VICType.GVIC.value -> imageResource = R.drawable.ic_gvic
                            }
                            id = R.id.VICProgramsImageView
                            adjustViewBounds = true
                        }.lparams(matchParent, wrapContent)
                        mRecyclerView = recyclerView {
                            id = R.id.vicProgramsRecyclerView
                        }.lparams(matchParent, wrapContent)
                    }.lparams(matchParent, wrapContent)
                }
                mTitleBar = titleBar { bkc.alpha = 0f }.addBackButton(true).addBackgroundColor(R.color.color_cover)
            }
        }
        return mView
    }

    constructor(context: Context?, vicType: Int) : super(context) {
        init(vicType = vicType)
    }
}

@Suppress("NOTHING_TO_INLINE")

inline fun ViewManager.vicProgramesView(vicType: Int, init: VICProgramsView.() -> Unit = {}) =
        ankoView({ ctx -> VICProgramsView(ctx, vicType) }, 0, { init() })