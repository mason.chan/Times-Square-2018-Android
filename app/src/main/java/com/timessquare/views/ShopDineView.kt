package com.timessquare.views

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.ViewManager
import android.widget.FrameLayout
import com.timessquare.R
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 *   Created by Mason on 19/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopDineView : FrameLayout {

    lateinit var mView: AnkoContext<ShopDineView>
    lateinit var layout: ConstraintLayout
    lateinit var mRecyclerView: RecyclerView
    lateinit var mTitleBar: TitleBar

    private fun init(): AnkoContext<ShopDineView> {
        mView = AnkoContext.createDelegate(this).apply {
            layout = constraintLayout {
                // Background
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(matchParent, wrapContent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                // TitleBar
                mTitleBar = titleBar {
                    id = R.id.ShopDineTitleBar
                }
                        .addBackgroundColor(R.color.color_cover)
                mRecyclerView = recyclerView {
                    id = R.id.ShopDineRecyclerView
                }.lparams(matchParent, matchConstraint) {
                    topToBottom = R.id.ShopDineTitleBar
                    bottomToBottom = PARENT_ID
                }
            }
        }
        return mView
    }

    constructor(context: Context?) : super(context) {
        init()
    }
}
@Suppress("NOTHING_TO_INLINE")

inline fun ViewManager.shopDineView( init: ShopDineView.() -> Unit = {}) =
        ankoView({ ctx -> ShopDineView(ctx) }, 0, { init() })