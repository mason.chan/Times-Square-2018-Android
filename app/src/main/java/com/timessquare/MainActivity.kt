package com.timessquare

import android.accounts.Account
import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.format.Time
import android.view.Gravity
import android.util.Log
import android.util.Log.i
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.timessquare.cover.CoverKey
import com.timessquare.index.IndexKey
import com.zhuinden.simplestack.BackstackDelegate
import com.zhuinden.simplestack.History
import com.zhuinden.simplestack.StateChange
import com.zhuinden.simplestack.StateChanger
import android.widget.TextView
import android.widget.Toast
import com.akexorcist.localizationactivity.core.LocalizationActivityDelegate
import com.akexorcist.localizationactivity.core.OnLocaleChangedListener
import com.crashlytics.android.Crashlytics
import com.timessquare.membercover.MemberCoverKey
import com.timessquare.apiservice.ApiService
import com.timessquare.apiservice.datamodel.*
import com.timessquare.apiservice.datas.*
import com.timessquare.database.AppDataBase
import com.timessquare.database.entity.EventEntity
import com.timessquare.database.entity.ShopEntity
import com.timessquare.menu.MenuItem
import com.timessquare.selectlanguage.SelectLanguageKey
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.orhanobut.dialogplus.*
import com.timessquare.database.entity.MemberEntity
import com.timessquare.database.entity.vicCard
import com.timessquare.member.MemberKey
import com.timessquare.utils.*
import io.fabric.sdk.android.Fabric
import io.reactivex.Observable.zip
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.support.v4.ctx
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.log



/**
 *   Created by Mason on 15/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MainActivity : AppCompatActivity(), StateChanger, OnLocaleChangedListener {
    // change language
    private val localizationDelegate = LocalizationActivityDelegate(this)

    // navigation fragment
    private lateinit var backstackDelegate: BackstackDelegate
    private lateinit var fragmentStateChanger: FragmentStateChanger
    // database
    lateinit var database: AppDataBase
    // api service
    lateinit var apiService: ApiService
    // menu adapter
    private var mFastItemAdapter: FastItemAdapter<MenuItem>? = null

    // Drop down menu
    lateinit var menu: DialogPlus

    // SharedPreferences
    lateinit var sp: SharedPreferenceUtils

    // account Manager
    lateinit var accountManager: AccountManager


    override fun onCreate(savedInstanceState: Bundle?) {

        backstackDelegate = BackstackDelegate(null)
        backstackDelegate.onCreate(savedInstanceState,
                lastCustomNonConfigurationInstance,
                History.single(CoverKey()))
        backstackDelegate.registerForLifecycleCallbacks(this)

        // change language
        localizationDelegate.addOnLocaleChangedListener(this)
        localizationDelegate.onCreate(savedInstanceState)
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        logUser()

        setContentView(R.layout.activity_main)

        fragmentStateChanger = FragmentStateChanger(supportFragmentManager, R.id.root)
        backstackDelegate.setStateChanger(this)

        /**
         *  init ApiService
         */
        apiService = ApiService.create()

        // init SharedPreferences
        sp = SharedPreferenceUtils.getInstance(this)
        // init AccountManager
        accountManager = AccountManager.get(getBaseContext())
        //init Database
        database = AppDataBase.getInstance(this)

        //init Menu
        createDropDownMenu()
        getCurrentDate()
    }

    override fun handleStateChange(stateChange: StateChange, completionCallback: StateChanger.Callback) {
        if (stateChange.topNewState<Any>() == stateChange.topPreviousState<Any>()) {
            completionCallback.stateChangeComplete()
            return
        }
        fragmentStateChanger.handleStateChange(stateChange)
        completionCallback.stateChangeComplete()
    }

    /**
     * init Data
     */
    fun dataLocalization() {
        Log.d("MainActivity", "dataLocalization")

        /**
         *  create Observable
         */
//        val getGoodiesObservable = apiService.getGoodies()
        val getEventObservable = apiService.getEvent()
        val getShopObservable = apiService.getShop()

        /**
         * combined Observable
         */
        val combinedObservable = zip(
                getShopObservable, getShopObservable, getEventObservable,
                Function3<List<ShopModel>, List<ShopModel>, List<EventModel>, PreloadData> { s, s2, s3 ->
                    PreloadData(s, s2, s3)
                })

        /**
         * create Observer
         */
        val combinedObserver = object : Observer<PreloadData> {
            override fun onComplete() {
                i("combinedObserver", "onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                i("combinedObserver", "onSubscribe" + d)
            }

            override fun onNext(t: PreloadData) {
                i("combinedObserver", "onNext")
                initLocalData(t)
                /**
                 * language Select
                 */
                if (sp.getStringValue(SParem.Language_Selected, null) == null) {
                    replaceHistory(SelectLanguageKey())
                } else {
                    replaceHistory(IndexKey())
                }
            }

            override fun onError(e: Throwable) {
                i("combinedObserver", "onError  " + e)
            }
        }

        /**
         *  subscribe
         */
        combinedObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(combinedObserver)

    }

    fun removeEcard(codeId: Int): Observable<RemoveEcardModel> {
        var accessToken = ""
        accessToken = database.memberDao().getFirst().accessToken!!
        i("removeEcard", accessToken + " " + codeId)
        val messageObservable = apiService.removeEcard(accessToken, codeId)

        return messageObservable
    }

    fun addEcard(cardCode: String): Observable<AddEcardModel> {
        var accessToken = ""
        if (database.memberDao().getFirst() !== null) {
            accessToken = database.memberDao().getFirst().accessToken ?: ""
        }
        i("addEcard", "accessToken  " + accessToken + "   " + cardCode)

        val messageObservable = apiService.addEcard(accessToken, cardCode)

        return messageObservable
    }

    fun login(): Observable<MemberLoginModel> {
        var email = ""
        email = database.memberDao().getFirst().email!!
        var password = sp.getStringValue(SParem.Member_Password, "")
        return login(email, password)
    }

    fun login(ecard_email: String, ecard_password: String): Observable<MemberLoginModel> {

        i("login", ecard_email + " " + ecard_password)

        var memberLoginObservable = apiService.attemptMemberLogin(ecard_email, ecard_password)

        return memberLoginObservable
    }

    fun getAccount(): Account {
        return accountManager.getAccountsByType(Constant.ACCOUNT_TYPE)[0]
    }

    /**
     * Create drop down menu
     */
    fun createDropDownMenu() {
        var holder = ViewHolder(R.layout.menucontent_view)
        var view = layoutInflater.inflate(R.layout.menucontent_view, null)
        mFastItemAdapter = FastItemAdapter()
        mFastItemAdapter!!.withOnClickListener { v, adapter, item, position ->
            navigateTo(item.mKey!!)
            false
        }



        menu = DialogPlus.newDialog(this).apply {
            setContentHolder(holder)
            setHeader(R.layout.menuheader_view)
            setCancelable(true)

            setGravity(Gravity.TOP)
            setOnClickListener { dialog, view ->
                //                    toast(view.tag.toString())
                when (view.tag) {
                    getString(R.string.tag_cancel) -> dialog.dismiss()
                    getString(R.string.tag_member) -> toast("go to member")
                }
            }
            setOnItemClickListener { dialog, item, view, position ->
                val textView = view.findViewById<TextView>(R.id.text_view)
                toast("setOnItemClickListener   :" + textView.text.toString())
            }
            setExpanded(false)
            setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
            setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
        }.create()
    }

    /**
     * Go to AnyWhere
     */
    fun navigateTo(key: BaseKey) {
        hideKeyboard()
        backstackDelegate.backstack.goTo(key)
    }


    /**
     * Go to Index Page
     */
    fun replaceHistory(rootKey: BaseKey) {
        hideKeyboard()
        backstackDelegate.backstack.setHistory(History.single(rootKey), StateChange.REPLACE)
    }

    fun replaceHistoryWithIndex(rootKey: BaseKey) {
        hideKeyboard()
        var list = listOf(IndexKey(), rootKey)
        backstackDelegate.backstack.setHistory(list, StateChange.REPLACE)
    }


    /**
     * Change Font Family
     */
    fun changeFontFamily(locale: Locale) {
        if (locale == LocaleHelper.hkLocale()) {
            this.setTheme(R.style.AppThemeHK)
        } else {
            this.setTheme(R.style.AppTheme)
        }
    }


    /**
     * Preload Data to Local
     */
    fun initLocalData(preloadData: PreloadData) {
        // insert to database
        insertAllShop(preloadData.shops)
        insertAllEvent(preloadData.events)
    }

    /**
     * Event to EventEntity
     */
    fun insertAllEvent(events: List<EventModel>) {
        i("events", "events" + events.size)
        /**
         * Convert EventModel to EventEntity for Database
         */
        var eventData = ArrayList<EventEntity>()
        events.forEach {
            eventData.add(
                    EventEntity(
                            id = it.id,
                            sortId = it.sortId ?: 1,
                            classId = it.classId ?: 1,
                            title = it.title ?: "",
                            titleZh = it.titleLocalized?.zh ?: it.title!!,
                            titleZhsc = it.titleLocalized?.zhsc ?: it.title!!,
                            thumb = it.thumb,
                            poster = it.poster ?: ArrayList(),
                            posterZh = it.posterLocalized?.zh ?: it.poster ?: ArrayList(),
                            terms = it.terms,
                            termsZh = it.termsLocalized?.zh ?: it.terms,
                            termsZhsc = it.termsLocalized?.zhsc ?: it.terms,
                            startDate = it.startDate,
                            endDate = it.endDate,
                            valid = if (it.startDate <= Date() && it.endDate >= Date()) true else false
                    ))
        }
        /**
         * Clean And Insert in Database
         */
        database.eventDao().deleteAll()
        database.eventDao().insertAll(eventData)
    }

    /**
     * Shop to ShopEntity
     */
    fun insertAllShop(shops: List<ShopModel>) {
        /**
         * Convert Shop
         */
        var shopData = ArrayList<ShopEntity>()

        shops.forEach {
            shopData.add(
                    ShopEntity(
                            id = it.Id,
                            number = it.number,
                            isShopNotFood = (it.classId == 1),
                            name = it.name,
                            nameZh = it.nameLocalized?.nameZh ?: it.name,
                            floorId = it.floorId,
                            categoryId = it.categoryId,
                            thumbImage = it.thumbImage,
                            description = it.description,
                            descriptionZh = it.descriptionLocalized?.descriptionZh ?: it.description,
                            openHours = it.openHours,
                            contact = it.phone,
                            detailImage = it.detailImage,
                            // fill data
//                            menu = arrayListOf("http://www.primantibros.com/resources/images/pdf/menu-pgh-city-strip.png","https://jacobjer.files.wordpress.com/2014/11/menu21.gif"),
                            menu = it.menu,
                            vicPrivilege = it.attribute?.vicPrivilege,
                            bvicPrivilege = it.attribute?.bvicPrivilege,
                            gvicPrivilege = it.attribute?.gvicPrivilege,
                            vicBirthdayOffer = it.attribute?.birthdayOffer,
                            vicYearRoundOffer = it.attribute?.yearRoundOffer,
                            officeClub = it.attribute?.officeClub,
                            clubOn = it.attribute?.clubOn,
                            vicYearRoundOfferDescription = it.extraDesctiption?.extraDesctiption?.yearRoundOffer,
                            vicYearRoundOfferDescriptionZh = it.extraDesctiption?.extraDesctiptionZh?.yearRoundOffer ?: it.extraDesctiption?.extraDesctiption?.yearRoundOffer,
                            vicBirthdayOfferDesctiption = it.extraDesctiption?.extraDesctiption?.birrthdayOffer,
                            vicBirthdayOfferDesctiptionZh = it.extraDesctiption?.extraDesctiptionZh?.birrthdayOffer ?: it.extraDesctiption?.extraDesctiption?.birrthdayOffer

                    )
            )
        }

        database.shopDao().deleteAll()
        database.shopDao().insertAll(shopData)
    }

    /**
     * update
     */
    fun updateMemberData(memberLoginModel: MemberLoginModel) {
        database.memberDao().deleteAll()

        var cardArray = ArrayList<vicCard>()
        memberLoginModel.vic_card.forEach {
            cardArray.add(vicCard(
                    cardPin = it.card_pin,
                    cardCode = it.card_code,
                    cardId = it.card_id,
                    cardTypeId = it.card_type_id,
                    cardStatus = it.card_status,
                    firstName = it.member_first_name,
                    lastName = it.member_last_name,
                    email = it.member_email,
                    tel = it.member_tel,
                    point = it.member_point,
                    expiryDate = it.member_point_expiry_date
            ))
        }
        database.memberDao().insert(MemberEntity(
                id = memberLoginModel.ecard_id,
                email = memberLoginModel.ecard_email,
                name = memberLoginModel.ecard_name,
                refreshToken = memberLoginModel.ecard_refresh_token,
                card = cardArray,
                accessToken = memberLoginModel.access_token,
                message = memberLoginModel.message,
                errorMessage = memberLoginModel.error_message,
                errorCode = memberLoginModel.error_code
        ))
    }

    /**
     * Request go to Member Page
     */
    fun requestToGoToMemberPage() {
        Log.i("requestToGoToMemberPage", sp.getBoolanValue(SParem.Member_Logined, false).toString())
        if (sp.getBoolanValue(SParem.Member_Logined, false)) {
            replaceHistoryWithIndex(MemberKey())
        } else {
            replaceHistoryWithIndex(MemberCoverKey())
        }
    }

    /**
     *  Share activity through context
     */
    override fun getSystemService(name: String): Any? = when {
        name == TAG -> this
        else -> super.getSystemService(name)
    }

    companion object {
        private val TAG = "MainActivity"

        @SuppressLint("WrongConstant")
        operator fun get(context: Context): MainActivity {
            return context.getSystemService(TAG) as MainActivity
        }
    }

    private fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier("userId")
        Crashlytics.setUserEmail("userEmail")
        Crashlytics.setUserName("userName")
    }


    override fun onBackPressed() {

        hideKeyboard()
        if (!backstackDelegate.onBackPressed()) {
            super.onBackPressed()
        }


    }

    /**
     * hidding keyboard
     */
    fun hideKeyboard() {
        try {
            var imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(this.currentFocus.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {

        }
    }

    /**
     * Get Current Date
     */
    fun getCurrentDate() {
        val today = Time(Time.getCurrentTimezone())
        today.setToNow()
        i("getCurrentDate", "getCurrentDate " + today.year + today.format("%k:%M:%S"))
        i("getCurrentDate", "getCurrentDate   " + Calendar.getInstance().getTime())
    }

    override fun onResume() {
        super.onResume()
        localizationDelegate.onResume(this)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(newBase))
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }

    override fun getResources(): Resources {
        return localizationDelegate.getResources(super.getResources())
    }

    fun setLanguage(language: String) {
        localizationDelegate.setLanguage(this, language)
    }

    fun setLanguage(locale: Locale) {
        localizationDelegate.setLanguage(this, locale)
        //update language in sharedpregerence
        sp.setValue(SParem.Language_Selected, LocaleHelper.setSharedPreference(locale))
        changeFontFamily(locale)
    }

    fun getLanguage(): Locale {
        return localizationDelegate.getLanguage(this)
    }

    fun isEnglish(): Boolean {
        return (localizationDelegate.getLanguage(this) == LocaleHelper.englishLocale())
    }

    fun setDefaultLanguage(language: String) {
        localizationDelegate.setDefaultLanguage(language)
    }

    fun setDefaultLanguage(locale: Locale) {
        localizationDelegate.setDefaultLanguage(locale)
    }

    fun getCurrentLanguage(): Locale {
        return localizationDelegate.getLanguage(this)
    }

    // For change language
    override fun onAfterLocaleChanged() {
    }

    override fun onBeforeLocaleChanged() {
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        return backstackDelegate.onRetainCustomNonConfigurationInstance();
    }

    fun AlertDialog(title: String, message: String, goBack: Boolean): IOSDialog {
        return IOSDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(R.string.member_register_register_failed_dialog_ok, R.color.color_text_red,
                        DialogInterface.OnClickListener { dialog, which ->
                            if(goBack) onBackPressed()
                        })
                .create()
    }
    fun AlertDialog(title: Int, message: Int, goBack: Boolean): IOSDialog {
        return AlertDialog(getString(title), getString(message), goBack)
    }
}