package com.timessquare.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class SettingsFragment :BaseFragment() {

    lateinit var mView : SettingsUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = SettingsUI()
        return mView.createView(AnkoContext.create(activity!!, this))

    }

}