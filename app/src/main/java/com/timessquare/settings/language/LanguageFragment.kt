package com.timessquare.settings.language

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import org.intellij.lang.annotations.Language
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class LanguageFragment: BaseFragment() {

    lateinit var mView : LanguageUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = LanguageUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }
}