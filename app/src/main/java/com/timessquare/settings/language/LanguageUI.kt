package com.timessquare.settings.language

import android.graphics.Typeface
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.utils.LocaleHelper
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class LanguageUI : AnkoComponent<LanguageFragment> {

    override fun createView(ui: AnkoContext<LanguageFragment>): View {
        return with(ui) {
            constraintLayout() {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    titleBar {}
                            .addTitle(R.string.language_title)
                            .addBackButton()
                            .addBackgroundColor(R.color.color_cover)
                    include<CardView>(R.layout.settings_item) {
                        onClick {
                            //                        MainActivity.get(view.context).updateLanguage(resources.getString(R.string.language_hk))
                            MainActivity.get(view.context).setLanguage(LocaleHelper.hkLocale())
                            MainActivity.get(view.context).onBackPressed()
                        }
                        var to_image_view = find<ImageView>(R.id.to_image_view)
                        to_image_view.visibility = View.INVISIBLE
                        var textview = find<TextView>(R.id.text_view)
                        textview.textResource = R.string.language_button_zh_hk
                        if (!MainActivity.get(ctx).isEnglish()) {
                            textview.setTypeface(null, Typeface.BOLD)
                        } else {
                            textview.textColorResource = R.color.color_text_gray
                        }
                    }
                    include<CardView>(R.layout.settings_item) {
                        onClick {
                            //                        MainActivity.get(view.context).updateLanguage(resources.getString(R.string.language_en))
                            MainActivity.get(view.context).setLanguage(LocaleHelper.englishLocale())
                            MainActivity.get(view.context).onBackPressed()
                        }
                        var to_image_view = find<ImageView>(R.id.to_image_view)
                        to_image_view.visibility = View.INVISIBLE
                        var textview = find<TextView>(R.id.text_view)
                        textview.textResource = R.string.language_button_en
                        if (MainActivity.get(ctx).isEnglish()) {
                            textview.setTypeface(null, Typeface.BOLD)
                        } else {
                            textview.textColorResource = R.color.color_text_gray
                        }
                    }
                }
            }
        }
    }
}