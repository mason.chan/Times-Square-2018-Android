package com.timessquare.settings

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.view.View
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.settings.language.LanguageKey
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 6/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class SettingsUI : AnkoComponent<SettingsFragment> {

    override fun createView(ui: AnkoContext<SettingsFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    titleBar {}
                            .addTitle(R.string.settings_title)
                            .addBackgroundColor(R.color.color_cover)
                            .addBackButton()
                    include<CardView>(R.layout.settings_item) {
                        onClick { MainActivity.get(view.context).navigateTo(LanguageKey()) }
                        var textview = find<TextView>(R.id.text_view)
                        textview.textResource = R.string.settings_change_language
                    }
                }.lparams(matchParent, wrapContent)
            }
        }
    }
}
