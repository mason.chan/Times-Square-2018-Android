package com.timessquare.goodies

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.R
import com.mikepenz.fastadapter.items.AbstractItem

class GoodiesItem : AbstractItem<GoodiesItem, GoodiesItem.ViewHolder>() {

    lateinit var mName: String
    lateinit var mImageUrl: String
    var mPosterUrl: String? = null

    fun withImage(imageUrl: String): GoodiesItem {
        this.mImageUrl = imageUrl
        return this
    }

    fun withName(name: String): GoodiesItem {
        this.mName = name
        return this
    }

    fun withPosterUrl(posterUrl: String): GoodiesItem {
        this.mPosterUrl = posterUrl
        return this
    }


    override fun getType(): Int {
        return R.id.index_item_id
    }

    override fun getViewHolder(v: View): ViewHolder{
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
return R.layout.goodies_item_view
    }


    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        val ctx = holder.itemView.context

        //define our data for the view
        holder.textView!!.text = mName


        Glide.with(ctx).load(mImageUrl).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).into(holder.imageView!!)
    }

    /**
     * GoodiesItem ViewHolder
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {

        lateinit var textView: TextView
        lateinit var imageView: ImageView

        init {
//            ButterKnife.bind(this, view)
            textView = view.findViewById(R.id.text_view)
            imageView = view.findViewById(R.id.image_view)
        }

    }
}
