package com.timessquare.goodies

import android.util.Log.i
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.database.dao.EventType
import com.timessquare.database.entity.GoodiesEntity
import com.timessquare.dummy.IndexDummyData
import com.timessquare.generallistview.GeneralListViewFragment
import com.timessquare.generallistview.GeneralListViewItem
import org.jetbrains.anko.support.v4.ctx
import java.util.*

/**
 *   Created by Mason on 24/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class GoodiesFragment : GeneralListViewFragment() {

    override fun addData(): List<GeneralListViewItem> {
//        var rawData = IndexDummyData.goodiesItems
        var rawData = MainActivity[ctx].database.eventDao().getEventType(EventType.goodies.value, Date(), Date())

        i("addData", "addData   " + rawData.size)
        var result = ArrayList<GeneralListViewItem>()

        rawData.forEach {
            if(MainActivity[ctx].isEnglish()) {
                result.add(GeneralListViewItem(it.title, it.thumb
                        ?: "", if (it.poster!!.size > 0) it.poster!![0] else ""))
            } else {
                result.add(GeneralListViewItem(it.titleZh, it.thumb
                        ?: "", if (it.posterZh!!.size > 0) it.posterZh!![0] else ""))
            }
        }
        return result
    }

    override fun setTitle(): Int {
        return R.string.goodies_title
    }
}