package com.timessquare.goodies

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 24/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class GoodiesKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(GoodiesKey))

    override fun createFragment(): BaseFragment = GoodiesFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}