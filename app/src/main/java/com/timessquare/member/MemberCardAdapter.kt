package com.timessquare.member

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.membercard.MemberCardKey
import com.timessquare.utils.Constant
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberCardAdapter(var ctx: Context) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return 3
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val constraintLayout = LayoutInflater.from(container.context).inflate(R.layout.member_card_view, null) as ConstraintLayout
        var imageView = constraintLayout.findViewById<ImageView>(R.id.image_view)
        when (position) {
            0 -> imageView.setImageResource(R.drawable.ic_vic)

            1 -> imageView.setImageResource(R.drawable.ic_bvic)

            2 -> imageView.setImageResource(R.drawable.ic_gvic)
        }
        constraintLayout.onClick {
            MainActivity[ctx].navigateTo(MemberCardKey().withArgs {
                when(position) {
                    0-> putInt(Constant.TAG_INTENT, position +1)
                    1-> putInt(Constant.TAG_INTENT, position +2)
                    2-> putInt(Constant.TAG_INTENT, position )
                }

            })
        }

        container.addView(constraintLayout)
        return constraintLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        var view = `object` as ConstraintLayout
        container.removeView(view)
    }
}