package com.timessquare.member

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.view.View
import android.widget.TextView
import com.timessquare.BuildConfig
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.membercover.MemberCoverFragment
import com.timessquare.utils.Constant
import com.timessquare.views.titleBar
import com.tmall.ultraviewpager.UltraViewPager
import kotlinx.android.synthetic.main.settings_item.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.support.v4.viewPager

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberUI() : AnkoComponent<MemberFragment> {
    lateinit var ultraViewPager: UltraViewPager
    lateinit var accountSettings: CardView
    lateinit var logout: CardView
    lateinit var tokenTextView: TextView
    override fun createView(ui: AnkoContext<MemberFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    titleBar {
                    }.addBackgroundColor(R.color.color_cover).addTitle(R.string.member_title).addMenuButton()
                    ultraViewPager = customView<UltraViewPager> {
                    }.lparams(matchParent, wrapContent) {
                        verticalMargin = dimen(R.dimen.margin_normal)
                    }
                    accountSettings = include<CardView>(R.layout.settings_item) {
                        var text_view = find<TextView>(R.id.text_view)
                        text_view.textResource = R.string.member_account_settings
                    }
                    logout = include<CardView>(R.layout.settings_item) {
                        var text_view = find<TextView>(R.id.text_view)
                        text_view.textResource = R.string.member_Logout
                        text_view.textColorResource = R.color.colorAccent
                    }
                }
            }
        }
    }
}