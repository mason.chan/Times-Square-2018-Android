package com.timessquare.member

import android.support.v4.view.ViewPager
import android.view.View

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberScaleTransformer : ViewPager.PageTransformer {

    private val MIN_SCALE = 0.75f

    override fun transformPage(page: View, position: Float) {
        val pageWidth = page.width
        val scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position))

        if (position < 0) { // [-1,0]
            // Scale the page down (between MIN_SCALE and 1)
            page.scaleX = scaleFactor
            page.scaleY = scaleFactor

        } else if (position == 0f) {
            page.scaleX = 1f
            page.scaleY = 1f

        } else if (position <= 1) { // (0,1]
            // Scale the page down (between MIN_SCALE and 1)
            page.scaleX = scaleFactor
            page.scaleY = scaleFactor

        }
    }
}