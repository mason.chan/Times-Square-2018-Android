package com.timessquare.member

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MemberKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberKey))

    override fun createFragment(): BaseFragment = MemberFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}