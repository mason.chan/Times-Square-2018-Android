package com.timessquare.member

import android.accounts.AccountManagerCallback
import android.accounts.AccountManagerFuture
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.i
import com.timessquare.BaseFragment
import com.timessquare.BuildConfig
import com.timessquare.MainActivity
import com.timessquare.index.IndexKey
import com.timessquare.memberaccountsettings.MemberAccountSettingsKey
import com.timessquare.utils.Constant
import com.timessquare.utils.SParem
import com.tmall.ultraviewpager.UltraViewPager
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberFragment : BaseFragment() {

    lateinit var mView: MemberUI
    lateinit var gravity_indicator: UltraViewPager.Orientation
    lateinit var adapter: MemberCardAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberUI()
        i("MemberFragment", "onCreateView ")
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager(view)
//        MainActivity[view.context].accountManager.getAuthToken(
//                MainActivity[view.context].accountManager.getAccountsByType(Constant.ACCOUNT_TYPE)[0],
//                Constant.API_ACCESS_TOKEN, null, activity, object : AccountManagerCallback<Bundle>{
//            override fun run(future: AccountManagerFuture<Bundle>?) {
//                Log.i("MemberFragment", "getAccountsByType")
//            }
//        }, null
//        )
        mView.accountSettings.onClick {
            MainActivity[view.context].navigateTo(MemberAccountSettingsKey())
        }
        mView.logout.onClick {
            // Change Shared Preference Logined false
            MainActivity[ctx].sp.setValue(SParem.Member_Logined, false)
            MainActivity[view.context].replaceHistory(IndexKey())
        }
//        i("MemberFragment", "member name: " + MainActivity[ctx].database.memberDao().getFirst().name)
    }

    fun initViewPager(view: View) {
        mView.ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL)
        adapter = MemberCardAdapter(view.context)
        mView.ultraViewPager.setAdapter(adapter)
        mView.ultraViewPager.setMultiScreen(0.8f)
        mView.ultraViewPager.setItemRatio(1.0)
        mView.ultraViewPager.setRatio(2.0f)
        mView.ultraViewPager.setAutoMeasureHeight(true)
        gravity_indicator = UltraViewPager.Orientation.HORIZONTAL
        //Loop
        mView.ultraViewPager.setInfiniteLoop(true)
        // Zoom
        mView.ultraViewPager.setPageTransformer(false, MemberScaleTransformer())
    }
}