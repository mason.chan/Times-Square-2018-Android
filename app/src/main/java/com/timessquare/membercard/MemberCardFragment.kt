package com.timessquare.membercard

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.pawegio.kandroid.i
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.database.entity.vicCard
import com.timessquare.memberscancard.MemberScanCardKey
import com.timessquare.utils.Constant
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.find
import java.io.File
import java.io.FileOutputStream
import java.util.*
import android.os.CountDownTimer
import android.widget.TextView
import com.pawegio.kandroid.runOnUiThread
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.find


/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberCardFragment : BaseFragment(), BaseSliderView.OnSliderClickListener {

    val vicType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: -1 }
    lateinit var mView: MemberCardUI
    var card = vicCard("", "1", -1, -1, -1, "", "", "", "", 0, Date())
    var havecard = false
    var multiFormatWriter = MultiFormatWriter()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberCardUI(vicType)
            updateCurrentCard()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            MainActivity[ctx].login()
                    .subscribeOn(Schedulers.io())
                    .doFinally { }
                    .subscribeBy(
                            onNext = {
                                MainActivity[ctx].updateMemberData(it)
                            },
                            onError = {
                            },
                            onComplete = {
                                    updateCurrentCard()
                                runOnUiThread {
                                    mView.memberPointRelativeLayout.find<TextView>(R.id.value).text = card.point.toString()
                                }
                            }
                    )
        updateUI(havecard)
        addCardView()
        initSliderLayout()
        startCountdown()
        mView.addCardCardView.onClick {
            MainActivity[view.context].navigateTo(MemberScanCardKey().withArgs {
                putInt(Constant.TAG_INTENT, vicType)
            })
        }
    }

    private fun startCountdown() {
        object : CountDownTimer(21000, 1000) {

            override fun onFinish() {
                mView.countDownTextView.setText("0.00")
            }

            override fun onTick(millisUntilFinished: Long) {
                if(millisUntilFinished / 1000 >= 10) {
                    mView.countDownTextView.setText("0." + millisUntilFinished / 1000)
                } else {
                    mView.countDownTextView.setText("0.0" + millisUntilFinished / 1000)
                }
            }

        }.start()
    }

    fun updateCurrentCard() {
        MainActivity[ctx].database.memberDao().getFirst().card!!.forEach {
            if (it.cardTypeId == vicType) {
                havecard = true
                card = it
            }
        }
        mView.card = card
    }
    fun addCardView() {
        var mcsv: BaseSliderView
        var mcsv2: BaseSliderView

        val sliderImage: Int
        val backgroundImage: Int

        //1=vic, 2=tourist vic, 3=beauty vic
        when (vicType) {
//            1 -> {
//                sliderImage = R.drawable.ic_vic
//                backgroundImage = R.drawable.ic_vic_back
////                mcsv = MemberCardSliderView(activity!!).image(R.drawable.ic_vic).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this)
////                mcsv2 = cardBackgroundGenerator(R.drawable.ic_vic_back, card.cardCode)
//            }
            2 -> {
                sliderImage = R.drawable.ic_gvic
                backgroundImage = R.drawable.ic_gvic_back
//                mcsv = MemberCardSliderView(activity!!).image(R.drawable.ic_gvic).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this)
//                mcsv2 = cardBackgroundGenerator(R.drawable.ic_gvic_back, card.cardCode)
            }
            3 -> {
                sliderImage = R.drawable.ic_bvic
                backgroundImage = R.drawable.ic_bvic_back
//                mcsv = MemberCardSliderView(activity!!).image(R.drawable.ic_bvic).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this)
//                mcsv2 = cardBackgroundGenerator(R.drawable.ic_bvic_back, card.cardCode)
            }

            else -> {
                sliderImage = R.drawable.ic_vic
                backgroundImage = R.drawable.ic_vic_back
//                mcsv = MemberCardSliderView(activity!!).image(R.drawable.ic_vic).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this)
//                mcsv2 = cardBackgroundGenerator(R.drawable.ic_vic_back, card.cardCode)
            }
        }

        mcsv = MemberCardSliderView(activity!!).image(sliderImage).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this)
        mcsv2 = cardBackgroundGenerator(backgroundImage, card.cardCode)
        i("Member", "mSliderLayout  ")
        mView.mSliderLayout.addSlider(mcsv)
        mView.mSliderLayout.addSlider(mcsv2)
    }

    fun initSliderLayout() {
        mView.mSliderLayout.run {
            setPresetTransformer(SliderLayout.Transformer.FlipHorizontal)
            setCustomAnimation(DescriptionAnimation())
            setDuration(500)
            stopAutoCycle()
        }
//        mView.mSliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal)
//        mView.mSliderLayout.setCustomAnimation(DescriptionAnimation())
//        mView.mSliderLayout.setDuration(500)
//        mView.mSliderLayout.stopAutoCycle()
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        mView.mSliderLayout.moveNextPosition()
    }

    fun cardBackgroundGenerator(backgroundInt: Int, number: String): BaseSliderView {


        // Background
        var backgroundBitmap = BitmapFactory.decodeResource(getResources(), backgroundInt)
//        var backgroundBitmap = decodeBitmap(context!!, backgroundInt)
        // QR code
        var bitMatrix: BitMatrix = multiFormatWriter.encode(number, BarcodeFormat.QR_CODE, (backgroundBitmap!!.height * 0.65).toInt(), (backgroundBitmap.height * 0.65).toInt())
        var barcodeEncoder = BarcodeEncoder()
        var bitmap = barcodeEncoder.createBitmap(bitMatrix)
        // Merge background and QRcode

        var bitCard = Bitmap.createBitmap(backgroundBitmap.getWidth(), backgroundBitmap.getHeight(), backgroundBitmap.getConfig())
        val canvas = Canvas(bitCard)
        canvas.drawBitmap(backgroundBitmap, Matrix(), null)
        canvas.drawBitmap(bitmap, backgroundBitmap.width / 2 - bitmap.width / 2f, backgroundBitmap.height / 2 - bitmap.height / 2f, null)
        // Save to local
        val file = File(context!!.filesDir, number + ".jpg")
        var out = FileOutputStream(file)
        bitCard.compress(Bitmap.CompressFormat.PNG, 100, out) // bmp is your Bitmap instance

        i("Member", "filesDir   " + file.path)
        return MemberCardSliderView(activity!!).image(file).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this)
    }

    fun updateUI(havecard: Boolean) {
        if (havecard) {
            find<ImageView>(R.id.member_card_setting).visibility = View.VISIBLE
            mView.normallyLayout.visibility = View.VISIBLE
            mView.addCardLayout.visibility = View.INVISIBLE
        } else {
            find<ImageView>(R.id.member_card_setting).visibility = View.INVISIBLE
            mView.normallyLayout.visibility = View.INVISIBLE
            mView.addCardLayout.visibility = View.VISIBLE
        }
    }
//    fun decodeBitmap(context: Context, resId: Int): Bitmap? {
//        val opt = BitmapFactory.Options()
//        opt.inPreferredConfig = Bitmap.Config.RGB_565
//        opt.inPurgeable = true
//        opt.inInputShareable = true
//        //获取资源图片
//        val `is` = context.getResources().openRawResource(resId)
//        return BitmapFactory.decodeStream(`is`, null, opt)
//    }

}