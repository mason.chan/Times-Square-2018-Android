package com.timessquare.membercard

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.v7.widget.CardView
import android.text.InputType
import android.text.util.Linkify
import android.view.Gravity
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.R.id.all
import com.timessquare.database.entity.vicCard
import com.timessquare.membercardsetting.MemberCardSettingKey
import com.timessquare.utils.Constant
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.menu_item.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.custom.customView
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberCardUI(var vicType: Int) : AnkoComponent<MemberCardFragment> {

    lateinit var card: vicCard
    lateinit var mSliderLayout: SliderLayout
    lateinit var mTitlebar: TitleBar
    lateinit var addCardLayout: ConstraintLayout
    lateinit var normallyLayout: ConstraintLayout
    lateinit var addCardCardView: CardView
    lateinit var countDownTextView: TextView
    lateinit var memberPointRelativeLayout: RelativeLayout

    override fun createView(ui: AnkoContext<MemberCardFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = PARENT_ID
                }
                mTitlebar = titleBar {
                    id = R.id.member_card_titlebar
                }.addBackgroundColor(R.color.color_cover)
                        .addTitle(R.string.member_card_title)
                        .addBackButton()
                        .addButton(imgRes = R.drawable.ic_setting, gravity = Gravity.END, marginRes = R.dimen.margin_normal, buttonId = R.id.member_card_setting,
                                onClickListener = View.OnClickListener { MainActivity[ctx].navigateTo(MemberCardSettingKey().withArgs {
                                    putInt(Constant.TAG_INTENT, vicType)
                                }) })
                scrollView {
                    isFillViewport = true
                    frameLayout {
                        // no card
                        addCardLayout = constraintLayout {
                            addCardCardView = cardView {
                                radius = 9f
                                constraintLayout {
                                    backgroundColorResource = R.color.color_white
                                    imageView {
                                        id = R.id.member_card_add_card_image_view
                                        imageResource = R.drawable.ic_add_gray
                                    }.lparams(dimen(R.dimen.manu_item_size), dimen(R.dimen.manu_item_size)) {
                                        topToTop = PARENT_ID
                                        bottomToBottom = PARENT_ID
                                        leftToLeft = PARENT_ID
                                        rightToRight = PARENT_ID
                                    }
                                    textView {
                                        textResource = R.string.member_card_add_vic_card
                                        textSizeDimen = R.dimen.text_normal
                                    }.lparams(){
                                        topToBottom = R.id.member_card_add_card_image_view
                                        leftToLeft = PARENT_ID
                                        rightToRight = PARENT_ID
                                        topMargin = dimen(R.dimen.margin_normal)
                                    }
                                }
                            }.lparams(matchParent, 0) {
                                dimensionRatio = "H,1.8:1"
                                margin = dimen(R.dimen.margin_normal)
                            }
                        }.lparams(matchParent, matchParent){
                            topMargin = dimen(R.dimen.margin_normal)
                        }
                        // normally
                        normallyLayout = constraintLayout {
                            mSliderLayout = customView<SliderLayout> {
                                id = R.id.member_card_slider_layout
                                indicatorVisibility = PagerIndicator.IndicatorVisibility.Invisible
                            }.lparams(width = matchParent, height = 0) {
                                dimensionRatio = "H,1.8:1"
                                topToTop = PARENT_ID
                            }
                            cardView {
                                id = R.id.cardView
                                verticalLayout {
                                    backgroundColorResource = R.color.color_white
                                    padding = dimen(R.dimen.padding_small)

//                                    include<RelativeLayout>(R.layout.member_card_info) {
//                                        var name = find<TextView>(R.id.name)
//                                        var value = find<TextView>(R.id.value)
//                                        name.textResource = R.string.member_card_member_points
//                                        name.textColorResource = R.color.color_black
//                                        name.textSizeDimen = R.dimen.text_subject
//                                        value.text = card.point.toString()
//                                        value.textSize = 40f
//                                    }
                                    memberPointRelativeLayout = include<RelativeLayout>(R.layout.member_card_info) {
                                        find<TextView>(R.id.name).textResource = R.string.member_card_member_points
                                        find<TextView>(R.id.value).text = card.point.toString()
                                    }
                                    include<RelativeLayout>(R.layout.member_card_info) {
                                        find<TextView>(R.id.name).textResource = R.string.member_card_expiry_date
                                        find<TextView>(R.id.value).text = SimpleDateFormat("yyyy-MM-dd").format(card.expiryDate)
//                                        find<TextView>(R.id.value).text = card.expiryDate.year.toString() + "-" + card.expiryDate.month.toString() + "-" + card.expiryDate.day
                                    }
                                    include<RelativeLayout>(R.layout.member_card_info) {
                                        find<TextView>(R.id.name).textResource = R.string.member_card_member_number
                                        find<TextView>(R.id.value).text = card.cardPin.toString()
                                    }
                                    include<RelativeLayout>(R.layout.member_card_info) {
                                        find<TextView>(R.id.name).textResource = R.string.member_card_name
                                        find<TextView>(R.id.value).text = if(MainActivity[ctx].isEnglish()) card.lastName + " " + card.firstName  else card.lastName + " " + card.firstName
                                    }
                                    include<RelativeLayout>(R.layout.member_card_info) {
                                        find<TextView>(R.id.name).textResource = R.string.member_card_email
                                        find<TextView>(R.id.value).text = card.email
                                        find<TextView>(R.id.value).textSizeDimen = R.dimen.text_small
                                    }
//                                    include<RelativeLayout>(R.layout.member_card_info) {
//                                        find<TextView>(R.id.name).textResource = R.string.member_card_phone
//                                        find<TextView>(R.id.value).autoLinkMask = Linkify.ALL
//                                        find<TextView>(R.id.value).inputType = InputType.TYPE_CLASS_PHONE
//                                        find<TextView>(R.id.value).text = card.tel
//                                    }
                                }
                            }.lparams(matchParent, wrapContent) {
                                margin = dimen(R.dimen.margin_normal)
                                topToBottom = R.id.member_card_slider_layout
                            }
                            textView {
                                id = R.id.qrcode_valid_time_textview
                                textResource = R.string.member_card_qrcode_valid_time
                                textSizeDimen = R.dimen.text_menu_size
                                topPadding = dimen(R.dimen.padding_small)
                            }.lparams(wrapContent, wrapContent) {
                                topToBottom = R.id.cardView
                                leftToLeft = PARENT_ID
                                rightToRight = PARENT_ID

                            }
                            countDownTextView = textView {
                                topPadding = dimen(R.dimen.padding_small)
                                bottomPadding = dimen(R.dimen.padding_small)
                                textSizeDimen = R.dimen.text_subject
                            }.lparams(wrapContent, wrapContent) {
                                topToBottom = R.id.qrcode_valid_time_textview
                                leftToLeft = PARENT_ID
                                rightToRight = PARENT_ID
                            }
                        }.lparams(matchParent, matchParent)
                    }
                }.lparams(0, 0) {
                    topToBottom = R.id.member_card_titlebar
                    bottomToBottom = PARENT_ID
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                }
            }
        }
    }
}