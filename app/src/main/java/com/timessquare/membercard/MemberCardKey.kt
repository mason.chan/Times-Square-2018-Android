package com.timessquare.membercard

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 11/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MemberCardKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberCardKey))

    override fun createFragment(): BaseFragment = MemberCardFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}