package com.timessquare.membercard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.timessquare.R

/**
 *   Created by Mason on 12/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberCardSliderView(var ctx: Context) : BaseSliderView(ctx) {

    override fun getView(): View {
        var v = LayoutInflater.from(ctx).inflate(R.layout.member_card_slider_view, null)
        var imageView = v.findViewById<ImageView>(R.id.image_view)
        bindEventAndShow(v, imageView)
        return v
    }
}