package com.timessquare.memberforgotpassword

import android.os.Bundle
import android.os.Looper
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.i
import com.pawegio.kandroid.runOnUiThread
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.apiservice.datamodel.ForgotPasswordModel
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx

/**
 *   Created by Mason on 13/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberForgotPasswordFragment : BaseFragment() {

    lateinit var mView: MemberForgotPasswordUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mView = MemberForgotPasswordUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView.mOKButton.onClick {
            AttemptForgotPassword()
        }
    }

    private fun AttemptForgotPassword() {
        var email = mView.mEmailEditText.text.toString()
        /**
         * field Checking
         */
        if (email.isEmpty() or !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView.mEmailTextLayout.error = getString(R.string.member_login_email_error)
            return
        }
        /**
         * Create Observable
         */
        var forgotPasswordObervable = MainActivity[ctx].apiService.forgotPassword(email)

        /**
         * Create Observer
         */
        var forgotPasswordObserver = object : Observer<ForgotPasswordModel> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                i("ForgotPassword", " onSubscribe")
                dismissDialog()
            }

            override fun onNext(t: ForgotPasswordModel) {
                i("ForgotPassword", " onNext errorCode:     " + t.errorCode)
                i("ForgotPassword", " onNext" + t.errorMessage)
                i("ForgotPassword", " onNext" + t.message)
                runOnUiThread() {
                    //                    i("ForgotPassword", "" + "UI    " + (Looper.myLooper() == Looper.getMainLooper()))
                    if (t.errorCode == null) {
                        MainActivity[ctx].AlertDialog(R.string.member_forgot_password_alert_title, R.string.member_forgot_password_alert_content, true).show()
                    } else {
                        MainActivity[ctx].AlertDialog(R.string.member_forgot_password_alert_title,R.string.member_forgot_password_alert_account_not_exist_content, true).show()
                    }
                }
            }

            override fun onError(e: Throwable) {
                i("ForgotPassword", " Error " + e)
            }
        }
        /**
         * Subscribe
         */

        forgotPasswordObervable.subscribeOn(Schedulers.io())
                .doOnSubscribe { popDialog(msgId = R.string.member_login_forgot_password_loading_dialog_message, cancelable = false) }
                .doFinally { }
                .subscribeWith(forgotPasswordObserver)
    }
}