package com.timessquare.memberforgotpassword

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import org.jetbrains.anko.constraint.layout.constraintLayout
import com.timessquare.R
import org.jetbrains.anko.*
import org.jetbrains.anko.design.textInputLayout

/**
 *   Created by Mason on 13/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberForgotPasswordUI() : AnkoComponent<MemberForgotPasswordFragment> {
    lateinit var mTitleBar: TitleBar
    lateinit var mEmailEditText: EditText
    lateinit var mEmailTextLayout: TextInputLayout
    lateinit var mOKButton: Button

    override fun createView(ui: AnkoContext<MemberForgotPasswordFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                mTitleBar = titleBar {
                    id = R.id.memberLoginTitleBar
                }.addTitle(R.string.member_forgot_password_title).addBackButton().addBackgroundColor(R.color.color_cover)
                        .lparams(width = matchParent) {
                            topToTop = PARENT_ID
                        }
                verticalLayout {
                    padding = dimen(R.dimen.padding_small)
                    // Email
                    textView(R.string.member_forgot_password_email_text_view) {
                        textColorResource = R.color.color_black
                        textSizeDimen = R.dimen.text_normal
                    }
                    mEmailEditText = include<EditText>(R.layout.car_park_edit_text) {
                        hintResource = R.string.member_forgot_password_email_edit_text_hint
                    }
                    mEmailTextLayout = textInputLayout { }
                }.lparams(matchParent, wrapContent) {
                    topToBottom = R.id.memberLoginTitleBar
                }
                mOKButton = button {
                    textResource = R.string.member_forgot_password_ok_button
                    textSizeDimen = R.dimen.text_subject
                    backgroundResource = R.drawable.button_sure
                    paddingHorizontal = dimen(R.dimen.padding_double)
                }.lparams(wrapContent, wrapContent) {
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                }
            }
        }
    }
}