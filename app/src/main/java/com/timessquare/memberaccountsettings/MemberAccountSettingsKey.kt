package com.timessquare.memberaccountsettings

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 12/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreatot")
@Parcelize
data class MemberAccountSettingsKey(override val tag: String) : BaseKey(tag) {

    constructor() : this(keyName(MemberAccountSettingsKey))

    override fun createFragment(): BaseFragment = MemberAccountSettingsFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}