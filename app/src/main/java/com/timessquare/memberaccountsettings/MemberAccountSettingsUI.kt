package com.timessquare.memberaccountsettings

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.CardView
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.memberchangepassword.MemberChangePasswordFragment
import com.timessquare.memberchangepassword.MemberChangePasswordKey
import com.timessquare.settings.SettingsKey
import com.timessquare.settings.language.LanguageKey
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import com.tmall.ultraviewpager.UltraViewPager
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.design.textInputLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 12/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberAccountSettingsUI() : AnkoComponent<MemberAccountSettingsFragment> {
    lateinit var mTitleBar: TitleBar

    override fun createView(ui: AnkoContext<MemberAccountSettingsFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                mTitleBar = titleBar {
                    id = R.id.memberLoginTitleBar
                }.addTitle(R.string.member_account_settings_title).addBackButton().addBackgroundColor(R.color.color_cover)
                        .lparams(width = matchParent) {
                            topToTop = PARENT_ID
                        }
                verticalLayout {
                    include<CardView>(R.layout.settings_item) {
                        onClick { MainActivity.get(ctx).navigateTo(MemberChangePasswordKey()) }
                        var textview = find<TextView>(R.id.text_view)
                        textview.textResource = R.string.member_account_settings_change_password
                    }

                }.lparams(matchParent, wrapContent) {
                    topToBottom = R.id.memberLoginTitleBar
                }
            }
        }
    }
}
