package com.timessquare.memberaccountsettings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import org.jetbrains.anko.AnkoContext


/**
 *   Created by Mason on 12/11/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberAccountSettingsFragment : BaseFragment() {
    lateinit var mView: MemberAccountSettingsUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberAccountSettingsUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}