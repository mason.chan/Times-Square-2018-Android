package com.timessquare.shopanddine

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.database.entity.ShopEntity
import com.timessquare.database.entity.SubShop
import com.timessquare.shopdetail.ShopDetailKey
import com.timessquare.shopfilter.ViewModel.DineFilterViewModel
import com.timessquare.shopfilter.ViewModel.ShopFilterViewModel
import com.timessquare.utils.Constant
import com.timessquare.utils.LocaleHelper
import com.timessquare.utils.SParem
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 30/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
abstract class ShopDineFragment : BaseFragment() {

    lateinit var mView: ShopDineUI
    private var mFastItemAdapter: FastItemAdapter<ShopDineItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = ShopDineUI(setTitle(), isShopNotFood())
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        i("ShopDineFragment", "onViewCreated")
        mFastItemAdapter = FastItemAdapter()
        mFastItemAdapter!!.withOnClickListener { v, adapter, item, position ->
            i("shopid", "shopid is :    " + item.mId)
            MainActivity.get(view.context).navigateTo(ShopDetailKey().withArgs { putInt(Constant.TAG_INTENT, item.mId) })
            false
        }
        mView.mRecyclerView.layoutManager = GridLayoutManager(context, 2)
        mView.mRecyclerView.adapter = mFastItemAdapter
        //fill with some sample data
//        mFastItemAdapter!!.add(IndexDummyData.shopDineItems)
        mFastItemAdapter!!.add(shopConverter(view.context, shopQuery(view.context)))

    }

    // return shopitem
    fun shopConverter(ctx: Context, shopEntityList: List<SubShop>): List<ShopDineItem> {
        val shopDineList = ArrayList<ShopDineItem>()

        shopEntityList.forEach {
            if (MainActivity[ctx].getLanguage() == LocaleHelper.englishLocale()) {
                shopDineList.add(ShopDineItem(it.id, it.thumbImage, it.name, it.number, it.floorId, context!!))
            } else {
                shopDineList.add(ShopDineItem(it.id, it.thumbImage, it.nameZh, it.number, it.floorId, context!!))
            }
        }
        return shopDineList
    }

    // query shops
    fun shopQuery(ctx: Context): List<SubShop> {

        /**
         * Recover ViewModel Setting
         */
        //Get the ViewModel

        val mModel = ViewModelProviders.of(activity!!).get(if (isShopNotFood()) ShopFilterViewModel::class.java else DineFilterViewModel::class.java)

        val categoryId = mModel.byCategory.value!!.mId
        val floorId = mModel.byFloor.value!!.mId
        var prefix = if (mModel.byPrefix.value!!.mId != 0) ctx.resources.getStringArray(R.array.shop_prefix_list)[mModel.byPrefix.value!!.mId] else ""
        prefix = prefix + "%"

        i("prefix", "categoryId:    " + categoryId + "`    floorId`    " + floorId + "   prefix " + prefix)
        // Query
        if (categoryId != 0 && floorId != 0) {
            return MainActivity.get(ctx).database.shopDao().filterShopCFP(isShopNotFood(), categoryId, floorId, prefix)
        } else if (categoryId != 0) {
            return MainActivity.get(ctx).database.shopDao().filterShopCP(isShopNotFood(), categoryId, prefix)
        } else if (floorId != 0) {
            return MainActivity.get(ctx).database.shopDao().filterShopFP(isShopNotFood(), floorId, prefix)
        } else {
            return MainActivity.get(ctx).database.shopDao().filterShopP(isShopNotFood(), prefix)
        }
    }

    abstract fun setTitle(): Int

    abstract fun isShopNotFood(): Boolean
}