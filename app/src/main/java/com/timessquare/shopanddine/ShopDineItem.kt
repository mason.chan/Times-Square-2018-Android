package com.timessquare.shopanddine

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.timessquare.R
import com.timessquare.apiservice.Path
import com.mikepenz.fastadapter.items.AbstractItem
import org.jetbrains.anko.backgroundColor

/**
 *   Created by Mason on 30/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopDineItem(mId: Int, mThumbnailUrl: String?, mName: String?, mNumber: String?, mFloorId: Int, ctx: Context) : AbstractItem<ShopDineItem, ShopDineItem.ViewHolder>() {

    var mId: Int = -1
    var mNumber: String
    var mName: String
    var mThumbnailUrl: String?
    var mFloorId = -1
    var mCategory = -1

    init {
        this.mId = mId
        this.mNumber = mNumber!!
        this.mName = mName!!
        this.mThumbnailUrl = mThumbnailUrl
        this.mFloorId = mFloorId
    }

    override fun getType(): Int {
        return R.id.shop_dine_item_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.shop_dine_item_view
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        //get the context
        val ctx = holder.itemView.context

        //define our data for the view
        holder.textViewName!!.text = mName
        holder.textViewNumber!!.setText(mNumber + " ," + ctx.resources.getStringArray(R.array.shop_floor_list)[mFloorId])
        if(!mThumbnailUrl.isNullOrEmpty()) {
            Glide.with(ctx).load(Path.PRO_IMAGE_URL + mThumbnailUrl).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(holder.imageView!!)
        } else {
            Glide.with(ctx).load(R.drawable.default_pic_black).apply(RequestOptions().placeholder(R.drawable.default_pic_black)).thumbnail(0.1f).into(holder.imageView!!)
        }
    }


    /**
     * Shop & Dine Item ViewHolder
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {
        var textViewName: TextView
        var textViewNumber: TextView
        lateinit var imageView: ImageView

        init {
            textViewName = view.findViewById(R.id.text_view_name)
            textViewNumber = view.findViewById(R.id.text_view_number)
            imageView = view.findViewById(R.id.image_view)
        }
    }
}
