package com.timessquare.shopanddine

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.shopfilter.ShopFilterKey
import com.timessquare.shopsearch.ShopSearchKey
import com.timessquare.utils.Constant
import com.timessquare.views.shopDineView
import org.jetbrains.anko.*

/**
 *   Created by Mason on 30/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopDineUI(var titleRes: Int, var isShopNotFood: Boolean) : AnkoComponent<ShopDineFragment> {

    lateinit var mRecyclerView: RecyclerView

    override fun createView(ui: AnkoContext<ShopDineFragment>): View {
        return with(ui) {
            frameLayout {
                shopDineView {
                    this@ShopDineUI.mRecyclerView = mRecyclerView
                    mTitleBar
                            .addTitle(titleRes)
                            .addMenuButton()
                            .addButton(R.drawable.ic_filter, Gravity.END, R.id.shop_filter_button, onClickListener = View.OnClickListener {
                                MainActivity[view.context].navigateTo(ShopFilterKey().withArgs {
                                    putBoolean(Constant.TAG_INTENT, isShopNotFood)
                                })
                            }, marginRes = R.dimen.margin_normal)
                            .addButton(R.drawable.ic_search, Gravity.END, R.id.shop_search_button, onClickListener = View.OnClickListener {
                                MainActivity[view.context].navigateTo(ShopSearchKey().withArgs {
                                    putBoolean(Constant.TAG_INTENT, isShopNotFood)
                                })
                            })
                }
            }

        }
    }
}