package com.timessquare.shopanddine.shopanddinedetail

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.items.AbstractItem
import com.timessquare.R

/**
 *   Created by Mason on 4/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class ShopDineDetailOfferItem(var iconDrawable: Int, var officeString: Int) : AbstractItem<ShopDineDetailOfferItem, ShopDineDetailOfferItem.ViewHolder>() {
    override fun getType(): Int {
        return R.id.shopDineDetailOfferItem
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.shop_dine_offer_item_view
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        //get the context
        val ctx = holder.itemView.context
        holder.offer.text = ctx.getString(officeString)
        Glide.with(ctx).load(iconDrawable).into(holder.icon)
    }

    /**
     * Shop & Dine Detail Office Item
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {
            var icon : ImageView
        var offer : TextView
        init {
            icon = view.findViewById(R.id.icon)
            offer = view.findViewById(R.id.offer)
        }
    }
}