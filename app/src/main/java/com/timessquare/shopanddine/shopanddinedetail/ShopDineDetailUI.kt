package com.timessquare.shopanddine.shopanddinedetail

import android.support.v7.widget.RecyclerView
import android.util.Log.i
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.shopanddine.ShopDineItem
import com.timessquare.shopdinefloormap.ShopDineFloorMapKey
import com.timessquare.utils.Constant
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 30/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopDineDetailUI(var item: ShopDineItem? = null) : AnkoComponent<ShopDineDetailFragment> {

    lateinit var titleBar: TitleBar
    lateinit var scrollView: ScrollView
    lateinit var coverImageView: ImageView
    lateinit var vicRecyclerView: RecyclerView
    lateinit var titleTextView: TextView
    lateinit var categoryTextView: TextView
    lateinit var addressLinearLayout: LinearLayout
    lateinit var addressTextView: TextView
    lateinit var timeLinearLayout: LinearLayout
    lateinit var timeTextView: TextView
    lateinit var contactTextView: TextView
    lateinit var contentTextView: TextView
    lateinit var meunVerticalLayout: LinearLayout
     var shopNumber = ""
     var shopFloor = 0
    override fun createView(ui: AnkoContext<ShopDineDetailFragment>): View {
        return with(ui) {
            verticalLayout {
                frameLayout {
                    scrollView = scrollView {
                        scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
                        verticalLayout {
                            //pic
                            coverImageView = imageView {
                                id = R.id.ShopDineDetailImageView
                                adjustViewBounds = true
                                imageResource = R.drawable.default_pic_black
                            }.lparams(matchParent, wrapContent)
                            // Recycler
                            vicRecyclerView = recyclerView {

                            }
                            // Privileges

                            // shadow
                            view { backgroundResource = R.drawable.shadow_bottom }.lparams(matchParent, 8)
                            // body
                            verticalLayout {
                                padding = dimen(R.dimen.padding_small)
                                //name
                                titleTextView = textView {
                                    //                                text = "TITTANY & CO."
                                    textSizeDimen = R.dimen.text_title
                                }
                                // cat
                                categoryTextView = textView {
                                    //                                text = "jewerly/ Watchs / Optical"
                                    textSizeDimen = R.dimen.text_small
                                    textColorResource = R.color.color_text_gray
                                }.lparams() {
                                    topMargin = dimen(R.dimen.margin_small)
                                }
                                // address
                                addressLinearLayout = linearLayout {
                                    gravity = Gravity.CENTER_VERTICAL
                                    imageView {
                                        imageResource = R.drawable.ic_location
                                        rightPadding = dimen(R.dimen.navigation_padding_button)
                                    }
                                    addressTextView = textView {
                                        //                                    text = "Square on 9/F"
                                        maxLines = 1
                                        textSizeDimen = R.dimen.text_small
                                    }
                                    onClick {
                                        i("shopId", "number       " + shopNumber)
                                        MainActivity[view.context].navigateTo(ShopDineFloorMapKey().withArgs {
                                            putString(Constant.TAG_INTENT, shopNumber)
                                            putInt(Constant.TAG_INTENT2, shopFloor)
                                        })
                                    }
                                }.lparams() {
                                    topMargin = dimen(R.dimen.margin_normal)
                                }
                                // office hours
                                timeLinearLayout = linearLayout {
                                    gravity = Gravity.CENTER_VERTICAL
                                    imageView {
                                        imageResource = R.drawable.ic_time
                                        rightPadding = dimen(R.dimen.navigation_padding_button)
                                    }
                                    timeTextView = textView {
                                        textSizeDimen = R.dimen.text_small
                                        maxLines = 1
//                                    text = "Mon to Sun 09:00 - 20:00 , Wed Closed"
                                    }
                                }.lparams() {
                                    topMargin = dimen(R.dimen.margin_normal)
                                }
                                // contact
                                timeLinearLayout = linearLayout {
                                    gravity = Gravity.CENTER_VERTICAL
                                    imageView {
                                        imageResource = R.drawable.ic_phone
                                        rightPadding = dimen(R.dimen.navigation_padding_button)
                                    }
                                    contactTextView = textView {
                                        textSizeDimen = R.dimen.text_small
                                        maxLines = 1
                                    }
                                }.lparams() {
                                    topMargin = dimen(R.dimen.margin_normal)
                                }
                                // content
                                contentTextView = textView {
                                    //                                text = "content"
                                }.lparams() {
                                    topMargin = dimen(R.dimen.margin_normal)
                                }

                            }.lparams(matchParent, wrapContent) {
                            }
                        }
                    }
                    titleBar = titleBar {
                        bkc.alpha = 0f
                    }.addBackButton(true).addBackgroundColor(R.color.color_cover)
                }.lparams(weight = 1f)
                // shadow
                view { backgroundResource = R.drawable.shadow_bottom }.lparams(matchParent, 8)
                meunVerticalLayout = verticalLayout {
                    backgroundColorResource = R.color.color_white
                    gravity = Gravity.CENTER
                    padding = dimen(R.dimen.padding_menu)
                    textView {
                        backgroundResource = R.drawable.button_sure
                        textResource = R.string.shop_menu
                        textSizeDimen = R.dimen.text_normal
                        gravity = Gravity.CENTER
                        verticalPadding = dimen(R.dimen.padding_menu)
                        horizontalPadding = dimen(R.dimen.padding_double2)
                    }.lparams(wrapContent, wrapContent)
                }.lparams(matchParent, wrapContent){
                }
            }
        }
    }
}