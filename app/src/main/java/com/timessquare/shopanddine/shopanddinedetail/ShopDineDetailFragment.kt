package com.timessquare.shopanddine.shopanddinedetail

import android.Manifest
import android.Manifest.permission.CALL_PHONE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.text.util.Linkify
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.apiservice.Path
import com.timessquare.database.entity.ShopEntity
import com.timessquare.multipleposter.MultiplePosterKey
import com.timessquare.utils.Constant
import com.timessquare.utils.LocaleHelper
import com.timessquare.utils.OpenHourConverter.openHourToReadableString
//import com.hita.utils.StringConverters.openHourToObject
//import com.hita.utils.StringConverters.vicAttributeToObject
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx

/**
 *   Created by Mason on 30/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

abstract class ShopDineDetailFragment : BaseFragment() {

    val mId: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: -1 }

    var isEnglish = true
    lateinit var shopEntity: ShopEntity

    lateinit var mView: ShopDineDetailUI

    private var mFastItemAdapter: FastItemAdapter<ShopDineDetailOfferItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = ShopDineDetailUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleAlpha()
//        isEnglish = MainActivity[view.context].isEnglish
        isEnglish = MainActivity[view.context].getLanguage() == LocaleHelper.englishLocale()
        shopEntity = MainActivity.get(view.context).database.shopDao().getById(mId)
        i("shopEntity", "contact:   " + shopEntity.contact)
        defaultSetting(view)
        fillData(view)
    }

    fun defaultSetting(view: View) {
        i("shopEntity.detailImage ", "shopEntity.detailImage " + shopEntity.detailImage.toString())
        i("shopEntity.contact", "contact:   " + shopEntity.contact)
        if (shopEntity.detailImage != null) {
            Glide.with(view.context).load(Path.PRO_IMAGE_URL + shopEntity.detailImage).apply(RequestOptions().placeholder(R.drawable.default_pic_black).fallback(R.drawable.default_pic)).thumbnail(0.1f).into(mView.coverImageView)
        } else {
            mView.coverImageView.imageResource = R.drawable.default_pic
        }
        // office recyclerview
        mFastItemAdapter = FastItemAdapter()
        mView.vicRecyclerView.layoutManager = GridLayoutManager(context, 2)
        mView.vicRecyclerView.adapter = mFastItemAdapter
        mFastItemAdapter!!.add(addOffice())
        i("category", "category" + shopEntity.id)
        mView.shopNumber = shopEntity.number
        mView.shopFloor = shopEntity.floorId
        mView.titleTextView.text = if (isEnglish) shopEntity.name else shopEntity.nameZh
        i("category", "category" + shopEntity.categoryId.toString())
//        i("category", "category" + view.context.resources.getStringArray(R.array.all_category_list)[shopEntity.categoryId])
        mView.categoryTextView.text = view.context.resources.getStringArray(R.array.all_category_list)[shopEntity.categoryId]
        mView.addressTextView.text = shopEntity.number + " ," + view.context.resources.getStringArray(R.array.shop_floor_list)[shopEntity.floorId]
//        mView.timeTextView.text = openHourToReadableString(view.context, openHourToObject(shopEntity.openHours))
        mView.timeTextView.text = openHourToReadableString(view.context, shopEntity.openHours!!)
        if (shopEntity.contact!!.length > 0) {
            mView.contactTextView.text = shopEntity.contact
            mView.timeLinearLayout.onClick {
                var callIntent = Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + shopEntity.contact));
                if (ContextCompat.checkSelfPermission(ctx, CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    MainActivity[ctx].startActivity(callIntent)
                } else {
                    ActivityCompat.requestPermissions(MainActivity[ctx],
                            arrayOf(Manifest.permission.CALL_PHONE), 1)
                }
            }
        } else {
            mView.timeLinearLayout.visibility = View.GONE
        }
        mView.meunVerticalLayout.visibility = if (shopEntity.menu!!.size > 0) View.VISIBLE else View.GONE
        mView.meunVerticalLayout.onClick {
            MainActivity[view.context].navigateTo(MultiplePosterKey().withArgs {
                //                putStringArrayList(Constant.TAG_INTENT, ArrayList(stringToListString(shopEntity.menu)))
                putStringArrayList(Constant.TAG_INTENT, shopEntity.menu)
            })
        }
    }

    abstract fun addOffice(): ArrayList<ShopDineDetailOfferItem>
    abstract fun fillData(view: View)

    fun titleAlpha() {
        mView.scrollView.getViewTreeObserver().addOnScrollChangedListener {
            var scrollY = mView.scrollView.getScrollY() / 3
            if (scrollY >= 0) {
                if (scrollY > 100) {
                    scrollY = 100
                }
                scrollY = scrollY / 10
                var alphaPercentage = scrollY.toFloat() / 10
                mView.titleBar.bkc.alpha = alphaPercentage

            }
        }
    }
}
