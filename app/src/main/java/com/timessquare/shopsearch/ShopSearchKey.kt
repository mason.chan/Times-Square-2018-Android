package com.timessquare.shopsearch

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 3/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class ShopSearchKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(ShopSearchKey))

    override fun createFragment(): BaseFragment = ShopSearchFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}