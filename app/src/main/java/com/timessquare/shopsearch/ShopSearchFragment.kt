package com.timessquare.shopsearch

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.shopanddine.ShopDineItem
import com.timessquare.shopdetail.ShopDetailKey
import com.timessquare.utils.Constant
import com.timessquare.utils.LocaleHelper
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItemAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import org.jetbrains.anko.AnkoContext
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter.items
import com.mikepenz.fastadapter.listeners.ItemFilterListener
import org.jetbrains.anko.appcompat.v7.coroutines.onQueryTextListener
import org.jetbrains.anko.sdk25.coroutines.onQueryTextListener


/**
 *   Created by Mason on 3/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class ShopSearchFragment : BaseFragment(), ItemFilterListener<ShopDineItem> {

    //    private var mFastItemAdapter: FastItemAdapter<ShopDineItem>? = null
    val isShopNotFood: Boolean by lazy { arguments?.getBoolean(Constant.TAG_INTENT) ?: true }

    lateinit var mFastAdapter: FastAdapter<ShopDineItem>
    lateinit var mItemAdapter: ItemAdapter<ShopDineItem>
    lateinit var mView: ShopSearchUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = ShopSearchUI()

        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initKeyboard(true)
        initRecyclerView(view.context)
//        mFastItemAdapter!!.add(getAllShops(view.context))
        mItemAdapter!!.add(getAllShops(view.context))
        mView.titleBar.backButton!!.setOnTouchListener { v, event ->
            initKeyboard(false)
            false
        }
    }

    fun getAllShops(ctx: Context): List<ShopDineItem> {
        val shopEntityList = MainActivity.get(ctx).database.shopDao().getAllBaseInfo(isShopNotFood)
        val shopDineList = ArrayList<ShopDineItem>()
        shopEntityList.forEach {
            //            if (MainActivity[ctx].isEnglish) {
            if (MainActivity[ctx].getLanguage() == LocaleHelper.englishLocale()) {
                shopDineList.add(ShopDineItem(it.id, it.thumbImage, it.name, it.number, it.floorId, context!!))
            } else {
                shopDineList.add(ShopDineItem(it.id, it.thumbImage, it.nameZh, it.number, it.floorId, context!!))
            }
        }
        return shopDineList
    }

    private fun initRecyclerView(ctx: Context) {

        mItemAdapter = items()
        mFastAdapter = FastAdapter.with(mItemAdapter)
        //configure the itemAdapter
        mItemAdapter.getItemFilter().withFilterPredicate(IItemAdapter.Predicate<ShopDineItem> { item, constraint ->
            //return true if we should filter it out
            //return false to keep it
            item.mName.toLowerCase().contains(constraint!!.toString().toLowerCase())

        })
        mFastAdapter!!.withOnClickListener { v, adapter, item, position ->
            MainActivity.get(ctx).navigateTo(ShopDetailKey().withArgs { putInt(Constant.TAG_INTENT, item.mId) })
            false
        }
        mItemAdapter.getItemFilter().withItemFilterListener(this)
        mView.recyclerView.layoutManager = GridLayoutManager(ctx, 2)
        mView.recyclerView.setItemAnimator(DefaultItemAnimator())
        mView.recyclerView.adapter = mFastAdapter

        mView.searchText.onQueryTextListener {
            onQueryTextSubmit { s ->
                mItemAdapter.filter(s)
                true
            }
            onQueryTextChange { s ->
                mItemAdapter.filter(s)
                true
            }
        }
        mView.searchText.onActionViewExpanded()

        mView.recyclerView.setOnTouchListener { v, event ->
            initKeyboard(false)
            false
        }

    }

    override fun onReset() {
    }

    override fun itemsFiltered(constraint: CharSequence?, results: MutableList<ShopDineItem>?) {
//        T(context, "filtered items count: " + mItemAdapter.getAdapterItemCount(), Toast.LENGTH_SHORT).show()
    }

    fun initKeyboard(open: Boolean) {
        i("initKeyboard", "open :" + open)
        mView.searchText.requestFocus()
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (open) {
            imm.showSoftInput(mView.searchText, InputMethodManager.SHOW_IMPLICIT)
        } else {
            imm.hideSoftInputFromWindow(mView.searchText.getWindowToken(), 0)
        }
    }

}