package com.timessquare.shopsearch

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SearchView
import com.timessquare.R
import com.timessquare.R.style.SearchViewStyle
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.style
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 *   Created by Mason on 3/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class ShopSearchUI : AnkoComponent<ShopSearchFragment> {

    lateinit var titleBar: TitleBar
//    lateinit var backButton: ImageView
    lateinit var searchText: SearchView
    lateinit var recyclerView: RecyclerView
    override fun createView(ui: AnkoContext<ShopSearchFragment>): View {
        return with(ui) {
            verticalLayout {
                titleBar = titleBar {
                }.addBackgroundColor(R.color.color_cover)
                        .addBackButton()
                        .addTitle(R.string.shop_search_title)
                include<LinearLayout>(R.layout.shop_search_bar_view) {
                    searchText = findViewById(R.id.searchText)
                }
                recyclerView = recyclerView {

                }

            }
        }
    }
}