package com.timessquare.memberscancard

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 13/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MemberScanCardKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberScanCardKey))

    override fun createFragment(): BaseFragment = MemberScanCardFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}