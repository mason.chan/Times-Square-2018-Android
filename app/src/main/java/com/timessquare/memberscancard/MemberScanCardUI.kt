package com.timessquare.memberscancard

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.timessquare.R
import com.timessquare.views.titleBar
import com.journeyapps.barcodescanner.CompoundBarcodeView
import com.timessquare.MainActivity
import com.timessquare.beourvicmember.BeOurVICMemberKey
import kotlinx.android.synthetic.main.zxing_barcode_scanner.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 13/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberScanCardUI() : AnkoComponent<MemberScanCardFragment> {
    lateinit var scanner: CompoundBarcodeView
    lateinit var scenStringTextView: TextView
    override fun createView(ui: AnkoContext<MemberScanCardFragment>): View {
        return with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = PARENT_ID
                }
                titleBar {
                    id = R.id.memeber_scan_card_title_bar
                }.addBackgroundColor(R.color.color_cover).addBackButton().addTitle(R.string.member_scan_card_title)
                scanner = customView<CompoundBarcodeView> {
                    id = R.id.memeber_scan_card_compound_barcode_view
                }.lparams(matchParent, 0) {
                    dimensionRatio = "H,1:1"
                    topToBottom = R.id.memeber_scan_card_title_bar
                }
                scenStringTextView = textView {
                    textResource = R.string.member_scan_card_scan_your_vic_card
                    textSizeDimen = R.dimen.text_subject
                    gravity = Gravity.CENTER
                    paddingHorizontal = dimen(R.dimen.padding_double)
                }.lparams(){
                    topToBottom = R.id.memeber_scan_card_compound_barcode_view
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                    topMargin = dimen(R.dimen.margin_double)
                }
                // Not a memeber yet?
                textView {
                    id = R.id.not_a_member_yet_text_view
                    textResource = R.string.member_scan_card_not_a_member_yet
                    textSizeDimen = R.dimen.text_subject
                    gravity = Gravity.CENTER
                    topPadding = dimen(R.dimen.padding_small)
                    bottomPadding = dimen(R.dimen.padding_small)
                    onClick {
                        MainActivity.get(view.context).navigateTo(BeOurVICMemberKey())
                    }
                }.lparams(matchParent, wrapContent) {
                    bottomToBottom = PARENT_ID
                }
                view {
                    backgroundColorResource = R.color.color_text_gray
                }.lparams(matchParent, 1){
                    bottomToTop = R.id.not_a_member_yet_text_view
                }
            }
        }
    }
}