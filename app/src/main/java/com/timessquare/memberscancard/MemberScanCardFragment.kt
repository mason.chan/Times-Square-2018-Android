package com.timessquare.memberscancard

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.zxing.ResultPoint
import com.google.zxing.integration.android.IntentIntegrator
import com.timessquare.BaseFragment
import com.timessquare.BuildConfig
import com.timessquare.MainActivity
import com.timessquare.membercard.MemberCardKey
import com.timessquare.memberregister.MemberRegisterKey
import com.timessquare.utils.Constant
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import com.pawegio.kandroid.runOnUiThread
import com.timessquare.R
import com.timessquare.apiservice.datamodel.OldCardInfoModel
import com.timessquare.checkcardinfo.CheckCardInfoKey
import com.timessquare.database.entity.vicCard
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.textResource
import java.util.*

/**
 *   Created by Mason on 13/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
/**
 * This page has two reason to come here,
 *  1. member register, then go to member info page
 *  2. add e-card, come with param: vicType, then back to member card
 */
class MemberScanCardFragment : BaseFragment() {

    private val PERMISSION_REQUEST_CAMERA = 0
    val vicType: Int by lazy { arguments?.getInt(Constant.TAG_INTENT) ?: 0 }
    lateinit var mView: MemberScanCardUI
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = MemberScanCardUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
        }

        mView.scanner.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                i("barcodeResult", "barcodeResult:      " + result!!.text)
                i("vicType ",  vicType.toString())
                /**
                 * check if vicType == -1 (no vicType), go to member register
                 * check if vicType == -2 (no vicType), go to check card info
                 * else register card and then go to member card
                 */
                if (vicType == 0) {
                    MainActivity[view.context].navigateTo(MemberRegisterKey().withArgs {
                        putString(Constant.TAG_INTENT, result!!.text)
                    })
                } else if (vicType == -2) {
                    mView.scanner.pause()
                    attemptCheckCardInfo(result!!.text)
                } else {
                    mView.scanner.pause()
                    attemptAddCard(result!!.text)
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {

            }
        })
    }

    private fun init() {
        mView.scenStringTextView.textResource = when(vicType) {
             -2 -> {
                 R.string.member_scan_card_scan_your_vic_card_check_card_info
             }
            -1 -> {
                R.string.member_scan_card_scan_your_vic_card
            }
            else -> {
                R.string.member_scan_card_scan_your_vic_card_registration
            }
        }
    }


    override fun onResume() {
        super.onResume()
        mView.scanner.resume()
    }

    override fun onPause() {
        super.onPause()
        mView.scanner.pause()
    }


    fun attemptCheckCardInfo(cardCode: String) {

        var card = vicCard("", "1", -1, -1, -1, "", "", "", "", -1, Date())

        var cardInfoObserver = MainActivity[ctx].apiService.checkCardInfo(cardCode)

        cardInfoObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<OldCardInfoModel> {
                    override fun onComplete() {
                        val gson = Gson()
                        val type = object : TypeToken<vicCard>() {
                        }.type
                        val json = gson.toJson(card, type)
                        MainActivity[ctx].navigateTo(CheckCardInfoKey().withArgs {
                            putString(Constant.TAG_INTENT, json)
                        })
                    }

                    override fun onSubscribe(d: Disposable) {
                        i("cardInfoObserver", "onSubscribe       " + d.toString())
                    }

                    override fun onNext(t: OldCardInfoModel) {
                        var data = t.oldCardData
                        card.point = data.member_point
                        card.expiryDate = data.member_point_expiry_date
                        card.cardPin = data.card_pin
                        card.firstName = data.member_first_name
                        card.lastName = data.member_last_name
                        card.email = data.member_email
                        card.tel = data.member_tel
                    }

                    override fun onError(e: Throwable) {
                        i("cardInfoObserver", "onError       " + e.message)
                        mView.scanner.resume()
                    }
                })
    }

    fun attemptAddCard(cardCode: String) {
        i("MemberScanCard", "attemptAddCard")
        MainActivity[ctx].addEcard(cardCode)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                        onComplete = {
                            i("MemberCardFragment", " onComplete go to card page 1")
                        },
                        onNext = {
                            /**
                             * Success
                             */
                            if (it.message != null) {
                                MainActivity[ctx].login()
                                        .subscribeOn(Schedulers.io())
                                        .doFinally { }
                                        .subscribeBy(
                                                onNext = {
                                                    i("MemberCardFragment", "save Member2 and go back to card page")
                                                    MainActivity[ctx].updateMemberData(it)
                                                },
                                                onError = {
                                                    i("MemberCardFragment", "onError  2 " + it.message)
                                                    ctx.runOnUiThread {
                                                        MainActivity[ctx].AlertDialog("Error", it.message!!, true).show()
                                                    }
                                                },
                                                onComplete = {
                                                    i("MemberCardFragment", "go to card page2")
                                                    ctx.runOnUiThread {
                                                        MainActivity[ctx].AlertDialog("Success", "Add Card Success! ", true).show()
                                                    }
                                                }
                                        )

                            } else {
                                /**
                                 * Failure
                                 */
                                i("MemberCardFragment", "onNext1 " + it.errorMessage)
                                ctx.runOnUiThread {
                                    MainActivity[ctx].AlertDialog("Error", it.errorMessage!!, true).show()
                                }

                            }
                        },
                        onError = {
                            i("MemberCardFragment", "onError  1  " + it.message)
                            ctx.runOnUiThread {
                                MainActivity[ctx].AlertDialog("Error", it.message!!, true).show()
                            }
                        }

                )
    }
}