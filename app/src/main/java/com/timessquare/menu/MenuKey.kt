package com.timessquare.menu

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 28/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class MenuKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MenuKey))

    override fun createFragment(): BaseFragment = MenuFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}