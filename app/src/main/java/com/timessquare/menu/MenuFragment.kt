package com.timessquare.menu

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.dummy.IndexDummyData
import com.timessquare.index.IndexKey
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 28/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MenuFragment : BaseFragment() {

    private var mFastItemAdapter: FastItemAdapter<MenuItem>? = null

    @BindView(R.id.MenuRecyclerView) lateinit var recyclerView : RecyclerView
    @BindView(R.id.MenuMember) lateinit var menuMember : LinearLayout
    @BindView(R.id.MenuCancel) lateinit var menuCancel : LinearLayout


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = MenuUI().createView(AnkoContext.create(activity!!, this))
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val menuKey = getKey<MenuKey>()

        mFastItemAdapter = FastItemAdapter()
        mFastItemAdapter!!.withOnClickListener {v, adapter, item, position ->
//            Toast.makeText(view.context, item.mName, Toast.LENGTH_SHORT).show()
            if(item.mKey != null) {
                if (item.mKey == IndexKey()) {
                    MainActivity.get(view.context).replaceHistory(item.mKey!!)

                }
                MainActivity.get(view.context).replaceHistoryWithIndex(item.mKey!!)
            }
            false
        }

        recyclerView.layoutManager = GridLayoutManager(view.context, 3)
        recyclerView.adapter = mFastItemAdapter
        mFastItemAdapter!!.add(IndexDummyData.menuItem)


        // Cancel function
        menuCancel.setOnClickListener {
            MainActivity.get(view.context).onBackPressed()
        }
        menuMember.setOnClickListener {
            MainActivity.get(view.context).requestToGoToMemberPage()
        }
    }
}