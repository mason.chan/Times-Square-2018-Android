package com.timessquare.menu

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.timessquare.BaseKey
import com.timessquare.R
import com.mikepenz.fastadapter.items.AbstractItem
import org.jetbrains.anko.textResource

/**
 *   Created by Mason on 28/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MenuItem : AbstractItem<MenuItem, MenuItem.ViewHolder>() {


     var mName: Int = 0
    var mImage: Int = 0
     var mKey: BaseKey? = null

    fun withName(name: Int): MenuItem {
        this.mName = name
        return this
    }

    fun withImage(image: Int): MenuItem {
        this.mImage = image
        return this
    }

    fun withKey(key: BaseKey): MenuItem {
        this.mKey = key
        return this
    }


    override fun getType(): Int {
        return R.id.index_item_id
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.menu_item
    }

    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>?) {
        super.bindView(holder, payloads)
        val ctx = holder.itemView.context

        //define our data for the view
        holder.textView!!.textResource = mName
        holder.imageView.setImageResource(mImage)


    }

    /**
     * MenuItem ViewHolder
     */
    class ViewHolder(protected var view: View) : RecyclerView.ViewHolder(view) {

        lateinit var textView: TextView
        lateinit var imageView: ImageView

        init {
//            ButterKnife.bind(this, view)
            textView = view.findViewById(R.id.text_view)
            imageView = view.findViewById(R.id.image_view)
        }

    }
}