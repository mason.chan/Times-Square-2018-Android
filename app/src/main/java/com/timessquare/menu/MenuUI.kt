package com.timessquare.menu

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.widget.LinearLayout.HORIZONTAL
import android.util.Log.i
import android.view.Gravity
import android.view.View
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.utils.SParem
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 *   Created by Mason on 28/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */


class MenuUI() : AnkoComponent<MenuFragment> {

    override fun createView(ui: AnkoContext<MenuFragment>): View {
        i("MenuUI", "MenuUI")
        return with(ui) {
            constraintLayout() {
                backgroundColorResource = R.color.color_white
                // Background
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(matchParent, wrapContent) {
                    bottomToBottom = PARENT_ID
                }
                titleBar(hasBackground = false, isIndexPage = false) {
                    id = R.id.MenuTitle
                }
                        .addCancelButton()
                        .addLogo()
                        .lparams(matchParent, wrapContent)
                // RecyclerView
                recyclerView {
//                    backgroundColorResource = R.color.colorPrimaryDark
                    id = R.id.MenuRecyclerView
//                    horizontalPadding = dimen(R.dimen.padding_small)
                }.lparams(width = matchParent, height = matchConstraint) {
                    topToBottom = R.id.MenuTitle
                    bottomToTop = R.id.MenuMember

                }
                // Member
                linearLayout {
                    backgroundResource = R.drawable.menu_border
                    id = R.id.MenuMember
                    gravity = Gravity.CENTER
                    padding = dimen(R.dimen.padding_item)
                    // icon
                    imageView {
                        imageResource = R.drawable.ic_login
                        padding = dimen(R.dimen.padding_item)
                    }.lparams(dimen(R.dimen.navigation_button), dimen(R.dimen.navigation_button))
                    // text
                    textView {
                         if (MainActivity.get(ctx).sp.getBoolanValue(SParem.Member_Logined, false)) {
                             textResource = R.string.menu_member_logout
                        } else {
                             textResource = R.string.menu_member_login
                         }
                        textColorResource = R.color.color_black
                    }.lparams(wrapContent, wrapContent)

                }.lparams(matchParent, wrapContent) {
                    topToBottom = R.id.MenuRecyclerView
                    bottomToTop = R.id.MenuCancel
                }
                // Close Menu
                linearLayout {
                    backgroundColorResource = R.color.color_cover
                    id = R.id.MenuCancel
                    gravity = Gravity.CENTER
                    padding = dimen(R.dimen.padding_item)

                    // icon
                    imageView {
                        imageResource = R.drawable.ic_cancel
                        padding = dimen(R.dimen.padding_item)
                    }.lparams(dimen(R.dimen.navigation_button), dimen(R.dimen.navigation_button))
                    // text
                    textView {
                        textResource = R.string.menu_close_menu
                        textColorResource = R.color.color_black
                    }.lparams(wrapContent, wrapContent)

                }.lparams(matchParent, wrapContent) {
                    topToBottom = R.id.MenuMember
                    bottomToBottom = PARENT_ID
                }
            }
        }
    }
}