package com.timessquare.agreement

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.index.IndexKey
import com.timessquare.utils.SParem
import kotlinx.android.synthetic.main.agreement_terms_of_service_view.*

/**
 *   Created by Mason on 18/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class AgreementTermsOfServicePage : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.agreement_terms_of_service_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        body.movementMethod = ScrollingMovementMethod()
        decline.setOnClickListener { MainActivity.get(view.context).finish() }
        accept.setOnClickListener {
            MainActivity.get(view.context).sp.setValue(SParem.Terms_Of_Service_Agreed, true)
            MainActivity.get(view.context).replaceHistory(IndexKey())
        }
    }
}