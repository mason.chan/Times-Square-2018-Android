package com.timessquare.agreement

import android.annotation.SuppressLint
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 18/5/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class AgreementTermsOfServiceKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(AgreementTermsOfServiceKey))

    override fun createFragment() = AgreementTermsOfServicePage()

    override fun stackIdentifier(): String = ""
    companion object {}
}