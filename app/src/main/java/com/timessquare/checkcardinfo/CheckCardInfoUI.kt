package com.timessquare.checkcardinfo

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.text.InputType
import android.text.util.Linkify
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.database.entity.vicCard
import com.timessquare.views.titleBar
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import java.text.SimpleDateFormat
import java.util.*

/**
 *   Created by Mason on 4/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class CheckCardInfoUI(var card : vicCard) : AnkoComponent<CheckCardInfoFragment> {
    override fun createView(ui: AnkoContext<CheckCardInfoFragment>) =
         with(ui) {
            constraintLayout {
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams() {
                    bottomToBottom = PARENT_ID
                }
                titleBar {
                    id = R.id.member_card_titlebar
                }.addBackgroundColor(R.color.color_cover)
                        .addTitle(R.string.check_card_info_title)
                        .addBackButton()

                constraintLayout {
                    cardView {
                        verticalLayout {
                            backgroundColorResource = R.color.color_white
                            padding = dimen(R.dimen.padding_small)
                            include<RelativeLayout>(R.layout.member_card_info) {
                                var name = find<TextView>(R.id.name)
                                var value = find<TextView>(R.id.value)
                                name.textResource = R.string.member_card_member_points
                                name.textColorResource = R.color.color_black
                                name.textSizeDimen = R.dimen.text_subject
                                value.text = card.point.toString()
                                value.textSize = 40f
                            }

                            include<RelativeLayout>(R.layout.member_card_info) {
                                find<TextView>(R.id.name).textResource = R.string.member_card_expiry_date
                                find<TextView>(R.id.value).text = SimpleDateFormat("yyyy-MM-dd").format(card.expiryDate)
//                                        find<TextView>(R.id.value).text = card.expiryDate.year.toString() + "-" + card.expiryDate.month.toString() + "-" + card.expiryDate.day
                            }
//                            include<RelativeLayout>(R.layout.member_card_info) {
//                                find<TextView>(R.id.name).textResource = R.string.member_card_card_type
//                                find<TextView>(R.id.value).text = when (card.cardTypeId) {
//                                    0 -> "VIC"
//                                    1 -> "BVIC"
//                                    2 -> "GVIC"
//                                    else -> "VIC"
//                                }
//                            }
                            include<RelativeLayout>(R.layout.member_card_info) {
                                find<TextView>(R.id.name).textResource = R.string.member_card_member_number
                                find<TextView>(R.id.value).text = card.cardPin.toString()
                            }
                            include<RelativeLayout>(R.layout.member_card_info) {
                                find<TextView>(R.id.name).textResource = R.string.member_card_name
//                                find<TextView>(R.id.value).text = if (MainActivity[ctx].isEnglish()) card.lastName + " " + card.firstName else card.lastName + " " + card.firstName
                                find<TextView>(R.id.value).text = card.firstName + " " + card.lastName
                            }
                            include<RelativeLayout>(R.layout.member_card_info) {
                                find<TextView>(R.id.name).textResource = R.string.member_card_email
                                find<TextView>(R.id.value).text = card.email
                            }
//                            include<RelativeLayout>(R.layout.member_card_info) {
//                                visibility = View.GONE
//                                find<TextView>(R.id.name).textResource = R.string.member_card_phone
//                                find<TextView>(R.id.value).autoLinkMask = Linkify.ALL
//                                find<TextView>(R.id.value).inputType = InputType.TYPE_CLASS_PHONE
//                                find<TextView>(R.id.value).text = card.tel
//                            }
                        }
                    }.lparams(matchParent, wrapContent) {
                        margin = dimen(R.dimen.margin_normal)
                        topToTop = PARENT_ID
//                        bottomToBottom = PARENT_ID
                    }
                }.lparams(matchParent, matchConstraint) {
                    topToBottom = R.id.member_card_titlebar
                    bottomToBottom = PARENT_ID
                }
            }
        }
    }
