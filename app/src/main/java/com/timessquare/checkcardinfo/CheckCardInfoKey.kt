package com.timessquare.checkcardinfo

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 4/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class CheckCardInfoKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(CheckCardInfoKey))

    override fun createFragment(): BaseFragment = CheckCardInfoFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}