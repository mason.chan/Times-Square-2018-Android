package com.timessquare.checkcardinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.timessquare.BaseFragment
import com.timessquare.database.entity.vicCard
import com.timessquare.utils.Constant
import org.jetbrains.anko.AnkoContext

/**
 *   Created by Mason on 4/12/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class CheckCardInfoFragment : BaseFragment() {
    val cardString: String by lazy { arguments?.getString(Constant.TAG_INTENT) ?: "" }

    lateinit var mView: CheckCardInfoUI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val gson = Gson()
        val type = object : TypeToken<vicCard>() {
        }.type
        val card = gson.fromJson<vicCard>(cardString, type)

        mView = CheckCardInfoUI(card)
        return mView.createView(AnkoContext.create(activity!!, this))
    }

}