package com.timessquare.memberlogin

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 9/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */


@SuppressLint("ParcelCreator")
@Parcelize
data class MemberLoginKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(MemberLoginKey))

    override fun createFragment(): BaseFragment = MemberLoginFragment()

    override fun stackIdentifier(): String =""
    companion object {}
}