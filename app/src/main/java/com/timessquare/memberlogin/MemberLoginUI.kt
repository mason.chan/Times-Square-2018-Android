package com.timessquare.memberlogin

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.support.design.widget.TextInputLayout
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.timessquare.BuildConfig
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.aboutus.AboutUsFragment
import com.timessquare.member.MemberKey
import com.timessquare.views.TitleBar
import com.timessquare.views.titleBar
import kotlinx.android.synthetic.main.dialog_view.view.*
import kotlinx.android.synthetic.main.menufooter_view.view.*
import kotlinx.android.synthetic.main.notification_template_lines_media.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.design.textInputLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 *   Created by Mason on 9/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */

class MemberLoginUI() : AnkoComponent<MemberLoginFragment> {
    lateinit var mTitleBar: TitleBar
    lateinit var mEmailEditText: EditText
    lateinit var mEmailTextLayout: TextInputLayout
    lateinit var mPasswordEditText: EditText
    lateinit var mPasswordTextLayout: TextInputLayout
    lateinit var mLoginButton: Button
    lateinit var mForgotPasswordTextView: TextView
    override fun createView(ui: AnkoContext<MemberLoginFragment>): View {
        return with(ui) {
            constraintLayout {
                mTitleBar = titleBar {
                    id = R.id.memberLoginTitleBar
                }.addTitle(R.string.member_login_title).addBackButton().addBackgroundColor(R.color.color_cover)
                        .lparams(width = matchParent) {
                            topToTop = PARENT_ID
                        }
                imageView {
                    imageResource = R.drawable.view_clock_tower
                    adjustViewBounds = true
                }.lparams(width = matchParent) {
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
                verticalLayout {
                    padding = dimen(R.dimen.padding_small)
                    // Email
                    textView(R.string.member_login_email) {
                        textColorResource = R.color.color_black
                        textSizeDimen = R.dimen.text_normal
                    }
                    mEmailEditText = include<EditText>(R.layout.car_park_edit_text) {
                        hintResource = R.string.member_login_email_hint
                    }
                    mEmailTextLayout = textInputLayout { }
                    // Password
                    textView(R.string.member_login_password) {
                        textColorResource = R.color.color_black
                        textSizeDimen = R.dimen.text_normal
                    }
                    mPasswordEditText = include<EditText>(R.layout.car_park_edit_text) {
                        inputType = TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
                        hintResource = R.string.member_login_password_hint
                    }
                    mPasswordTextLayout = textInputLayout { }

                    mLoginButton = button {
                        textResource = R.string.member_login_login_button
                        textSizeDimen = R.dimen.text_subject
                        backgroundResource = R.drawable.button_sure
                        paddingHorizontal = dimen(R.dimen.padding_double)
                    }.lparams(wrapContent, wrapContent) {
                        topMargin = 200
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                    mForgotPasswordTextView = textView {
                        textResource = R.string.member_login_Forgot_password
                        textSizeDimen = R.dimen.text_normal
                    }.lparams() {
                        gravity = Gravity.CENTER_HORIZONTAL
                        topMargin = dimen(R.dimen.margin_normal)
                    }
                }.lparams(matchParent, wrapContent) {
                    topToBottom = R.id.memberLoginTitleBar
                }
            }
        }
    }
}