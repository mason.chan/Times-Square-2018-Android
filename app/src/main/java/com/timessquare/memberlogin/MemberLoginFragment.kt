package com.timessquare.memberlogin

import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.accounts.AccountManagerFuture
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.i
import com.pawegio.kandroid.runDelayedOnUiThread
import com.pawegio.kandroid.runOnUiThread
import com.timessquare.BaseFragment
import com.timessquare.MainActivity
import com.timessquare.R
import com.timessquare.apiservice.datamodel.ForgotPasswordModel
import com.timessquare.apiservice.datamodel.MemberLoginModel
import com.timessquare.apiservice.datamodel.VicCard
import com.timessquare.database.entity.MemberEntity
import com.timessquare.database.entity.vicCard
import com.timessquare.member.MemberKey
import com.timessquare.memberforgotpassword.MemberForgotPasswordKey
import com.timessquare.utils.Constant
import com.timessquare.utils.SParem
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.delay
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx


/**
 *   Created by Mason on 9/7/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class MemberLoginFragment : BaseFragment() {
    lateinit var mView: MemberLoginUI
    lateinit var accountManager: AccountManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        this!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        mView = MemberLoginUI()
        return mView.createView(AnkoContext.create(activity!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accountManager = MainActivity[ctx].accountManager

        mView.mLoginButton.onClick {
            AttemptLogin()
        }
        mView.mForgotPasswordTextView.onClick {
//            ForgotPassword()
            MainActivity[ctx].navigateTo(MemberForgotPasswordKey())
        }
    }

    private fun ForgotPassword() {
        var email = mView.mEmailEditText.text.toString()
        /**
         * field Checking
         */
        if (email.isEmpty() or !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView.mEmailTextLayout.error = getString(R.string.member_login_email_error)
            return
        }
        /**
         * Create Observable
         */
        var   forgotPasswordObervable = MainActivity[ctx].apiService.forgotPassword(email)

        /**
         * Create Observer
         */
        var forgotPasswordObserver = object : Observer<ForgotPasswordModel> {
            override fun onComplete() {
                i("ForgotPassword", " onComplete          " + (Looper.myLooper() == Looper.getMainLooper()))
                runOnUiThread() {
//                //                                i("ForgotPassword", "" + it.message)
                    i("ForgotPassword", "" + "UI    " + (Looper.myLooper() == Looper.getMainLooper()))
                    MainActivity[ctx].AlertDialog("complete", "The forgot password email has be send to your email. Please check your email", true).show()
////                                }
                }
            }

            override fun onSubscribe(d: Disposable) {
                i("ForgotPassword", " onSubscribe")
                dismissDialog()
            }

            override fun onNext(t: ForgotPasswordModel) {
                i("ForgotPassword", " onNext")
//                if(t.message != null) {
//                    MainActivity[ctx].AlertDialog("Success", t.message!!, false).show()
//                } else {
//                    i("ForgotPassword", " Error  " + "No this Email.")
//                }
            }

            override fun onError(e: Throwable) {
                i("ForgotPassword", " Error " + e)
            }
        }
        /**
         * Subscribe
         */

        forgotPasswordObervable.subscribeOn(Schedulers.io())
                .doOnSubscribe { popDialog(msgId = R.string.member_login_forgot_password_loading_dialog_message, cancelable = false) }
                .doFinally { }
                .subscribeWith(forgotPasswordObserver)
    }

    private fun AttemptLogin() {


        var email = mView.mEmailEditText.text.toString()
        var password = mView.mPasswordEditText.text.toString()

        /**
         * Clean Error
         */
        mView.mEmailTextLayout.error = null
        mView.mPasswordTextLayout.error = null

        /**
         * Data Checking
         */
        if (email.isEmpty() or !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView.mEmailTextLayout.error = getString(R.string.member_login_email_error)
            return
        }
        if (password.isEmpty()) {
            mView.mPasswordTextLayout.error = getString(R.string.member_login_password_error)
            return
        }

        /**
         * Create Observable
         */
        var loginObservable = MainActivity[ctx].apiService.attemptMemberLogin(email, password)

        /**
         * Create Observer
         */

        var loginObserver = object : Observer<MemberLoginModel> {

            override fun onComplete() {
                i("observableString", " onComplete  ")
                // Go to Member Page
                runDelayedOnUiThread(500) {
                    if (MainActivity[ctx].sp.getBoolanValue(SParem.Member_Logined, false)) {
                        MainActivity[ctx].replaceHistoryWithIndex(MemberKey())
                    }
                    dismissDialog()
                }
            }

            override fun onSubscribe(d: Disposable) {
                i("observableString", " onSubscribe  " + d)
            }

            override fun onNext(t: MemberLoginModel) {
                i("observableString", " onNext  " + (Looper.myLooper() == Looper.getMainLooper()).toString())
                i("observableString", " onNext  " + t.error_message)
                if (!t.error_message.isNullOrBlank()) {
                    activity!!.runOnUiThread {
                        AlertDialog(R.string.member_login_login_incorrect_password_dialog_title, R.string.member_login_login_incorrect_password_dialog_message).show()
                    }
                } else {
                    Log.d("onNext", "logining ")
                    // Create
                    createAccount(email, password, t.access_token)
                    // Change Shared Preference Logined true
                    MainActivity[ctx].sp.setValue(SParem.Member_Logined, true)
                    // Save Password Shared Preference
                    MainActivity[ctx].sp.setValue(SParem.Member_Password, password)
                    // Update user data for Room database
                    MainActivity[ctx].database.memberDao().deleteAll()
                    var cardArray = ArrayList<vicCard>()
                    t.vic_card.forEach {
                        cardArray.add(vicCard(
                                cardPin = it.card_pin,
                                cardCode = it.card_code,
                                cardId = it.card_id,
                                cardTypeId = it.card_type_id,
                                cardStatus = it.card_status,
                                firstName = it.member_first_name,
                                lastName = it.member_last_name,
                                email = it.member_email,
                                tel = it.member_tel,
                                point = it.member_point,
                                expiryDate = it.member_point_expiry_date
                        ))
                    }

                    MainActivity[ctx].database.memberDao().insert(MemberEntity(
                            id = t.ecard_id,
                            email = t.ecard_email,
                            name = t.ecard_name,
                            refreshToken = t.ecard_refresh_token,
                            card = cardArray,
                            accessToken = t.access_token,
                            message = t.message,
                            errorMessage = t.error_message,
                            errorCode = t.error_code
                    ))
                    // Delete Previous Account
                    deletePreviousAccount()
                }
            }

            override fun onError(e: Throwable) {
                i("observableString", " onError  " + e.message)
                dismissDialog()
            }


        }
        /**
         * Subscribe
         */
        loginObservable.subscribeOn(Schedulers.io())
                .doOnSubscribe { popDialog(msgId = R.string.member_login_login_dialog, cancelable = false) }
                .doFinally { }
                .subscribeWith(loginObserver)
    }

    private fun createAccount(email: String, password: String, accessToken: String) {
        i("createAccount", "    " + email + password + accessToken)

        val map = HashMap<String, String>()
        map[Constant.API_EMAIL] = email
        map[Constant.API_PASSWORD] = password
        map[Constant.API_ACCESS_TOKEN] = accessToken
        val account = Account(map[Constant.API_EMAIL], Constant.ACCOUNT_TYPE)
        accountManager.addAccountExplicitly(account, map[Constant.API_PASSWORD], null)
        accountManager.setAuthToken(account, Constant.ACCOUNT_TYPE, map[Constant.API_ACCESS_TOKEN])
    }

    /**
     * Delete previous account.
     */
    private fun deletePreviousAccount() {
        runOnUiThread {
            i("deletePreviousAccount", "VERSION :" + if (Build.VERSION.SDK_INT < 23) "< 23" else "> 23" + "     size:    " + accountManager.getAccountsByType(Constant.ACCOUNT_TYPE).size)
            if (accountManager.getAccountsByType(Constant.ACCOUNT_TYPE).size > 1) {
                val currentAccount = accountManager.getAccountsByType(Constant.ACCOUNT_TYPE)[0]
                if (Build.VERSION.SDK_INT < 23) {
                    accountManager.removeAccount(currentAccount,
                            ({
                                i("deletePreviousAccount", "Account deleted.")
                            }),
                            Handler()
                    )
                } else {
                    accountManager.removeAccount(currentAccount,
                            MainActivity[ctx],
                            (AccountManagerCallback<Bundle> {
                                i("deletePreviousAccount", "Account deleted.")
                            }),
                            Handler()
                    )
                }
            }
//            accountManager.getAccountsByType(Constant.ACCOUNT_TYPE).forEach {
//                i("deletePreviousAccount", "Delete :" + it.name)
//            }
        }
    }
}