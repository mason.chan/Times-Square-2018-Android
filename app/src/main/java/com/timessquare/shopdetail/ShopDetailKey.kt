package com.timessquare.shopdetail

import android.annotation.SuppressLint
import com.timessquare.BaseFragment
import com.timessquare.BaseKey
import com.timessquare.utils.keyName
import kotlinx.android.parcel.Parcelize

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class ShopDetailKey(override val tag: String) : BaseKey(tag) {
    constructor() : this(keyName(ShopDetailKey))

    override fun createFragment(): BaseFragment = ShopDetailFragment()

    override fun stackIdentifier(): String = ""

    companion object  {}
}