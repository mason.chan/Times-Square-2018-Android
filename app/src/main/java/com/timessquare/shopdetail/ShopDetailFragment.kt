package com.timessquare.shopdetail

import android.view.View
import com.timessquare.R
import com.timessquare.shopanddine.shopanddinedetail.ShopDineDetailFragment
import com.timessquare.shopanddine.shopanddinedetail.ShopDineDetailOfferItem

/**
 *   Created by Mason on 22/6/2018.
 *   Hita Group
 *   mason.chan@sllin.com
 */
class ShopDetailFragment : ShopDineDetailFragment() {
    override fun fillData(view: View) {
//        mView.vicLinearLayout.visibility = if (shopEntity.vicPrivilege?: false) View.VISIBLE else View.GONE
//        mView.bvicLinearLayout.visibility = if (shopEntity.bvicPrivilege?: false) View.VISIBLE else View.GONE
        mView.contentTextView.text = if (isEnglish) shopEntity.description else shopEntity.descriptionZh
    }

    override fun addOffice(): ArrayList<ShopDineDetailOfferItem> {
        var officeArrayList = ArrayList<ShopDineDetailOfferItem>()

        if (shopEntity.vicPrivilege?: false) officeArrayList.add(ShopDineDetailOfferItem(R.drawable.ic_vic_privileges, R.string.shop_vic_privileges))
        if (shopEntity.bvicPrivilege?: false) officeArrayList.add(ShopDineDetailOfferItem(R.drawable.ic_bvic_privileges, R.string.shop_bvic_privileges))
        if (shopEntity.officeClub?: false) officeArrayList.add(ShopDineDetailOfferItem(R.drawable.ic_office_club_privileges, R.string.shop_office_club_privileges))
        if (shopEntity.clubOn?: false) officeArrayList.add(ShopDineDetailOfferItem(R.drawable.ic_club_on_privileges, R.string.shop_club_on_privileges))

        return officeArrayList
    }
}